package no.uit.metapipe.common.utils

import no.uit.metapipe.common.utils.packaging.GenericVersion
import org.scalatest.{FunSpec, Matchers}

class GenericVersionTest extends FunSpec with Matchers {
  describe("GenericVersion should") {
    it("compare properly: 1.2.3 == 1.2.3") {
      GenericVersion.compare("1.2.3", "1.2.3") == 0 should be(true)
    }
    it("compare properly: 1.2.3 < 2.2.3") {
      GenericVersion.compare("1.2.3", "2.2.3") < 0 should be(true)
    }
    it("compare properly: 1.2.3 < 1.3.3") {
      GenericVersion.compare("1.2.3", "1.3.3") < 0 should be(true)
    }
    it("compare properly: 1.2.3 < 1.2.4") {
      GenericVersion.compare("1.2.3", "1.2.4") < 0 should be(true)
    }
    it("compare properly: abc < abd") {
      GenericVersion.compare("abc", "abd") < 0 should be(true)
    }
  }
}
