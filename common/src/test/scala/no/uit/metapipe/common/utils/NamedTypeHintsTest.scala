package no.uit.metapipe.common.utils

import org.json4s.jackson.Serialization
import org.scalatest.{FunSpec, Matchers}

sealed trait TestClass
case class Test1(whatever: String) extends TestClass
case class Test2(whatever_1: String) extends TestClass
case class TestBox(tests: List[TestClass])

class NamedTypeHintsTest extends FunSpec with Matchers {

  def typeHints =
    NamedTypeHints.format(
      "_type",
      Map("test-1" -> classOf[Test1], "test-2" -> classOf[Test2]))

  describe("Named type hints") {
    it("Serialize properly") {
      implicit val formats = typeHints
      val correct = List(
        Test1("whatever"),
        Test2("whatever2")
      )
      val json = Serialization.write(correct)
      require(json.contains("\"_type\""))
      val result: List[_] = Serialization.read[List[_]](json)
      result match {
        case List(Test1(_), Test2(_)) => // pass
      }
    }
  }
}
