package no.uit.metapipe.common.execution.toolabs

import org.scalatest.{FunSpec, Matchers}

class ToolValidationTest extends FunSpec with Matchers {
  val cmd = Seq("/usr/bin/env")

  describe("ToolValidator") {
    it("should be able to validate exit code") {
      val resOk = ProcValidator
        .exitCode(0)
        .runValidate(cmd)
      resOk.isEmpty shouldBe true

      val resErr = ProcValidator
        .exitCode(1)
        .runValidate(cmd)
      resErr.isEmpty shouldBe false
    }

    it("should be able to validate stderr") {
      val resOk =
        ProcValidator.stderr(_ == "", "stderr was not empty").runValidate(cmd)
      val resErr = ProcValidator
        .stderr(_ == ":)", "stderr did not contain smiley")
        .runValidate(cmd)
      resOk.isEmpty shouldBe true
      resErr.isEmpty shouldBe false
    }

    it("should be able to validate stdout") {
      val resOk = ProcValidator
        .stdout(_.contains("USER"), "stdout did not contain USER")
        .runValidate(cmd)
      val resErr = ProcValidator
        .stdout(_ == ":)", "stdout did not contain smiley")
        .runValidate(cmd)
      resOk.isEmpty shouldBe true
      resErr.isEmpty shouldBe false
    }

    it("should be able to handle non-existing binaries") {
      val resErr = ProcValidator.runValidate(Seq("/should/not/exist/blah"))
      resErr.isEmpty shouldBe false
    }
  }
}
