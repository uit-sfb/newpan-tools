package no.uit.metapipe.common.utils;

import java.security.SecureRandom;

public class Random {
    public static String string(int length) {
        return string(length, "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789");
    }
    public static String string(int length, String characterSetString) {
        java.security.SecureRandom random = new SecureRandom();
        char[] characterSet = characterSetString.toCharArray();
        char[] result = new char[length];
        for (int i = 0; i < result.length; i++) {
            int randomCharIndex = random.nextInt(characterSet.length);
            result[i] = characterSet[randomCharIndex];
        }
        return new String(result);
    }
}
