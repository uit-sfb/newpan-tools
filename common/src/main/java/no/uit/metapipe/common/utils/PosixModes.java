package no.uit.metapipe.common.utils;

import java.nio.file.attribute.PosixFilePermission;
import java.util.HashSet;
import java.util.Set;

import static java.nio.file.attribute.PosixFilePermission.*;

public class PosixModes {
    public static int toInt(Set<PosixFilePermission> permissions) {
        int res = 0;
        for(PosixFilePermission p : permissions) {
            switch (p) {
                case OWNER_READ:
                    res |= 0400;
                    break;
                case OWNER_WRITE:
                    res |= 0200;
                    break;
                case OWNER_EXECUTE:
                    res |= 0100;
                    break;
                case GROUP_READ:
                    res |= 0040;
                    break;
                case GROUP_WRITE:
                    res |= 0020;
                    break;
                case GROUP_EXECUTE:
                    res |= 0010;
                    break;
                case OTHERS_READ:
                    res |= 0004;
                    break;
                case OTHERS_WRITE:
                    res |= 0002;
                    break;
                case OTHERS_EXECUTE:
                    res |= 0001;
                    break;
            }
        }
        return res;
    }

    public static Set<PosixFilePermission> fromInt(int mode) {
        HashSet<PosixFilePermission> p = new HashSet<>();
        if((mode & 0400) != 0) {
            p.add(OWNER_READ);
        }
        if((mode & 0200) != 0) {
            p.add(OWNER_WRITE);
        }
        if((mode & 0100) != 0) {
            p.add(OWNER_EXECUTE);
        }
        if((mode & 0040) != 0) {
            p.add(GROUP_READ);
        }
        if((mode & 0020) != 0) {
            p.add(GROUP_WRITE);
        }
        if((mode & 0010) != 0) {
            p.add(GROUP_EXECUTE);
        }
        if((mode & 0004) != 0) {
            p.add(OTHERS_READ);
        }
        if((mode & 0002) != 0) {
            p.add(OTHERS_WRITE);
        }
        if((mode & 0001) != 0) {
            p.add(OTHERS_EXECUTE);
        }
        return p;
    }
}
