package no.uit.metapipe.sparkabstractions.toolabs.utils

import org.apache.spark.SparkContext
import org.apache.spark.rdd.RDD

import scala.reflect.ClassTag

object Helpers {
  def ifFeatureEnabled[T: ClassTag](featureEnabled: Boolean)(f: => RDD[T])(
      implicit sc: SparkContext): RDD[T] = {
    if (featureEnabled)
      f
    else
      sc.emptyRDD
  }
}
