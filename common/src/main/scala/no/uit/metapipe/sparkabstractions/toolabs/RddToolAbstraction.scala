package no.uit.metapipe.sparkabstractions.toolabs

import no.uit.metapipe.common.logging.Logging
import no.uit.metapipe.common.execution.toolabs._
import no.uit.metapipe.sparkabstractions.toolabs.utils.KryoSerializationWrapper
import org.apache.spark.rdd.RDD

import scala.language.implicitConversions
import scala.reflect.ClassTag

class RddWithTool[A](rdd: RDD[A]) extends Logging {
  def mapPartitionsWithTool[B: ClassTag, Input, Output: ClassTag](
      toolWI: ToolWrapperInterface[Input, Output],
      toInput: Iterator[A] => Input,
      toOutput: Output => Iterator[B]): RDD[B] = {
    val taskName: String = toolWI.ctxName
    log.info(s"'$taskName' sent to Spark for eventual execution")
    val exec = KryoSerializationWrapper(toolWI)
    rdd
      .mapPartitionsWithIndex { (idx, it) =>
        val in: Input = toInput(it)
        val out = exec.value.execute(in, Some(idx))
        toOutput(out)
      }
      .setName(taskName)
  }
}

object RddToolAbstraction {
  implicit def rddWithTool[A](rdd: RDD[A]): RddWithTool[A] =
    new RddWithTool[A](rdd)
}
