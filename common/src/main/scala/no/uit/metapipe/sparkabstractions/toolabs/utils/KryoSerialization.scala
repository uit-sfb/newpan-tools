package no.uit.metapipe.sparkabstractions.toolabs.utils

import java.nio.ByteBuffer

import org.apache.spark.{SparkConf, SparkEnv}
import org.apache.spark.serializer.{KryoSerializer => SparkKryoSerializer}
import scala.reflect.ClassTag

/**
  * Makes any unserializable object serializable by both Kryo and Java!
  */
class KryoSerializationWrapper[T: ClassTag] extends Serializable {
  @transient var value: T = _

  private var valueSerialized: Array[Byte] = _

  // The getter and setter for valueSerialized is used for XML serialization.
  def getValueSerialized(): Array[Byte] = {
    valueSerialized = KryoSerializer.serialize(value)
    valueSerialized
  }

  def setValueSerialized(bytes: Array[Byte]) = {
    valueSerialized = bytes
    value = KryoSerializer.deserialize[T](valueSerialized)
  }

  // Used for Java serialization.
  private def writeObject(out: java.io.ObjectOutputStream) {
    getValueSerialized()
    out.defaultWriteObject()
  }

  private def readObject(in: java.io.ObjectInputStream) {
    in.defaultReadObject()
    setValueSerialized(valueSerialized)
  }
}

object KryoSerializationWrapper {
  def apply[T: ClassTag](value: T): KryoSerializationWrapper[T] = {
    val wrapper = new KryoSerializationWrapper[T]
    wrapper.value = value
    wrapper
  }
}

/**
  * Java object serialization using Kryo. This is much more efficient, but Kryo
  * sometimes is buggy to use. We use this mainly to serialize the object
  * inspectors.
  */
object KryoSerializer {
  @transient lazy val ser: SparkKryoSerializer = {
    val sparkConf = Option(SparkEnv.get).map(_.conf).getOrElse(new SparkConf())
    new SparkKryoSerializer(sparkConf)
  }

  def serialize[T: ClassTag](o: T): Array[Byte] = {
    ser.newInstance().serialize(o).array()
  }

  def deserialize[T: ClassTag](bytes: Array[Byte]): T = {
    ser.newInstance().deserialize[T](ByteBuffer.wrap(bytes))
  }
}
