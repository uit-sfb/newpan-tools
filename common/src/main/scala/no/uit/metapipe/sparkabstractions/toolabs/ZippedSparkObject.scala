package no.uit.metapipe.sparkabstractions.toolabs

import java.nio.file.Path

import no.uit.sfb.scalautils.common.FileUtils
import org.apache.spark.SparkContext
import org.apache.spark.rdd.RDD

import scala.language.implicitConversions
import scala.reflect.ClassTag

class ZippedSparkObject[T: ClassTag](rdd: RDD[T])(implicit sc: SparkContext) {
  def saveAsZso(path: Path): Unit = {
    val parentPath = path.getParent
    val unzippedTempPath =
      parentPath.resolve(s"${path.getFileName}.unzipped.tmp")
    val zippedTempPath = parentPath.resolve(s"${path.getFileName}.zipped.tmp")
    //Spark complains in case the directory already exists, so we delete it first in case
    if (FileUtils.exists(unzippedTempPath))
      FileUtils.deleteDir(unzippedTempPath)
    FileUtils.createParentDirs(unzippedTempPath)
    rdd.saveAsObjectFile(unzippedTempPath.toString)
    FileUtils.zip(unzippedTempPath, zippedTempPath, true)
    FileUtils.moveFile(zippedTempPath, path)
  }
}

object ZippedSparkObject {
  implicit def withZippedSparkObject[T: ClassTag](rdd: RDD[T])(
      implicit sc: SparkContext): ZippedSparkObject[T] =
    new ZippedSparkObject(rdd)

  def loadFromZso[T: ClassTag](path: Path)(
      implicit sc: SparkContext): RDD[T] = {
    val unzippedTempPath =
      path.getParent.resolve(s"${path.getFileName}.unzipped.tmp")
    FileUtils.unzip(path, unzippedTempPath, false)
    val rdd = sc.objectFile[T](unzippedTempPath.toString)
    rdd
  }
}
