package no.uit.metapipe.sparkabstractions.toolabs

import java.nio.file.Path

import no.uit.metapipe.common.utils.snapshot.InFileSnapshotStrategy
import no.uit.sfb.scalautils.common.FileUtils
import org.apache.spark.SparkContext
import org.apache.spark.rdd.RDD

class RddSnapshotStrategy[T: Manifest](path: Path)(implicit sc: SparkContext)
    extends InFileSnapshotStrategy[RDD[T]](path) {
  protected override def toFile(filePath: Path, t: RDD[T]): Unit = {
    FileUtils.createParentDirs(filePath)
    t.saveAsObjectFile(filePath.toString)
  }

  protected override def fromFile(filePath: Path): RDD[T] = {
    sc.objectFile(filePath.toString)
  }
}
