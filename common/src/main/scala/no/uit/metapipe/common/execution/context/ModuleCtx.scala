package no.uit.metapipe.common.execution.context

import java.nio.file.Path

import no.uit.metapipe.common.execution.pipeline.Identifier

import java.net.URL

/**
  * Do not create manually -- use an AttemptCtx or another ModuleCtx
  */
case class ModuleCtx(protected val basePath: Path,
                     identifier: Identifier,
                     numThreads: Int = 1,
                     useDocker: Boolean,
                     artifactoryUrl: URL)
    extends InOutContextLike {
  //Use numThreadsOverride if you wish to use another value than the one set in the current ctx
  def newModuleCtx(moduleName: String,
                   numThreadsOverride: Int = numThreads,
                   useDocker: Boolean = useDocker,
                   artifactoryUrl: URL = artifactoryUrl) =
    ModuleCtx(basePath,
              identifier + moduleName,
              numThreadsOverride,
              useDocker,
              artifactoryUrl)

  //Use numThreadsOverride if you wish to use another value than the one set in the current ctx
  def newToolCtx(toolName: String,
                 partitionIdx: Option[Int] = None,
                 numThreadsOverride: Int = numThreads,
                 useDocker: Boolean = useDocker,
                 artifactoryUrl: URL = artifactoryUrl) =
    ToolCtx(
      basePath,
      identifier + toolName + partitionIdx.map { _.toString }.getOrElse(""),
      numThreadsOverride,
      useDocker,
      artifactoryUrl) //added Docker flag
}
