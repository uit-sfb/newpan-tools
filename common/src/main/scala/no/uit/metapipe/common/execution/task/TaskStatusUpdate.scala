package no.uit.metapipe.common.execution.task

import java.nio.charset.StandardCharsets

import no.uit.metapipe.common.clients.DatasetUrl
import no.uit.sfb.scalautils.json.Json

case class TaskStatusUpdate(state: Option[TaskState] = None,
                            notifications: Option[Map[String, String]] = None,
                            outputs: Option[Map[String, DatasetUrl]] = None) {
  lazy val toOTW =
    TaskStatusUpdateOTW(state map { _.name }, notifications, outputs)
}

object TaskStatusUpdate {
  def parse(bin: Array[Byte]): TaskStatusUpdate = {
    val otw =
      Json.parse[TaskStatusUpdateOTW](new String(bin, StandardCharsets.UTF_8))
    fromOTW(otw)
  }
  private def fromOTW(otw: TaskStatusUpdateOTW): TaskStatusUpdate =
    TaskStatusUpdate(otw.state map { s =>
      TaskState.fromString(s)
    }, otw.properties, otw.outputs)
}

//Over the wire
case class TaskStatusUpdateOTW(
    state: Option[String] = None,
    properties: Option[Map[String, String]] = None,
    outputs: Option[Map[String, DatasetUrl]] = None
)
