package no.uit.metapipe.common.utils.fsm

trait FsmWrapper[S <: FsmState] extends FSM[S] {
  protected def fsm: FSM[S]

  override final lazy val quietInitialTransition: Boolean =
    fsm.quietInitialTransition
  override final lazy val quietSelfTransition: Boolean = fsm.quietSelfTransition
  /*override final lazy val forceAllowSelfTransition: Boolean =
    fsm.forceAllowSelfTransition*/
  override final lazy val ignoreAllWhenTerminal: Boolean =
    fsm.ignoreAllWhenTerminal

  final lazy val stateCompanion: FsmStateCompanion[S] = fsm.stateCompanion
  final def state: S = fsm.state
  final def transitionTo(newState: S,
                         arg: Any = null,
                         force: Boolean = false): Unit =
    fsm.transitionTo(newState, arg, force)
}
