package no.uit.metapipe.common.execution.toolabs

object Defs {
  type ValidatorResult = Seq[ValidationError]
  type Validator[T] = T => ValidatorResult
}
