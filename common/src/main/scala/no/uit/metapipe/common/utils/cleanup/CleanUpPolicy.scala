package no.uit.metapipe.common.utils.cleanup

trait CleanUpPolicy {
  def apply(): Unit
}
