package no.uit.metapipe.common.lib.config

case class Credentials(login: String, password: String = "")
