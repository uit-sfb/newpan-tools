package no.uit.metapipe.common.utils

import java.nio.file.Path

import scala.language.implicitConversions

object Implicits {
  implicit def autoWrapOptions[T](t: T): Option[T] = Option(t)
  implicit def pathToString(p: Path): String = p.toString
}
