package no.uit.metapipe.common.execution.pipeline

import no.uit.metapipe.common.execution.context.ModuleCtx
import no.uit.metapipe.common.lib.TaskParameter
import no.uit.metapipe.common.execution.execmanapi.SpecializedExecutionApi
import no.uit.metapipe.common.execution.executor.{ExecutorGroups, TaskExecutor}
import no.uit.metapipe.common.execution.task.{TaskResult, TaskState}
import no.uit.metapipe.common.lib.dataset.impl.{DatasetForm, MetapipeDataset}
import no.uit.metapipe.common.logging.Logging
import rx.Rx

import scala.concurrent._

case object CancelException extends RuntimeException("Cancelled")
case object InputFailed extends RuntimeException("Input failed")
case object OutputNotCompleted extends RuntimeException("Output not completed")

/**
  * Responsible for the execution and of part of a Pipeline asynchronously.
  * Pipeline and Module are JobManagerAPI-aware.
  * It is where the logic gets selected from the TaskRepository and completed with metapipe parameters and the restricted task execution API.
  * Non-blocking -> possible to run several independent modules within a pipeline in parallel.
  * As with all executors, cannot be reuse (discard when failure / finish).
  */
abstract class ModuleExecutor(val context: ModuleCtx)(
    implicit ec: ExecutionContext)
    extends ModuleLike[TaskState]
    with Logging {
  lazy val moduleName: String = context.identifier.name
  def execGroup: String =
    ExecutorGroups.default //On which executor group this module should be executed on
  protected def execApi: SpecializedExecutionApi

  protected def executor
    : TaskExecutor[TaskParameter, TaskResult] //An instance of TaskExecutorImpl
  //To refer to the output of another module: otherModule.outputs("out1"), or if you want all of them: otherModule.outputs
  protected[pipeline] def fInputs
    : Map[String, Future[MetapipeDataset]] //Should be implemented as a val.
  protected[pipeline] def fConstraint
    : Future[Unit] //Used to delay the execution of a module in case some resource constraint may apply (ex: only one module executing on Spark at the same time)
  def pOutputs
    : Map[String, Promise[MetapipeDataset]] //Should be implemented as a val with all the keys already in place.
  final def bindAll: Map[String, Future[MetapipeDataset]] =
    pOutputs mapValues {
      _.future
    }
  final def bind(key: String): (String, Future[MetapipeDataset]) =
    key -> pOutputs
      .getOrElse(key, throw new RuntimeException(s"Key $key not found"))
      .future

  protected lazy val _state: Rx[TaskState] =
    executor.stateRx

  /**
    * We consume the TaskResult here and push outputs/notification
    */
  def execute(): Unit

  override final def cancel(): Unit = {
    super.cancel()
    executor.cancel()
  }

  /**
    * Pushes all the outputs for this ModuleExecutor whose promise is not completed already and completes the promise
    * Blocking
    * Will throw an error in case pushing fails
    */
  protected def pushOutputs(outputs: Map[String, DatasetForm]): Unit
}
