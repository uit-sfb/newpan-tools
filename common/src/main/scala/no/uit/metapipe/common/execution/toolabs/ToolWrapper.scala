package no.uit.metapipe.common.execution.toolabs

import java.io._
import java.nio.file.Path

import no.uit.metapipe.common.execution.context.ToolCtx
import no.uit.metapipe.common.logging.Logging
import no.uit.metapipe.common.execution.toolabs.Defs.ValidatorResult
import no.uit.metapipe.common.metadata.Versioned

import scala.language.existentials
import scala.util.{Failure, Success, Try}

trait ToolWrapper[In, Out] extends Serializable with Versioned with Logging {
  def toolName: String
  def packageDir: Option[Path]

  final def execute(ctx: String => ToolCtx, in: In): Out = {
    execute(ctx(toolName), in)
  }

  /**
    * Executes the wrapped tool
    */
  final def execute(ctx: ToolCtx, in: In): Out = {
    log.info(
      s"Executing '${ctx.identifier}' with ${ctx.numThreads} thread(s) if tools allows it...")

    assert(
      ctx.isVirgin(),
      s"A ToolCtx can only be used ones, but '${ctx.identifier} seems to be already existing'")

    val output = Try {
      execImpl(ctx, in)
    } match {
      case Success(out) =>
        log.info(s"Successfully executed tool '${ctx.identifier}'")
        ctx.archive("SUCCESS")
        out
      case Failure(NonZeroStatusCodeException(code)) =>
        ctx.archive(s"FAILED")
        throw ctx.failureException(code)
      case Failure(e) =>
        ctx.archive(s"FAILED")
        throw e
    }

    output
  }

  def execImpl(ctx: ToolCtx, in: In): Out

  def validateTool: ValidatorResult
}
