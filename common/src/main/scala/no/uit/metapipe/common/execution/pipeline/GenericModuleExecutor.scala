package no.uit.metapipe.common.execution.pipeline

import no.uit.metapipe.common.execution.context.ModuleCtx
import no.uit.metapipe.common.execution.executor.impl.LocalTaskExecutorImpl
import no.uit.metapipe.common.execution.executor.TaskExecutor
import no.uit.metapipe.common.execution.task.{
  GenericTaskOrder,
  TaskResult,
  TaskState
}
import no.uit.metapipe.common.lib.dataset.impl.{DatasetForm, MetapipeDataset}
import no.uit.metapipe.common.lib.{TaskLogic, TaskParameter}
import no.uit.sfb.scalautils.common.FutureUtils
import no.uit.sfb.scalautils.json.Json

import scala.concurrent.{Await, ExecutionContext, Future, Promise}
import scala.concurrent.duration._
import scala.util.control.NonFatal

/**
  * Module executed locally
  */
abstract class GenericModuleExecutor[Params <: AnyRef: Manifest](
    val parameters: Params,
    moduleCtx: ModuleCtx,
    protected[pipeline] val fInputs: Map[String, Future[MetapipeDataset]],
    config: ModuleExecutorConfig,
    protected[pipeline] val fConstraint: Future[Unit] = Future.successful(Unit))(
    implicit ec: ExecutionContext)
    extends ModuleExecutor(moduleCtx) {
  protected val execApi = config.execApi
  def logic: TaskLogic

  lazy val executor: TaskExecutor[TaskParameter, TaskResult] =
    new LocalTaskExecutorImpl[TaskParameter, TaskResult](
      context,
      logic
    )

  /**
    * We consume the TaskResult here and push outputs/notification
    * Attention: In case of retry, if not all but some of the outputs already exists, they will NOT be overwritten even though they are computed anew
    */
  final def execute(): Unit = {
    if (pOutputs.isEmpty)
      log.warn(s"Module $id does not have any output -- will not be executed")

    //We first load any output already available (in case this is not the first attempt)
    pOutputs.keys foreach { outputRef =>
      Await.result(execApi.fetchDataset(outputRef), 5.minutes) match {
        case Some(res) =>
          log.info(s"Output $outputRef already available")
          completePromise(outputRef, res)
          Some(outputRef -> res)
        case None =>
          log.info(s"Output $outputRef NOT available")
          None
      }
    }

    //We execute the modules if needed
    val fTaskResult: Future[TaskResult] =
      if (pOutputs exists {
            case (_, promise) =>
              !promise.isCompleted
          }) {
        //One or more outputs were not available -> we execute the module
        executor.tryTransitionTo(TaskState.WAITING_FOR_RESOURCE)
        (FutureUtils.sequenceMap(fInputs) transform (
          inp => {
            //We wait for all the registered inputs to complete, then we execute the logic passing them as parameter
            val param = TaskParameter(
              GenericTaskOrder(id = id.toString,
                               parameters = Json.serialize[Params](parameters)),
              inp
            )
            //First we wait for the constraint Future to complete
            fConstraint.flatMap { _ =>
              //Now we got the lock we can start execution
              executor.execute(param)
            }
          },
          e => {
            //If an input failed, we fail the executor and return failed future.
            fInputs.foreach {
              case (k, f) =>
                f.failed.foreach {
                  case InputFailed =>
                  case err =>
                    log.warn(s"Input '$k' failed due to ${err.getMessage}")
                }
            }
            executor.failedWith(TaskState.INPUT_FAILED)
            InputFailed
          }
        )).flatMap(identity) //Use .flatten in scala 2.12
      } else {
        //All the outputs were already available (i.e. computed during a previous attempt -> we complete the promises without executing the module)
        executor.tryTransitionTo(TaskState.SKIPPED)
        Future.successful(TaskResult())
      }

    //We push the results and notifications if successful, or push a notification if failed
    (fTaskResult map { taskResult =>
      pushOutputs(taskResult.outputs) //We push only datasets that do not exist yet
      execApi.updateProperties(taskResult.notifications)
      checkAllPromisesCompleted()
      executor.tryTransitionTo(TaskState.COMPLETED)
    }).failed.foreach { e =>
      failOutputsWith(e)
    }
  }

  private def completePromise(key: String, dataset: MetapipeDataset): Unit = {
    pOutputs.get(key) match {
      case Some(p) => p.success(dataset)
      case None =>
        log.info(
          s"Note: output $key pushed to storage but is not part of the pipeline circuit -- this might be an omission.")
    }
  }

  final protected def pushOutputs(outputs: Map[String, DatasetForm]): Unit = {
    //For each output received,
    outputs foreach {
      case (key, datasetForm) =>
        if (!pOutputs(key).isCompleted) {
          //we push it to storage BEFORE we complete the promise
          //Note: if already persisted, no upload will be performed
          val fDataset = execApi.pushDataset(key, datasetForm)
          val dataset = Await.result(fDataset, 2.hours) //May throw an error
          //and we compete its associated promise
          completePromise(key, dataset)
        }
    }
  }

  private def failOutputsWith(e: Throwable): Unit = {
    e match {
      case CancelException => log.info(s"Cancelling module '$id'")
      case InputFailed =>
        log.warn(s"Module '$id' failed due to ${InputFailed.getMessage}")
      case OutputNotCompleted =>
        log.warn(s"Module '$id' failed due to ${OutputNotCompleted.getMessage}")
      case NonFatal(err) =>
        log.warn(s"Module '$id' failed due to ${err.getMessage}", err)
    }
    log.debug(
      s"Failing non-already completed outputs for $id because of ${e.getMessage}")
    pOutputs foreach {
      case (_, p) =>
        if (!p.isCompleted)
          p.failure(e)
    }
    executor.tryTransitionTo(TaskState.FAILED)
  }

  //We check that all the promises have been completed and if not, we raise an error since it means that one output will never be ready.
  private def checkAllPromisesCompleted(): Unit = {
    val notCompleted: Seq[String] =
      (for {
        (key, p) <- pOutputs
        if !p.isCompleted
      } yield {
        key
      }).toSeq
    if (notCompleted.nonEmpty) {
      log.warn(
        s"The following outputs were not completed:\n${notCompleted.mkString("\n")}")
      executor.tryTransitionTo(TaskState.MISSING_OUTPUT)
      throw OutputNotCompleted
    }
  }

  def outputList: Seq[String]
  lazy val pOutputs: Map[String, Promise[MetapipeDataset]] = (outputList map {
    out =>
      out -> Promise[MetapipeDataset]()
  }).toMap

  onStateChange(s => execApi.updateProperties(Map(s"state.$id" -> s.name)))
}
