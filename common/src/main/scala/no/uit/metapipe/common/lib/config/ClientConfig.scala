package no.uit.metapipe.common.lib.config

import java.net.URL
import java.nio.file.Path

case class ClientConfig(
    credentials: Credentials,
    jobMan: URL,
    objectStore: URL,
    auth: URL,
    cachePath: Path,
    tags: Set[String]
)
