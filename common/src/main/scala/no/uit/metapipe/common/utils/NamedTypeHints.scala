package no.uit.metapipe.common.utils

import org.json4s.{DefaultFormats, TypeHints}

class NamedTypeHints(types: Map[String, Class[_]]) extends TypeHints {
  require(types.values.toSet.size == types.values.size) // types are unique

  private val reverse: Map[Class[_], String] = types.map(_.swap)

  override val hints: List[Class[_]] = types.values.toList

  override def classFor(hint: String): Option[Class[_]] = types.get(hint)

  override def hintFor(clazz: Class[_]): String = reverse(clazz)
}

object NamedTypeHints {
  def format(name: String, types: Map[String, Class[_]]) = new DefaultFormats {
    override val typeHints = new NamedTypeHints(types)
    override val typeHintFieldName = name
  }
}
