package no.uit.metapipe.common.lib.dataset

/**
  * ManagedDatasets are Datasets managed by a DatasetManagerLike providing caching capabilities
  */
trait ManagedDatasetLike extends DatasetLike {
  type ManagerType <: DatasetManagerLike[_]

  def datasetManager: ManagerType
}
