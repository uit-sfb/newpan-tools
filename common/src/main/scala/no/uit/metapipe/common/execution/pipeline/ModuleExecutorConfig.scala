package no.uit.metapipe.common.execution.pipeline

import no.uit.metapipe.common.execution.execmanapi.SpecializedExecutionApi
import no.uit.metapipe.common.lib.dataset.impl.MetapipeDataset
import no.uit.metapipe.common.lib.dataset.DatasetManagerLike

case class ModuleExecutorConfig(execApi: SpecializedExecutionApi)

case class ModuleExecutorProxyConfig(
    execConf: ModuleExecutorConfig,
    datasetManager: DatasetManagerLike[MetapipeDataset],
    vhost: String)
