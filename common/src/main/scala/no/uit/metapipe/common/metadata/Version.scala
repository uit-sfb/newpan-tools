package no.uit.metapipe.common.metadata

import java.nio.file.Path

import no.uit.metapipe.common.utils.NamedTypeHints
import no.uit.sfb.scalautils.common.FileUtils
import org.joda.time.format.ISODateTimeFormat
import org.joda.time.{DateTimeZone, Instant}
import org.json4s.jackson.Serialization

case class VersionInfo(name: String, ver: String)

object VersionInfo {
  def fromVersionFile(path: Path): VersionInfo = {
    VersionInfo(path.getParent.getFileName.toString,
                path.getFileName.toString.split("_").head)
  }
}

sealed trait Version {
  def toJson: String = Version.toJson(this)
}

object Version {
  object newpantools {
    val build = "custom build"
    val version = Version.String(build)
  }

  val DEFAULT_FILENAME = "version.json"

  private implicit val typeHints = NamedTypeHints.format(
    "type",
    Map(
      "major-minor" -> classOf[MajorMinor],
      "major-minor-patch" -> classOf[MajorMinorPatch],
      "last-modified" -> classOf[LastModified],
      "string" -> classOf[String]
    )
  )

  def parse(input: java.lang.String): Version = {
    Serialization.read[Version](input)
  }

  def toJson(version: Version): java.lang.String = {
    Serialization.writePretty(version)
  }

  def readFile(path: Path): Version = {
    parse(FileUtils.readFile(path))
  }

  def writeFile(path: Path, version: Version): Unit = {
    FileUtils.writeToFile(path, toJson(version))
  }

  case class YearMonth(year: Int, month: Int)
      extends Version
      with Comparable[YearMonth] {
    override def toString = s"${year}_$month"

    override def compareTo(o: YearMonth): Int = {
      import scala.math.Ordered.orderingToOrdered

      val t1 = (year, month)
      val t2 = (o.year, o.month)
      t1 compare t2
    }
  }

  case class MajorMinor(major: Int, minor: Int)
      extends Version
      with Comparable[MajorMinor] {
    override def toString = s"$major.$minor"

    override def compareTo(o: MajorMinor): Int = {
      import scala.math.Ordered.orderingToOrdered

      val t1 = (major, minor)
      val t2 = (o.major, o.minor)
      t1 compare t2
    }
  }

  case class MajorMinorPatch(major: Int, minor: Int, patch: Int)
      extends Version
      with Comparable[MajorMinorPatch] {
    override def toString = s"$major.$minor.$patch"

    override def compareTo(o: MajorMinorPatch): Int = {
      import scala.math.Ordered.orderingToOrdered

      val t1 = (major, minor, patch)
      val t2 = (o.major, o.minor, o.patch)
      t1 compare t2
    }
  }

  case class LastModified(millisSinceEpoch: Long) extends Version {
    override def toString: java.lang.String = {
      new Instant(millisSinceEpoch).toDateTime
        .withZone(DateTimeZone.UTC)
        .toString(ISODateTimeFormat.dateTimeNoMillis())
    }

    def toInstant = new Instant(millisSinceEpoch)
  }

  /** Any String value that does not fit any of the other models.
    */
  case class String(value: java.lang.String) extends Version {
    override def toString = s"$value"
  }

  /** Should not be used for other purposes than to temporarily satisfy the compiler!
    */
  case object Unknown extends Version {
    override def toString = s"unknown"
  }
}

trait Versioned {
  def version: Version
}
