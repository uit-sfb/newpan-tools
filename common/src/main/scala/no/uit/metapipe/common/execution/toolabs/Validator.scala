package no.uit.metapipe.common.execution.toolabs

import no.uit.metapipe.common.execution.toolabs.Defs.Validator

object Validator {
  def apply[T](p: T => Boolean, msg: String): Validator[T] = { t: T =>
    if (p(t)) {
      Nil
    } else {
      Seq(ValidationError(msg))
    }
  }
}

case class ValidationError(msg: String)
