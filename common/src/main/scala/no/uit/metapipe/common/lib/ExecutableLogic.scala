package no.uit.metapipe.common.lib

trait ExecutableLogic[P, T] {
  def execute(p: P): T
}

trait ReferencedExecutableLogic[P, T] extends ExecutableLogic[P, T] {

  /**
    * The reference should follow the format:
    *   workflow.stage.implementation.version
    */
  def implRef: String
}
