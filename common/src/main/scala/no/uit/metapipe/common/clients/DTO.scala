package no.uit.metapipe.common.clients

case class DatasetUrl(url: String)

case class CreateUserJobDto(
    tag: String,
    inputs: Map[String, DatasetUrl],
    properties: Map[String, String],
    parameters: String
)

case class UserJobDto(
    tag: String,
    outputs: Map[String, DatasetUrl],
    inputs: Map[String, DatasetUrl],
    parameters: String,
    properties: Map[String, String],
    jobId: String,
    timeSubmitted: String,
    state: String, //JobState (not AttemptState)
    userId: String
) {
  lazy val toCreateUserJobDto =
    CreateUserJobDto(tag, inputs, properties, parameters)
}

case class AttemptDto(
    executorId: String,
    state: String,
    attemptId: String,
    timeCreated: String,
    lastHeartbeat: String,
    outputs: Map[String, DatasetUrl]
)

case class ExecutorJobDto(
    jobId: String,
    userId: Option[String],
    //groupId: Option[String],
    attempt: AttemptDto,
    attemptUri: String,
    selfUri: String,
    inputs: Map[String, DatasetUrl],
    parameters: String
)

case class TaskOrderDto(
    id: String,
    implRef: String,
    parameters: String
)
