package no.uit.metapipe.common.execution.pipeline
import no.uit.metapipe.common.execution.attempt.Attempt

import java.nio.file.{Path, Paths}

class Identifier(seq: Seq[String]) {
  val name = seq.last
  override def toString: String = seq.mkString(".")
  def toRelativePath: Path = Paths.get(seq.head, seq.tail: _*)
  def +(s: String): Identifier = resolve(s)
  def resolve(s: String): Identifier = {
    if (s.nonEmpty)
      new Identifier(seq :+ s)
    else
      this
  }
}

object Identifier {
  def apply(ids: String*): Identifier = new Identifier(ids)
  def fromString(str: String): Identifier = new Identifier(str.split('.'))
}
