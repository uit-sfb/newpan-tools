package no.uit.metapipe.common.lib.dataset

import java.net.URI
import java.nio.file.Path

import no.uit.metapipe.common.utils.cleanup.{CleanUpPolicy, CleanUpOlderThan}
import no.uit.metapipe.common.lib.dataset.impl.MetaDescriptor

import scala.concurrent.Future
import scala.reflect.ClassTag

/**
  * The DatasetManager is responsible for keeping remote and local copies of a dataset in sync.
  * We assume only one cache level and the data is stored as files:
  *   - on disk (local)
  *   - on a remote server (remote)
  * Caching means bringing a copy on disk, while persisting means uploading a copy to the remote server.
  * Depending of the policy implemented, the dataset could be:
  *   - in full sync at any moment
  *   - in sync when needed
  *   - in sync only when asked to
  *
  *   Note: we took the party to use an asynchronous API because
  *   even though it might feel attractive to abstract away the remoteness, future force use to deal with exceptions,
  *   while it is very easy to forget to handle synchronous exception
  */
trait DatasetManagerLike[DatasetType <: ManagedDatasetLike] {

  /**
    * Tells if a dataset exists somewhere (is cached or persisted)
    */
  def isDefined(id: String): Future[Boolean]

  /**
    * Tells if a dataset is cached
    */
  protected[dataset] def isCached(id: String): Future[Boolean]

  /**
    * Tells where the data is stored remotely
    */
  protected[dataset] def dataUri(id: String): URI

  /**
    * Tells where the data is stored locally
    */
  protected[dataset] def dataPath(id: String): Path

  /**
    * Download the dataset locally (if not cached already)
    * If not persisted -> RuntimeException
    * Returns the local path to the data
    */
  protected def cacheDataset(id: String): Future[Unit]

  /**
    * Reload an existing (remote) dataset creating by another process (or from the same process in case we lost the handle)
    * No caching is done, so this operation is quick (caching will be performed next time the dataset is consumed)
    * Exception thrown if dataset does not exist (remotely)
    * Note: To avoid any cache inconsistency, if a file is present in the cache, it is silently deleted.
    */
  def reloadDataset(id: String): Future[DatasetType]

  /**
    * Load and cache the dataset (to be used exclusively when one wants to force downloading the data)
    * Exception thrown if dataset does not exist (locally and remotely)
    */
  //def forceCache(dataset: DatasetType): Future[DatasetType]

  /**
    * Creates a managed dataset from a DatasetForm
    * Throws RuntimeException if already exist
    * The newly created dataset is in sync
    * The resource pointed by the form is not deleted or moved.
    * Slow because of full syncing
    */
  def createDataset[T: ClassTag](id: String,
                                 form: DatasetFormLike): Future[DatasetType]

  /**
    * Cleans-up the dataset cache according to some clean-up policy
    */
  def cleanUp(cleanupPol: CleanUpPolicy): Unit = cleanupPol()
}
