package no.uit.metapipe.common.execution.pipeline

import no.uit.metapipe.common.execution.attempt.{Attempt, AttemptState}
import no.uit.metapipe.common.execution.execmanapi.SpecializedExecutionManagerApi
import no.uit.metapipe.common.execution.task.TaskState
import no.uit.metapipe.common.logging.Logging
import no.uit.metapipe.common.metadata.Version
import no.uit.sfb.apis.stateful.DirectedGraphLike
import no.uit.sfb.rxfsm.RxStatefulLike
import no.uit.sfb.scalautils.common.concurrency.DoEvery
import rx.{Ctx, Obs, Rx}

import scala.concurrent.ExecutionContext
import scala.concurrent.duration._
import scala.util.Try

/**
  * Responsible for the execution of a pipeline (= logic executed during an attempt), sending of heartbeat and update state of attempt to JobManager
  * Blocking and not reusable
  */
class PipelineExecutor[Params](
    attempt: Attempt[Params],
    val moduleGroup: ModuleGroupExecutor,
    val execApi: SpecializedExecutionManagerApi[AttemptState])(
    implicit ec: ExecutionContext,
    val ctx: Ctx.Owner)
    extends RxStatefulLike[AttemptState]
    with DirectedGraphLike[AttemptState, Obs]
    with Logging {
  protected lazy val _state: Rx[AttemptState] = Rx {
    StateConverter.toAttemptState(moduleGroup.stateRx())
  }

  lazy val id = moduleGroup.id

  def execute(): Unit = {
    log.debug(s"Running pipeline for attempt '${attempt.id}'")

    execApi.updateProperties(
      Map(
        "metapipe.newpantools.version" -> Version.newpantools.version.toString
      ))

    periodic.start()
    moduleGroup.execute()
  }

  def sendHeartbeatAndFetchJMState(): Unit = {
    Try { attempt.state() } map { jobManState =>
      if (jobManState.isTerminal) {
        if (jobManState != AttemptState.CANCELLED)
          log.warn(
            s"Unexpected terminal state '${jobManState.name}' from JobManager for attempt ${attempt.id} -- ignoring")
        else {
          log.info(
            s"Cancel order received from JobManager for attempt ${attempt.id} -- cancelling pipeline")
          cancel()
        }
      } else {
        execApi.sendHeartbeat()
      }
    }
  }

  onStateChange(s => execApi.updateState(s))

  val periodic = new DoEvery(1.seconds, sendHeartbeatAndFetchJMState(), false)

  def cancel(): Unit = {
    log.info(s"Cancelling pipeline for attempt ${attempt.id}...")
    moduleGroup.cancel()
  }

  override def finishWith(): Unit = {
    periodic.cancel()
    super.finishWith()
  }
}

object StateConverter {
  def toAttemptState(s: TaskState): AttemptState = {
    s match {
      case TaskState.NOT_STARTED => AttemptState.QUEUED_EXECUTOR
      case TaskState.WAITING_FOR_RESOURCE =>
        AttemptState.RUNNING //Since there is no transition from RUNNING -> QUEUED_EXECUTOR in JobManager
      case TaskState.RUNNING => AttemptState.RUNNING
      case TaskState.FINISHED =>
        AttemptState.RUNNING //Since FINISHED only means the executor finished, but the module still needs to complete
      case TaskState.COMPLETED      => AttemptState.FINISHED
      case TaskState.SKIPPED        => AttemptState.FINISHED
      case TaskState.CANCELLED      => AttemptState.CANCELLED
      case TaskState.FAILED         => AttemptState.EXECUTOR_FAILED
      case TaskState.INPUT_FAILED   => AttemptState.EXECUTOR_FAILED
      case TaskState.NO_CONTACT     => AttemptState.TIMED_OUT
      case TaskState.CRASHED        => AttemptState.WORKFLOW_FAILED
      case TaskState.MISSING_OUTPUT => AttemptState.WORKFLOW_FAILED
    }
  }

  def toTaskState(s: AttemptState): TaskState = {
    s match {
      case AttemptState.QUEUED_EXECUTOR => TaskState.NOT_STARTED
      case AttemptState.RUNNING         => TaskState.RUNNING
      case AttemptState.FINISHED        => TaskState.COMPLETED
      case AttemptState.CANCELLED       => TaskState.CANCELLED
      case AttemptState.EXECUTOR_FAILED => TaskState.FAILED
      case AttemptState.TIMED_OUT       => TaskState.NO_CONTACT
      case AttemptState.WORKFLOW_FAILED => TaskState.CRASHED
    }
  }
}
