package no.uit.metapipe.common.execution.executor

import no.uit.metapipe.common.execution.execmanapi.SpecializedExecutionManagerApi
import no.uit.metapipe.common.execution.task.TaskState
import no.uit.metapipe.common.logging.Logging

/**
  * Remote server spawning TaskExecutors when requested by some ProxyTaskExecutors
  */
abstract class ExecutionServer(autoStart: Boolean = false) extends Logging {
  def startServer(): Unit
  def stopServer(): Unit
  protected def newExecutor(
      prefix: String,
      implRef: String,
      execApi: SpecializedExecutionManagerApi[TaskState]): RemoteTaskExecutor

  if (autoStart)
    startServer()
}
