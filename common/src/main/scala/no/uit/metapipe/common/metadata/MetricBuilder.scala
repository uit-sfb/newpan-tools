package no.uit.metapipe.common.metadata

import no.uit.metapipe.common.utils._
import java.io.File
import java.nio.file.Path

import no.uit.metapipe.common.execution.context.AttemptCtx
import no.uit.metapipe.common.execution.context.ContextObj
import no.uit.metapipe.common.execution.context.DefaultCtxStatus
import no.uit.metapipe.common.execution.toolabs._
import no.uit.sfb.scalautils.common.FileUtils
import no.uit.sfb.scalautils.json.Json

import scala.collection.immutable.Map
import scala.util.Try

case class MetricNode(wallClockStatus: Option[DefaultCtxStatus],
                      processStatus: Option[ProcessStatus],
                      children: Option[Map[String, MetricNode]])

object MetricBuilder {
  def apply(ctx: AttemptCtx): Path = {

    val pathRoot = ctx.dir.getParent

    def parseStatusData[A: Manifest](data: Option[Path]): Option[A] = {
      data.flatMap { x => //flatten the structure to get data
        Try {
          Json.fromFileAs[A](x)
        }.toOption
      }
    }

    def buildTree(pathToRoot: Path): MetricNode = {
      def processDirectory(f: Path): (Seq[Path], Seq[Path]) = {
        val (directories, files) = FileUtils.ls(f)
        directories -> files
      }

      def getDirectoriesIfNotEmpty(
          d: Map[String, MetricNode]): Option[Map[String, MetricNode]] = {
        if (d.nonEmpty) Some(d)
        else None
      }

      def loop(f: Path): MetricNode = {
        val (directories, files) = processDirectory(f) //all subdirectories and all files of the directory
        lazy val filePath = (s: String) =>
          files.find(_.getFileName.toString == s) //lambda expression
        val subDirs = directories.map { dir =>
          dir.toString.split("/").last -> loop(dir)
        }.toMap
        MetricNode(
          parseStatusData[DefaultCtxStatus](filePath(ContextObj.moduleStatus)),
          parseStatusData[ProcessStatus](filePath(ContextObj.processStatus)),
          getDirectoriesIfNotEmpty(subDirs)
        )
      }
      loop(pathToRoot)
    }

    //We add the folder name of the top level directory
    val tree: Map[String, MetricNode] = Map(
      pathRoot.getFileName.toString -> buildTree(pathRoot))
    val path = pathRoot.resolve("metrics.json")
    Json.toFileAs(path, tree, true)
    path
  }
}
