package no.uit.metapipe.common.lib

import no.uit.metapipe.common.execution.task.TaskResult
import no.uit.metapipe.common.logging.Logging

/**
  * Throw an exception in case of failure
  */
trait TaskLogic
    extends ReferencedExecutableLogic[TaskParameter, TaskResult]
    with Logging
