package no.uit.metapipe.common.lib.dataset

/**
  * A form from which a dataset can be constructed
  */
trait DatasetFormLike {
  def resource: Any
}
