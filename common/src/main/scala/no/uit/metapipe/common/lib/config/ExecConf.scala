package no.uit.metapipe.common.lib.config

case class ExecConf(
    sparkConf: Map[String, String],
    packages: Option[String] = None,
    cleanupCache: Int = 30, //In days
    numThreads: Int = 1,
    parallelism: Int = 1,
    executionCtx: Int = -1,
    useDocker: Boolean = false //flag to use docker or default run
    //<0: ExecutionContext.global
    //0: Executors.newCachedThreadPool()
    //x>0: Executors.newFixedThreadPool(x)
)
