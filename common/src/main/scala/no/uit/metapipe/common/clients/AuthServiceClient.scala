package no.uit.metapipe.common.clients

import no.uit.metapipe.common.lib.config.Credentials

trait AuthServiceClient {
  def getTempCredentials(): Credentials
}
