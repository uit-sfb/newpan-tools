package no.uit.metapipe.common.lib.dataset.impl

import java.io.InputStream
import java.net.URI
import java.nio.file.{FileSystemException, Files, Path}

import no.uit.sfb.apis.storage.SingleBucketObjectStoreClientLike
import no.uit.metapipe.common.lib.dataset._
import no.uit.metapipe.common.logging.Logging

import scala.concurrent.blocking
import no.uit.metapipe.common.utils.cleanup.{CleanUpOlderThan, CleanUpPolicy}
import no.uit.sfb.s3store.{MinioS3Client, MinioS3SingleBucketClient}
import no.uit.sfb.scalautils.common.FutureUtils.RetryUntilTrue
import no.uit.sfb.scalautils.http.HttpClient
import no.uit.sfb.scalautils.common.{FileUtils, FutureUtils}
import no.uit.sfb.scalautils.json.Json

import scala.concurrent.duration._
import scala.concurrent.{ExecutionContext, Future}
import scala.reflect.ClassTag
import scala.util.Try
import sys.process._

/**
  * Those underlying files can be stored remotely or locally and can be:
  *   - sync'ed immediately upon creation
  *   - not sync'ed when reloaded: the caching will be performed when newStream is called (if needed), and the uploading should never be necessary
  *
  * Note: it might come as a surprise that datasets data fields do not have any extension. This is by design for the following reasons:
  *   - datasets are meant to be used via the dataset API and, as such, the files should not be looked at by a human
  *   - in case you seed() the dataset, one can give whatever extension. In addition, the type of data should be defined in the metadata
  *   - the problem of whether the data is compressed is tricky enough to not use extensions and leave the API deal with compression/decompression automatically
  */
case class CredentialsContainer(client: MinioS3Client,
                                bucket: String,
                                login: String,
                                password: String,
                                createBucketIfNotExist: Boolean = false)
    extends MinioS3SingleBucketClient(client, bucket, createBucketIfNotExist)

case class JsonDataContainer(status: String,
                             accessKey: String,
                             policyName: String,
                             userStatus: String)

class MetapipeDatasetManager(
    datasetsPath: Path,
    storageUri: URI,
    newStorageClient: () => SingleBucketObjectStoreClientLike)(
    implicit ec: ExecutionContext)
    extends DatasetManagerLike[MetapipeDataset]
    with Logging {

  implicit val datasetManager: MetapipeDatasetManager = this

  protected[dataset] def dataUri(id: String): URI = {
    new URI(id)
  }

  protected[dataset] def newStorage(): SingleBucketObjectStoreClientLike =
    newStorageClient()

  private def toLocalPath(uri: URI): Path = {
    val path = datasetsPath.resolve(storageUri.relativize(uri).toString)
    log.debug(s"$uri translated to $path")
    path
  }

  protected[dataset] def dataPath(id: String): Path = {
    toLocalPath(dataUri(id))
  }

  def isDefined(id: String): Future[Boolean] = {
    for {
      c <- isCached(id)
      p <- { implicit val stc = newStorageClient(); exists(id) }
    } yield {
      c || p
    }
  }

  protected[dataset] def isCached(id: String): Future[Boolean] = {
    Future {
      FileUtils.exists(dataPath(id))
    }
  }

  protected[dataset] def exists(id: String)(
      implicit stc: SingleBucketObjectStoreClientLike): Future[Boolean] = {
    stc.objectExists(dataUri(id))
  }

  private def fromDatasetMeta(id: String,
                              metadata: MetaDescriptor): MetapipeDataset = {
    new MetapipeDataset(id, metadata)
  }

  protected def cacheDataset(id: String): Future[Unit] = {
    implicit val stc = newStorageClient()
    for {
      cached <- isCached(id)
      persisted <- exists(id)
      _ <- {
        if (!cached) {
          log.info(s"Caching dataset '$id")
          if (!persisted)
            throw new RuntimeException(
              s"Dataset '$id' could not be cached because it does not exist remotely")
          else
            stc.getObject(dataUri(id), dataPath(id))
        } else {
          log.debug(s"Request to cache dataset '$id', but already cached")
          Future.successful(())
        }
      }
    } yield ()
  }

  def reloadDataset(id: String): Future[MetapipeDataset] = {
    log.info(s"Reloading dataset '$id'")
    implicit val stc = newStorageClient()
    stc.objectExists(dataUri(id)) flatMap { dataExists =>
      if (dataExists) {
        stc.getObjectMeta(dataUri(id)) map { meta =>
          fromDatasetMeta(id, MetaDescriptor.fromMap(meta.customMeta).get) //.get should be ok since it is checked in storage.exists
        }
      } else {
        throw new RuntimeException(s"No data found on remote: '$id'")
      }
    }
  }

  /**
    * Load and cache the dataset (to be used exclusively when one wants to force downloading the data)
    * Exception thrown if dataset does not exist (locally and remotely)
    */
  def forceCache(dataset: MetapipeDataset): Future[Unit] = {
    dataset.consume(_ => Future { blocking { Unit } })
  }

  def createDataset[T: ClassTag](
      id: String,
      form: DatasetFormLike): Future[MetapipeDataset] = {
    implicit val stc = newStorageClient()
    form match {
      case ref: FileDatasetForm =>
        createDatasetFromFile(id, ref)
      case ref: RemoteDatasetForm =>
        createDatasetFromRemote(id, ref)
      case ref: StreamDatasetForm =>
        createDatasetFromStream(id, ref)
    }
  }

  var tryCount = 0

  protected def createDatasetHelper(id: String,
                                    form: DatasetForm,
                                    createLocalCopy: => Future[Unit])(
      implicit stc: SingleBucketObjectStoreClientLike)
    : Future[MetapipeDataset] = {
    val fMeta = exists(id) flatMap { ex =>
      if (ex)
        throw new RuntimeException(
          s"Dataset creation failed as a dataset already exist at this location '${dataUri(id)}'")
      //We get hold on the data
      createLocalCopy.map { _ => //Do not write `createLocalCopy map` or it may not compile as it does not see the ExecutionContext
        form.descr
      }
    }

    fMeta flatMap { datasetMeta =>
      val f: Future[Unit] =
        FutureUtils.retry(
          stc.putObject(dataUri(id), dataPath(id), datasetMeta.toMap()),
          maxRetries = 99,
          recoverableException = { case e: Throwable => e }
        )
      f map { _ =>
        fromDatasetMeta(id, datasetMeta)
      }
    }
  }

  protected def createDatasetFromFile(id: String, form: FileDatasetForm)(
      implicit stc: SingleBucketObjectStoreClientLike)
    : Future[MetapipeDataset] = {
    log.info(s"Creating dataset '$id' from local reference: '$form'")
    createDatasetHelper(
      id,
      form,
      Future {
        (Try {
          FileUtils.hardLink(form.resource, dataPath(id))
        } recover {
          case _: FileSystemException =>
            FileUtils.copyFile(form.resource, dataPath(id))
        }).get
      }
    )
  }

  protected def createDatasetFromRemote(id: String, form: RemoteDatasetForm)(
      implicit stc: SingleBucketObjectStoreClientLike)
    : Future[MetapipeDataset] = {
    log.info(s"Creating dataset '$id' from remote reference: '$form'")
    val uri = form.resource
    val storageApiUri = storageUri.resolve("/minio") // /minio is needed as if not present, we consider this URL as public
    lazy val copyLocal =
      if (uri.toString.startsWith(storageApiUri.toString)) { //If the URI is within storageApiUri, we use MinioClient
        stc.getObject(storageApiUri
                        .resolve(storageApiUri.getPath + storageUri.getPath)
                        .relativize(uri),
                      dataPath(id))
      } else //Otherwise we you HttpClient
        Future {
          blocking {
            new HttpClient()
              .download(uri.toString, dataPath(id))
            ()
          }
        }
    createDatasetHelper(id, form, copyLocal)
  }

  protected def createDatasetFromStream(id: String, form: StreamDatasetForm)(
      implicit stc: SingleBucketObjectStoreClientLike)
    : Future[MetapipeDataset] = {
    log.info(s"Creating dataset '$id' from stream")
    createDatasetHelper(id, form, Future {
      FileUtils.streamToFile(dataPath(id), form.resource)
    })
  }

  /**
    * Returns a new input stream to a dataset's data.
    * If cached we return a FileInputStream to the data file
    * Otherwise we first cache then return a FileInputStream to the data file
    * Slow only first time
    */
  protected[dataset] def newStream(id: String): Future[InputStream] = {
    log.info(s"Accessing stream from dataset '$id'")
    cacheDataset(id) map { _ =>
      Files.newInputStream(dataPath(id))
    }
  }

  protected[dataset] def copy(id: String, toPath: Path): Unit = {
    if (FileUtils.exists(toPath) && FileUtils.isFile(toPath))
      log.info(s"File '$toPath' already seed")
    else {
      FileUtils.copyFile(dataPath(id), toPath) //Cannot use smart copy because of docker issue with symlinks
    }
  }

  override def cleanUp(
      cleanupPol: CleanUpPolicy = new CleanUpOlderThan(7.days, datasetsPath))
    : Unit = super.cleanUp(cleanupPol)
}
