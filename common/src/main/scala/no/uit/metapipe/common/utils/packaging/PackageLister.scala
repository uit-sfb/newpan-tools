package no.uit.metapipe.common.utils.packaging

import java.net.URL

import sys.process._

object PackageLister {
  def listAll(artifactsUrl: URL): Map[String, UnversionedPackage] = {
    val rep = s"curl -sL $artifactsUrl".!!
    val artifactList = (for {
      line <- rep.split("\n")
      packageName <- parseLine(line) if !packageName.contains(".tgz")
    } yield {
      val rep = s"curl -sL $artifactsUrl/$packageName".!!

      for {
        line <- rep.split("\n")
        artifactName <- parseLine(line)
        artifact <- Artifact.parse(artifactName)
      } yield artifact
    }).flatten

//    artifactList.foreach(f => println(s"Artifact ${f}")) //not inside

    artifactList.foldLeft(Map[String, UnversionedPackage]())(
      (acc, artifact) => {
        val newUp = acc.get(artifact.packageName) match {
          case Some(ud) =>
            ud.pVersions.get(artifact.pVersion) match {
              case Some(d) =>
                ud ++ (d ++ artifact.toDependency).toUnversionedPackage
              case None =>
                ud ++ artifact.toDependency.toUnversionedPackage
            }
          case None =>
            artifact.toDependency.toUnversionedPackage
        }
        acc + (newUp.packageName -> newUp)
      })
  }

  def listPackage(packageName: String,
                  artifactsUrl: URL): UnversionedPackage = {
    listAll(artifactsUrl)
      .getOrElse(packageName, UnversionedPackage(packageName, Map()))
  }

  def parseLine(str: String): Option[String] = {
    str.split("<a href=\"").toList match {
      case "" :: a :: Nil =>
        a.split("\">").headOption
      case _ =>
        None
    }
  }
}
