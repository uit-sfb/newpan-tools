package no.uit.metapipe.common.execution.toolabs

import java.io.{File, IOException}

import no.uit.metapipe.common.execution.toolabs.Defs.{
  Validator,
  ValidatorResult
}

import scala.sys.process.{Process, ProcessIO}

object ProcValidator
    extends ProcValidator(stdout = Nil, stderr = Nil, exitCode = Nil, cwd = ".")

case class ProcValidator private (
    private val stdout: List[Validator[String]] = Nil,
    private val stderr: List[Validator[String]] = Nil,
    private val exitCode: List[Validator[Int]] = Nil,
    private val cwd: String = "."
) {
  def exitCode(int: Int, msg: String = "unexpected exit code"): ProcValidator =
    exitCode(_ == int, msg)

  def exitCode(p: Int => Boolean, msg: String): ProcValidator =
    exitCode(Validator[Int](p, msg))

  def exitCode(p: Validator[Int]): ProcValidator = {
    this.copy(exitCode = p :: exitCode)
  }

  def stderr(p: String => Boolean, msg: String): ProcValidator =
    stderr(Validator(p, msg))

  def stderr(p: Validator[String]): ProcValidator = {
    this.copy(stderr = p :: stderr)
  }

  def stdout(p: String => Boolean, msg: String): ProcValidator =
    stdout(Validator(p, msg))

  def stdout(p: Validator[String]): ProcValidator = {
    this.copy(stdout = p :: stdout)
  }

  def cwd(cwd: String): ProcValidator = {
    this.copy(cwd = cwd)
  }

  def runValidate(argv: Seq[String]): ValidatorResult = {
    @volatile var stdoutRes = List[ValidationError]()
    @volatile var stderrRes = List[ValidationError]()

    val stdoutHandler = Outs.withString { out =>
      stdoutRes = stdout.flatMap(v => v(out))
    }

    val stderrHandler = Outs.withString { out =>
      stderrRes = stderr.flatMap(v => v(out))
    }

    val proc = Process(argv, new File(cwd))
    val processIO =
      new ProcessIO(in = Ins.close, out = stdoutHandler, err = stderrHandler)

    val ret = try {
      proc.run(processIO).exitValue()
    } catch {
      case e: IOException =>
        val cmd = argv.mkString(" ")
        return Seq(ValidationError(s"Failed to run $cmd. Cause: ${e.toString}"))
    }
    val retErr: List[ValidationError] = exitCode.flatMap(f => f(ret))
    stdoutRes ++ stderrRes ++ retErr
  }
}
