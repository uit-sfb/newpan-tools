package no.uit.metapipe.common.execution.executor.impl

import java.util.concurrent.CancellationException

import no.uit.metapipe.common.execution.executor.{
  ExecutionImplementation,
  TaskExecutor
}
import no.uit.metapipe.common.execution.pipeline.CancelException
import no.uit.metapipe.common.execution.task.TaskState
import no.uit.metapipe.common.logging.Logging
import no.uit.metapipe.common.lib.ExecutableLogic
import no.uit.sfb.scalautils.common.FutureUtils

import scala.concurrent.{ExecutionContext, Future}
import scala.util.Try
import scala.util.control.NonFatal

abstract class TaskExecutorImpl[P, T](prefix: String)(
    implicit val ec: ExecutionContext)
    extends TaskExecutor[P, T]
    with ExecutionImplementation[P, T]
    with Logging {
  final val taskId: String = s"$prefix.executor"

  protected lazy val initialState = TaskState.initialState

  protected def logic: ExecutableLogic[P, T]

  final def isCompleted: Boolean = state.isTerminal

  final def execute(p: P): Future[T] = {
    val f: Future[T] = FutureUtils
      .fromTry(Try {
        tryTransitionTo(TaskState.RUNNING)
        executeImpl(p)
      })
      .flatMap(identity) //.flatten scala 2.12
    f transform (res => {
      tryTransitionTo(TaskState.FINISHED)
      res
    }, {
      case ex: CancellationException =>
        log.info(s"Execution of '$taskId' cancelled")
        throw CancelException
      case NonFatal(ex) =>
        tryTransitionTo(TaskState.FAILED)
        log.warn(s"Task '$taskId' failed", ex)
        ex
    })
  }

  final def cancel(): Unit = {
    failedWith(TaskState.CANCELLED)
    cancelImpl()
  }
}
