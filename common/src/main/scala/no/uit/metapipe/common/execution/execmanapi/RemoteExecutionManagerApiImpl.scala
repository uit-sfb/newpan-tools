package no.uit.metapipe.common.execution.execmanapi

import no.uit.metapipe.common.clients._
import no.uit.metapipe.common.execution.attempt.{Attempt, AttemptState}
import no.uit.metapipe.common.lib.dataset.impl.{DatasetForm, MetapipeDataset}
import no.uit.metapipe.common.lib.dataset.DatasetManagerLike
import no.uit.metapipe.common.logging.Logging

import scala.concurrent.{ExecutionContext, Future}
import scala.util.{Failure, Success, Try}
import scala.util.control.NonFatal

class SpecializedExecutionApiImpl(
    attempt: Attempt[_],
    jobServiceClient: JobServiceClient,
    datasetManager: DatasetManagerLike[MetapipeDataset],
    storageUriGenerator: StorageUriGenerator
)(implicit ec: ExecutionContext)
    extends SpecializedExecutionApi
    with Logging {
  def fetchDataset(key: String): Future[Option[MetapipeDataset]] = {
    val inputs = attempt.inputs
    val outputs = attempt.outputs()
    val oRef = (inputs ++ outputs).get(key)
    oRef match {
      case Some(ref) =>
        log.info(s"Fetching dataset '$key' from ${ref.url}")
        datasetManager.reloadDataset(ref.url) map { Some(_) }
      case None =>
        log.info(s"Did not find dataset '$key' in inputs(${inputs
          .mkString(", ")} or outputs(${outputs.mkString(", ")}))")
        val uriOut = storageUriGenerator.outputUri(attempt, key)
        log.info(
          s"Trying to recover dataset '$key' from remote origin '$uriOut'")
        datasetManager.reloadDataset(uriOut) map { Some(_) } recoverWith {
          case NonFatal(_) =>
            val uriInterm = storageUriGenerator.intermUri(attempt, key)
            log.info(
              s"Trying to recover dataset '$key' from remote origin '$uriInterm'")
            datasetManager.reloadDataset(uriInterm) map { Some(_) } recover {
              case NonFatal(_) =>
                log.info(s"Could not find dataset '$key' despite best effort")
                None
            }
        }
    }
  }

  def pushDataset(key: String, ref: DatasetForm): Future[MetapipeDataset] = {
    val outputRef = DatasetUrl(
      url =
        if (ref.descr.tags.contains("output"))
          storageUriGenerator.outputUri(attempt, key)
        else
          storageUriGenerator.intermUri(attempt, key)
    )
    log.info(s"Uploading dataset $key to ${outputRef.url}")
    val fDataset =
      datasetManager.createDataset(outputRef.url, ref)
    //Register output to JobManager
    fDataset
      .onComplete { //onSuccess removed as deprecated, used onComplete instead
        case Success(_) =>
          jobServiceClient.registerOutput(attempt.attemptUri, key, outputRef)
        case Failure(e) =>
          log.error(
            "error occured while trying to push dataset: " + e.getMessage)
      }
    fDataset
  }

  def updateProperties(properties: Map[String, String]): Unit = {
    jobServiceClient.updateProperties(attempt.attemptUri, properties)
  }
}

class SpecializedExecutionManagerApiImpl(
    attempt: Attempt[_],
    jobServiceClient: JobServiceClient,
    datasetManager: DatasetManagerLike[MetapipeDataset],
    storageUriGenerator: StorageUriGenerator
)(implicit ec: ExecutionContext)
    extends SpecializedExecutionApiImpl(
      attempt,
      jobServiceClient,
      datasetManager,
      storageUriGenerator
    )
    with SpecializedExecutionManagerApi[AttemptState]
    with Logging {
  def sendHeartbeat(): Boolean = {
    Try {
      jobServiceClient.sendHeartbeat(attempt.attemptUri)
    } match {
      case Success(_) => true
      case Failure(e) =>
        log.warn(s"Sending of heartbeat failed due to error: ${e.getMessage}")
        false
    }
  }

  def updateState(state: AttemptState): Unit = {
    jobServiceClient.updateAttemptState(attempt.attemptUri, state)
  }
}
