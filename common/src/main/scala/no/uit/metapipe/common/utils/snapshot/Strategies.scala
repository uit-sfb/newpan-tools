package no.uit.metapipe.common.utils.snapshot

import java.nio.file.Path

import no.uit.sfb.scalautils.common.FileUtils
import no.uit.sfb.scalautils.json.Json

class NoopSnapshotStrategy[T] extends SnapshotStrategy[T] {
  override def save(identifier: String, t: T): Unit = {}

  override def restore(identifier: String): Option[T] = None
}

class InMemoryCacheSnapshotStrategy[T] extends SnapshotStrategy[T] {
  var map: Map[String, T] = Map()

  override def save(identifier: String, t: T): Unit = {
    map = map ++ Map[String, T](identifier -> t)
  }

  override def restore(identifier: String): Option[T] = {
    map.get(identifier)
  }
}

class InFileSnapshotStrategy[T <: AnyRef: Manifest](path: Path)
    extends SnapshotStrategy[T] {
  protected def toFile(filePath: Path, t: T): Unit = {
    Json.toFileAs(filePath, t)
  }

  protected def fromFile(filePath: Path): T = {
    Json.fromFileAs(filePath)
  }

  override def save(identifier: String, t: T): Unit = {
    val finalPath = getPath(identifier)
    val tempPath = path.resolve(s"$identifier.tmp")
    toFile(tempPath, t)
    FileUtils.moveFile(tempPath, finalPath)
  }

  override def restore(identifier: String): Option[T] = {
    val p = getPath(identifier)
    if (FileUtils.isFile(p))
      Some(fromFile(p))
    else
      None
  }

  def getPath(identifier: String): Path = path.resolve(identifier)
}
