/*package no.uit.metapipe.common.execution.execmanapi

import com.rabbitmq.client.Connection
import no.uit.metapipe.common.clients.DatasetUrl
import no.uit.metapipe.common.execution.task.{
  TaskOrder,
  TaskState,
  TaskStatusUpdate
}
import no.uit.metapipe.common.execution.executor.impl.rabbitmq.StatusMessageSender
import no.uit.metapipe.common.lib.dataset.impl.{MetaDescriptor, MetapipeDataset}
import no.uit.metapipe.common.lib.dataset.{DatasetFormLike, DatasetManagerLike}
import no.uit.metapipe.common.logging.Logging

import scala.concurrent.{ExecutionContext, Future}

class RabbitmqExecutionManagerApiImpl(
    task: TaskOrder[String],
    datasetManager: DatasetManagerLike[MetapipeDataset],
    conn: Connection,
    vhost: String)(implicit ec: ExecutionContext)
    extends SpecializedExecutionManagerApi[TaskState]
    with Logging {
  private val channel = conn.createChannel()
  private val statusSender = new StatusMessageSender(vhost, channel)
  statusSender.exchangeDeclare()

  def close(): Unit = channel.close()

  protected def sendStatusUpdate(statusUpdate: TaskStatusUpdate): Unit = {
    statusSender.publish(task.id, statusUpdate)
  }

  def fetchDataset(key: String): Future[Option[MetapipeDataset]] = {
    datasetManager.reloadDataset(task.parameters.inputs(key)) map {
      Some(_)
    }
  }

  def pushDataset(key: String,
                  ref: DatasetFormLike): Future[MetapipeDataset] = {
    val uri = task.outputsUriPattern
      .replace(":key:", StorageUriGenerator.keyToFileName(key))
    val fDataset =
      datasetManager.createDataset(uri, ref)
    fDataset map { x =>
      sendStatusUpdate(
        TaskStatusUpdate(outputs = Some(Map(key -> DatasetUrl(uri)))))
      x
    }
  }

  def sendHeartbeat(): Boolean = {
    sendStatusUpdate(TaskStatusUpdate())
    true
  }

  def updateProperties(properties: Map[String, String]): Unit = {
    sendStatusUpdate(TaskStatusUpdate(notifications = Some(properties)))
  }

  def updateState(newState: TaskState): Unit = {
    sendStatusUpdate(TaskStatusUpdate(state = Some(newState)))
  }
}
 */
