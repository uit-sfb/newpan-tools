package no.uit.metapipe.common.utils.packaging

case class UnversionedPackage(packageName: String,
                              pVersions: Map[String, Package]) {
  def ++(other: UnversionedPackage): UnversionedPackage = {
    if (packageName != other.packageName)
      throw new Exception(
        s"Cannot compare not related dependencies (${other.packageName} != $packageName)")
    else
      this.copy(pVersions = pVersions ++ other.pVersions)
  }

  lazy val latest: Package = {
    if (pVersions.nonEmpty) {
      pVersions.values.maxBy(_.pVersion)(GenericVersion)
    } else
      throw new Exception(
        s"Could not find any version for package '$packageName'")
  }

  def get(oDepVersion: Option[String], oVersion: Option[String]): Artifact = {
    val dep = oDepVersion match {
      case Some(odv) =>
        pVersions.getOrElse(
          odv,
          throw new Exception(s"Version $odv not found for $packageName"))
      case None => latest
    }
    dep.get(oVersion)
  }
}

case class Package(packageName: String,
                   pVersion: String,
                   aVersions: Map[String, Artifact]) {
  def ++(other: Package): Package = {
    if (packageName != other.packageName)
      throw new Exception(
        s"Cannot compare not related dependencies (${other.packageName} != $packageName)")
    else if (pVersion != other.pVersion)
      throw new Exception(
        s"Cannot compare not related dependencies (${other.packageName} != $packageName)")
    else
      this.copy(aVersions = aVersions ++ other.aVersions)
  }

  lazy val toUnversionedPackage =
    UnversionedPackage(packageName, Map(pVersion -> this))

  lazy val latest: Artifact = {
    if (aVersions.nonEmpty)
      aVersions.values.maxBy(_.aVersion)(GenericVersion)
    else
      throw new Exception(
        s"Could not find any artifact version for package '${packageName}_$pVersion'")
  }

  def get(oVersion: Option[String]): Artifact = {
    oVersion match {
      case Some(ov) =>
        aVersions.getOrElse(
          ov,
          throw new Exception(
            s"Version $ov not found for $packageName/$pVersion"))
      case None => latest
    }
  }
}
