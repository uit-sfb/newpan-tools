package no.uit.metapipe.common.metadata

case class Provenance(tool: VersionInfo, databases: Seq[VersionInfo])
