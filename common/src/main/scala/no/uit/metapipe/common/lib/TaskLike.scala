package no.uit.metapipe.common.lib

/**
  * This is the abstraction for a unit of work (workflow attempt, task, ...)
  */
trait TaskLike[+Params] {
  def id: String
  def parameters: Params
}
