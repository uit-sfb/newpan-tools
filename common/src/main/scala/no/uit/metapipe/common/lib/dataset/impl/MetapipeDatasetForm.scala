package no.uit.metapipe.common.lib.dataset.impl

import java.io.BufferedInputStream
import java.net.URI
import java.nio.file.Path

import no.uit.metapipe.common.lib.dataset.DatasetFormLike

/**
  * Contrary to DatasetRef, a DatasetForm does not point to a resource managed by the DatasetManager
  * DatasetForms are used as input to DatasetManager to produce managed Datasets
  */
trait DatasetForm extends DatasetFormLike {
  val descr: MetaDescriptor
}

/**
  * DatasetForm from local file containing the data
  */
case class FileDatasetForm(
    resource: Path,
    descr: MetaDescriptor
) extends DatasetForm

/**
  * DatasetForm from URI of the remote file containing the data
  */
case class RemoteDatasetForm(
    resource: URI,
    descr: MetaDescriptor
) extends DatasetForm

/**
  * DatasetForm from InputStream containing the data
  * Note: We use a lazy val resource so that we open the stream only when needed and at use time
  * (otherwise we may experience that the stream producer) closes the stream after a timeout.
  */
class StreamDatasetForm(
    _resource: => BufferedInputStream,
    val descr: MetaDescriptor
) extends DatasetForm {
  lazy val resource = _resource

  def unapply(arg: StreamDatasetForm): Boolean = true
}
