package no.uit.metapipe.common.execution.pipeline

import no.uit.metapipe.common.execution.context.ContextLike
import no.uit.metapipe.common.execution.execmanapi.SpecializedExecutionApi
import no.uit.metapipe.common.execution.task.TaskState
import no.uit.metapipe.common.logging.Logging
import no.uit.sfb.rxfsm.{RxStatefulLike, RxUtils}
import rx.{Ctx, Rx}

import scala.concurrent.ExecutionContext

/**
  * Responsible for the execution of a pipeline (= logic executed during an attempt), sending of heartbeat and update state of attempt to JobManager
  * Blocking and not reusable
  */
class ModuleGroupExecutor(val context: ContextLike,
                          val dependencyList: Seq[ModuleLike[TaskState]],
                          val execApi: SpecializedExecutionApi)(
    implicit ec: ExecutionContext,
    val ctx: Ctx.Owner)
    extends ModuleLike[TaskState]
    with RxStatefulLike[TaskState]
    with Logging {

  //Reacts to the change of any input
  private lazy val inputRx = RxUtils.sequence(dependencyList.map { _.stateRx })

  //inputRx.trigger(log.warn(s"${inputRx.now}"))

  //Reacts only the the aggregate change
  protected lazy val _state: Rx[TaskState] =
    if (dependencyList.isEmpty)
      Rx { TaskState.COMPLETED } else
      inputRx map { _.reduce(aggregate) }

  def aggregate(s1: TaskState, s2: TaskState): TaskState =
    StateUtils.merge(s1, s2)

  lazy val moduleName: String = context.identifier.name

  def execute(): Unit = {

    /**
      * The modules can all be started altogether since they are plugged together via their fInputs/pOutputs.
      * The real execution would actually not start before all the inputs are ready.
      */
    dependencyList foreach {
      _.execute()
    }
  }

  onStateChange(s => execApi.updateProperties(Map(s"state.$id" -> s.name)))

  override final def cancel(): Unit = {
    super.cancel()
    dependencyList foreach { _.cancel() }
  }
}

object StateUtils extends Logging {

  /**
    * Optimist state mapping: If an error state is present in the list, and at least one module is in a non-terminal state,
    * we don't report the error.
    * How ever as soon as there is no non-terminal state left, the error win over the Finished modules.
    * CANCELLED wins over all but CRASHED.
    *
    *       NS  WR  R   Fi  Sk  Cp   C   Fa  IF  NC  Cr  MO
    *
    * NS    .
    * WR    WR  .
    * R     R   R   .
    * Fi    R   WR  R   .
    * Sk    WR  WR  R   Fi  .
    * Cp    WR  WR  R   Fi  Cp  .
    * C     C   C   C   C   C   C   .
    * Fa    WR  WR  R   Fi  Fa  Fa  C   .
    * IF    WR  WR  R   Fi  IF  IF  C   Fa  .
    * NC    WR  WR  R   Fi  NC  NC  C   NC  NC  .
    * Cr    WR  WR  R   Fi  Cr  Cr  Cr  Cr  Cr  Cr  .
    * MO    WR  WR  R   Fi  MO  MO  MO  MO  MO  MO  MO  .
    */
  def merge(s1: TaskState, s2: TaskState): TaskState = {
    def halfLookUp(s1: TaskState, s2: TaskState): TaskState = { //Only the half LUT incl diagonal
      (s1, s2) match {
        case (TaskState.NOT_STARTED, TaskState.WAITING_FOR_RESOURCE) =>
          TaskState.WAITING_FOR_RESOURCE
        case (TaskState.NOT_STARTED, TaskState.RUNNING) => TaskState.RUNNING
        case (TaskState.NOT_STARTED, TaskState.FINISHED) =>
          TaskState.RUNNING
        case (TaskState.NOT_STARTED, TaskState.SKIPPED) =>
          TaskState.WAITING_FOR_RESOURCE
        case (TaskState.NOT_STARTED, TaskState.COMPLETED) =>
          TaskState.WAITING_FOR_RESOURCE
        case (TaskState.WAITING_FOR_RESOURCE, TaskState.RUNNING) =>
          TaskState.RUNNING
        case (TaskState.RUNNING, TaskState.FINISHED) => TaskState.RUNNING
        case (_, TaskState.CANCELLED)                => TaskState.CANCELLED
        case (TaskState.NOT_STARTED, _)              => TaskState.WAITING_FOR_RESOURCE
        case (TaskState.WAITING_FOR_RESOURCE, _) =>
          TaskState.WAITING_FOR_RESOURCE
        case (TaskState.RUNNING, _)                     => TaskState.RUNNING
        case (TaskState.FINISHED, _)                    => TaskState.FINISHED
        case (TaskState.SKIPPED, x)                     => x
        case (TaskState.COMPLETED, x)                   => x
        case (_, TaskState.CRASHED)                     => TaskState.CRASHED
        case (_, TaskState.MISSING_OUTPUT)              => TaskState.MISSING_OUTPUT
        case (TaskState.CANCELLED, _)                   => TaskState.CANCELLED
        case (TaskState.FAILED, TaskState.INPUT_FAILED) => TaskState.FAILED
        case (TaskState.FAILED, x)                      => x
        case (TaskState.NO_CONTACT, TaskState.INPUT_FAILED) =>
          TaskState.NO_CONTACT
        case (x, _) => x //Includes remaining cases and diagonal
      }
    }
    if (TaskState.lteq(s1, s2))
      halfLookUp(s1, s2)
    else {
      halfLookUp(s2, s1)
    }
  }

  /*def toGlobalState(stateList: Seq[TaskState]): TaskState = {
    stateList.headOption match {
      case Some(st) =>
        val tail = stateList.tail
        if (tail.isEmpty)
          st //We actually want to use FINISHED only in the case where there are no modules at all
        else
          merge(st, toGlobalState(stateList.tail))
      case None => TaskState.FINISHED
    }
  }*/
}
