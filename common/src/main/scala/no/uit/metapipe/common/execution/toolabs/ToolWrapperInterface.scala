package no.uit.metapipe.common.execution.toolabs

import java.io._

import no.uit.metapipe.common.execution.context.ModuleCtx
import no.uit.metapipe.common.logging.Logging

import scala.language.existentials

trait ToolWrapperInterface[Input, Output] extends Serializable with Logging {
  def moduleCtx: ModuleCtx
  def runName: Option[String]

  def tool: ToolWrapper[_, _]

  final lazy val ctxName = runName match {
    case Some(postfix) => s"${tool.toolName}-$postfix"
    case None          => tool.toolName
  }

  def execute(in: Input, partitionIdx: Option[Int]): Output

  final def newToolCtx(partitionIdx: Option[Int]) =
    moduleCtx.newToolCtx(ctxName, partitionIdx)
}
