package no.uit.metapipe.common.lib.dataset

/**
  * An UnmanagedDatasetLike is a dataset not managed by a DatasetManagerLike.
  */
trait UnmanagedDatasetLike extends DatasetLike {}
