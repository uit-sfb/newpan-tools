package no.uit.metapipe.common.clients

sealed abstract class MetapipeClientException(description: String,
                                              cause: Exception = null)
    extends Exception(description, cause)

case class NotFoundException(description: String, cause: Exception = null)
    extends MetapipeClientException(description, cause)
case class NotAuthorizedException(description: String, cause: Exception = null)
    extends MetapipeClientException(description, cause)
case class MaxRetriedException(description: String, cause: Exception = null)
    extends MetapipeClientException(description, cause)
