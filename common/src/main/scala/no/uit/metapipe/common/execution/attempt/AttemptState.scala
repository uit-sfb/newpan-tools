package no.uit.metapipe.common.execution.attempt

/**
  * DO NOT CHANGE:
  * This file is based on JobManager/AttemptState.java
  */
import no.uit.metapipe.common.utils.fsm.{FsmState, FsmStateCompanion}

import scala.language.implicitConversions

/**
  * This is the state definitions used by JobManager.
  */
abstract class AttemptState extends FsmState {
  lazy val isFailure = AttemptState.failureStates contains this
}

object AttemptState extends FsmStateCompanion[AttemptState] {
  //Because Set is not invariant =(
  implicit def convert(as: Set[AttemptState]): Set[FsmState] = as map {
    _.asInstanceOf[FsmState]
  }

  private[attempt] lazy val failureStates: Set[AttemptState] = Set(
    CANCELLED,
    TIMED_OUT,
    WORKFLOW_FAILED,
    EXECUTOR_FAILED
  )

  /**
    * The attempt is in the JobManager queue and is ready to be assigned to an executor.
    * This is the initial state of an attempt.
    */
  case object QUEUED_LOCALLY extends AttemptState {
    val nextStates: Set[FsmState] = Set(
      ASSIGNED_EXECUTOR,
      CANCELLED
    )
  }

  /**
    * The attempt has been assigned to a specific executor.
    * This means that either of these conditions hold true:
    *  1. It has been decided which executor should run the attempt. In this case
    *     the attempt should be in that executor's queue
    *  2. An executor has just read this attempt from it's queue.
    */
  case object ASSIGNED_EXECUTOR extends AttemptState {
    val nextStates: Set[FsmState] = Set(
      QUEUED_EXECUTOR,
      RUNNING,
      FINISHED
    ) ++ failureStates
  }

  /**
    * The attempt has been read from the executor queue, but is queued either by the executor
    * or by a job scheduler system that is being used by the executor.
    */
  case object QUEUED_EXECUTOR extends AttemptState {
    val nextStates: Set[FsmState] = Set(
      RUNNING,
      FINISHED
    ) ++ failureStates
  }

  /**
    * The executor has reported that it is processing the attempt
    */
  case object RUNNING extends AttemptState {
    val nextStates: Set[FsmState] = Set(
      FINISHED
    ) ++ failureStates
  }

  /**
    * The executor has reported that the attempt finished successfully
    */
  case object FINISHED extends AttemptState {
    val nextStates: Set[FsmState] = Set()
  }

  // Admin intervention

  /**
    * An admin (or a part of the system) has cancelled this attempt.
    */
  case object CANCELLED extends AttemptState {
    val nextStates: Set[FsmState] = Set()
  }

  // Job failure

  /**
    * The attempt has not received a heart-beat from an executor within the
    * set time-out period. The attempt is assumed to have failed.
    *
    * It is assumed that the error is transient and that the job can be retried (and succeed) at
    * a later time without admin intervention.
    */
  case object TIMED_OUT extends AttemptState {
    val nextStates: Set[FsmState] = Set()
  }

  /**
    * The executor has reported a failure during the workflow execution.
    *
    * Typically caused by an exception being thrown by a tool parser (a bug) or a tool crashing during
    * execution.
    *
    * It is assumed that the failure is systematic and that there is no point in retrying automatically.
    *
    * This state requires an admin (or a developer) to investigate the cause of the error, possibly fix a bug or a
    * deployment issue, and then create a new attempt when the cause has been resolved.
    */
  case object WORKFLOW_FAILED extends AttemptState {
    val nextStates: Set[FsmState] = Set()
  }

  /**
    * The executor has reported a failure that is not related to the workflow execution itself.
    *
    * This could be caused by a dependency becoming unavailable such as a job scheduler or
    * a compute node going offline during the execution of a job or similar.
    *
    * It is assumed that the error is transient and that the job can be retried (and succeed) at
    * a later time without admin intervention.
    */
  case object EXECUTOR_FAILED extends AttemptState {
    val nextStates: Set[FsmState] = Set()
  }

  lazy val states = Seq(QUEUED_LOCALLY,
                        ASSIGNED_EXECUTOR,
                        QUEUED_EXECUTOR,
                        RUNNING,
                        FINISHED,
                        CANCELLED,
                        TIMED_OUT,
                        WORKFLOW_FAILED,
                        EXECUTOR_FAILED)

  lazy val initialState: AttemptState = QUEUED_LOCALLY
}
