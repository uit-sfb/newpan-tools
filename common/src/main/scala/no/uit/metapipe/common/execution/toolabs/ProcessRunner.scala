package no.uit.metapipe.common.execution.toolabs

import java.io.{InputStream, OutputStream}
import java.nio.charset.StandardCharsets
import java.nio.file.Path

import no.uit.metapipe.common.execution.context.{
  StartStopTime,
  WallClockTimeInfo
}
import no.uit.metapipe.common.logging.Logging
import no.uit.metapipe.common.metadata.{Provenance, VersionInfo}
import no.uit.sfb.scalautils.common.FileUtils
import no.uit.sfb.scalautils.json.Json
import org.apache.commons.io.IOUtils

import scala.concurrent.duration._
import scala.sys.process.{Process, ProcessIO}

case class NonZeroStatusCodeException(statusCode: Int)
    extends RuntimeException(
      s"Procedure returned non-zero status ($statusCode)")

case class CpuTimeInfo(percentOfCpuUsed: Long,
                       cpuTimeKernelMilliSec: Long,
                       cpuTimeUserMilliSec: Long,
                       wallClockTimeMilliSec: Long,
                       maxResidentSetSizeKilobytes: Long,
                       SwappedOutOfMainMemory: Int,
                       ContextSwitchedInvoluntarily: Int,
                       ContextSwitchedVoluntarily: Int)

object CpuTimeInfo {
  def fromFile(p: Path): CpuTimeInfo = {
    lazy val timeToLong = (n: Double) => (n * 1000).toLong
    val getInfoFromFile: Array[String] = {
      val timeInfo = FileUtils.readFile(p).split("\n").last //In case the command fails, it would write "Command exited with non-zero status x" at the first line.
      //So we take the last line of the file so that we know it is the timing info for sure.
      FileUtils.deleteFile(p)
      timeInfo.split(",")
    }
    val Array(cpuPercent,
              cpuKernel,
              cpuUser,
              elapsedReal,
              residentSetSize,
              swappedOut,
              contextSwitchedInv,
              contextSwitchedV) = getInfoFromFile
    CpuTimeInfo(
      cpuPercent.take(cpuPercent.length - 1).toLong,
      timeToLong(cpuKernel.toDouble),
      timeToLong(cpuUser.toDouble),
      timeToLong(elapsedReal.toDouble),
      residentSetSize.toLong,
      swappedOut.toInt,
      contextSwitchedInv.toInt,
      contextSwitchedV.toInt
    )
  }
}

case class ProcessStatus(
    exitCode: Int,
    cmd: String,
    provenance: Provenance,
    metrics: Option[ProcessMetrics]
)
case class ProcessMetrics(wallClock: WallClockTimeInfo,
                          cpuTimeInfo: CpuTimeInfo)

class ProcessRunner(cwd: Path,
                    statusPath: Path,
                    stdout: InputStream => Unit = Outs.close,
                    stderr: InputStream => Unit = Outs.close,
                    stdin: OutputStream => Unit = Ins.close)
    extends Logging {

  def call(toolPath: Path,
           args: Seq[String],
           caller: Seq[String] = Seq(),
           env: Map[String, String] = Map(),
           toolVersionPath: Path,
           databaseVersionPaths: Seq[Path] = Seq()): Int = {
    //provenance
    val provenance = Provenance(
      VersionInfo.fromVersionFile(toolVersionPath),
      databaseVersionPaths map { path =>
        VersionInfo.fromVersionFile(path)
      }
    )

    val temporaryFileName = "time.tmp"
    val cmd: Seq[String] = (caller :+ toolPath.toString) ++ args
    val proc =
      Process(
        "/usr/bin/time" +: "-o" +: s"$temporaryFileName" +: "-f" +: "%P,%S,%U,%e,%M,%W,%c,%w" +: cmd,
        cwd.toFile,
        env.toSeq: _*)
    val processIO = new ProcessIO(stdin, stdout, stderr)

    val tBefore = System.currentTimeMillis()
    val ret = proc.run(processIO).exitValue()
    val tAfter = System.currentTimeMillis()

    val metrics = ProcessMetrics(
      StartStopTime(tBefore.millis, tAfter.millis).wallClockTimeInfo,
      CpuTimeInfo.fromFile(cwd.resolve(temporaryFileName))
    )

    Json.toFileAs[ProcessStatus](
      statusPath,
      ProcessStatus(ret, cmd.mkString(" "), provenance, Some(metrics)))

    ret
  }

  def callAndThrow(toolPath: Path,
                   args: Seq[String],
                   caller: Seq[String] = Seq(),
                   env: Map[String, String] = Map(),
                   toolVersionPath: Path,
                   databaseVersionPaths: Seq[Path] = Seq()): Unit = {
    val code =
      call(toolPath, args, caller, env, toolVersionPath, databaseVersionPaths)
    if (code != 0)
      throw NonZeroStatusCodeException(code)
  }
}

object Ins {
  val close: OutputStream => Unit = out => out.close()
}

object Outs {
  type OutputHandler = InputStream => Unit

  val close: OutputHandler = in => in.close()
  def redirect(path: Path): OutputHandler = { in =>
    FileUtils.streamToFile(path, in)
  }

  def withString(f: String => Unit): OutputHandler = { in =>
    val str = IOUtils.toString(in, StandardCharsets.UTF_8)
    in.close()
    f(str)
  }
}
