package no.uit.metapipe.common.execution.context

import scala.concurrent.duration._

trait ContextStatusLike {
  def wallClock: WallClockTimeInfo
}

case class StartStopTime(start: Duration, stop: Duration) {
  lazy val wallClockTimeInfo =
    WallClockTimeInfo(start.toMillis, stop.toMillis, (stop - start).toMillis)
}

case class WallClockTimeInfo(start: Long, stop: Long, durationMillis: Long)

case class DefaultCtxStatus(finalState: String,
                            wallClock: WallClockTimeInfo,
                            cpuTime: Option[Long] = None)
    extends ContextStatusLike
