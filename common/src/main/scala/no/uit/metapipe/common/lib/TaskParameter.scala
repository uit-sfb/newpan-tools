package no.uit.metapipe.common.lib

import no.uit.metapipe.common.execution.task.GenericTaskOrder
import no.uit.metapipe.common.lib.dataset.impl.MetapipeDataset

case class TaskParameter(taskRef: GenericTaskOrder[String],
                         datasets: Map[String, MetapipeDataset])
