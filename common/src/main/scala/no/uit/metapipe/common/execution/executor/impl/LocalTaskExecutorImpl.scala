package no.uit.metapipe.common.execution.executor.impl

import no.uit.metapipe.common.execution.context.ModuleCtx
import no.uit.metapipe.common.execution.executor.DetachedExecution
import no.uit.metapipe.common.lib.ExecutableLogic
import rx.Ctx

import scala.concurrent.ExecutionContext

/**
  * Local executor (Non-blocking)
  * @param taskId
  * @param logic The logic to be executed by this executor
  * @tparam P Type of the parameter
  * @tparam T Type of the returned value
  */
class LocalTaskExecutorImpl[P, T](
    moduleCtx: ModuleCtx,
    val logic: ExecutableLogic[P, T]
)(implicit ec: ExecutionContext, val ctx: Ctx.Owner)
    extends TaskExecutorImpl[P, T](moduleCtx.identifier.toString)
    with DetachedExecution[P, T] {
  protected val oModuleCtx = Option(moduleCtx)
}
