package no.uit.metapipe.common.execution.task

import no.uit.metapipe.common.clients.TaskOrderDto
import no.uit.metapipe.common.lib.TaskLike
import no.uit.sfb.scalautils.json.Json

/**
  * Task order free of any logic/implementation
  */
case class GenericTaskOrder[Params](id: String, parameters: Params)
    extends TaskLike[Params]

/**
  * Task order with defined logic/implementation
  */
case class TaskOrder[Params <: AnyRef: Manifest](id: String,
                                                 implRef: String,
                                                 parameters: Params)
    extends TaskLike[Params] {
  lazy val toDto: TaskOrderDto = {
    TaskOrderDto(
      id,
      implRef,
      Json.serialize[Params](parameters)
    )
  }
  lazy val toGenericTaskOrder: GenericTaskOrder[Params] =
    GenericTaskOrder(id, parameters)
}

object TaskOrder {
  def fromTaskDto[Params <: AnyRef: Manifest](
      dto: TaskOrderDto): TaskOrder[Params] = {
    TaskOrder[Params](
      dto.id,
      dto.implRef,
      Json.parse[Params](dto.parameters)
    )
  }
}
