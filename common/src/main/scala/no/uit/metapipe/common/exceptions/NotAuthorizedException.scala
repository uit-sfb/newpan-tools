package no.uit.metapipe.common.exceptions

class NotAuthorizedException(message: String) extends Exception(message)
