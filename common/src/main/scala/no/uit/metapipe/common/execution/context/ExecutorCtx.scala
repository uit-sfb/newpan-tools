package no.uit.metapipe.common.execution.context

import java.nio.file.Path

import no.uit.metapipe.common.execution.pipeline.Identifier

case class ExecutorCtx(protected val basePath: Path, identifier: Identifier)
    extends InOutContextLike {
  require(dir.isAbsolute, s"basePath MUST be absolute but '$dir' found")

  override protected lazy val mayBeMultiple = true

  def newJobCtx(jobId: String) =
    JobCtx(basePath, identifier + jobId)
}
