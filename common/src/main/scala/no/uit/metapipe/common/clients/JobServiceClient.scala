package no.uit.metapipe.common.clients

import no.uit.metapipe.common.execution.attempt.AttemptState

trait JobServiceClient {
  // Executor API
  def executorQueue(executorId: String, tags: Set[String]): Seq[ExecutorJobDto]
  def popExecutorQueue(executorId: String,
                       tags: Set[String],
                       limit: Int): Seq[ExecutorJobDto]

  def registerOutput(attemptUri: String,
                     datasetKey: String,
                     datasetRef: DatasetUrl)
  def updateAttemptState(attemptUrl: String, state: AttemptState)

  def getAttempt(attemptUri: String): AttemptDto
  def getExecutorJob(executorJobUri: String): ExecutorJobDto
  def sendHeartbeat(attemptUri: String): Unit

  //Some metadata sent to JobManager
  def updateProperties(attemptUri: String,
                       properties: Map[String, String]): Unit

  // User API
  def createJob(userId: String,
                jobId: String,
                job: CreateUserJobDto): UserJobDto
  def exists(userId: String, jobId: String): Boolean
  def getUserJob(userId: String, jobId: String): UserJobDto
  def getUserJobs(userId: String): Seq[UserJobDto] //Not used
}
