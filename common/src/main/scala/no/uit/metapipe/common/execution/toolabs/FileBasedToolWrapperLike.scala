package no.uit.metapipe.common.execution.toolabs

import java.nio.file.Path

import no.uit.metapipe.common.execution.context.ToolCtx
import no.uit.sfb.scalautils.common.FileUtils

case class FileBaseToolInLike[A](inputs: Map[String, Path], params: A) //List of input files to (smart)copy to 'inputs' and use as input

case class FileBaseToolOutLike(outputs: Map[String, Path]) //List of output files

/**
  * FileBasedToolLike (smart)copies automatically the input files in the 'inputs' folder and executes
  * the tool in the 'outputs' folder
  */
trait FileBasedToolWrapperLike[A]
    extends ToolWrapper[FileBaseToolInLike[A], FileBaseToolOutLike] {
  def execImpl(ctx: ToolCtx, in: FileBaseToolInLike[A]): FileBaseToolOutLike = {
    val out = runProcess(ctx, prepare(ctx, in))
    cleanUp() //possible to clean up the containers
    out
  }

  //We copy the input files
  def prepare(ctx: ToolCtx,
              in: FileBaseToolInLike[A]): FileBaseToolInLike[A] = {
    in.copy(inputs = in.inputs map {
      case (inName, inPath) =>
        val copyPath = ctx.inputFile(inPath.getFileName.toString)
        FileUtils
          .copyFile(inPath, copyPath) //replaced smartCopy //replace hardLink
        inName -> copyPath
    })
  }

  def runProcess(ctx: ToolCtx, in: FileBaseToolInLike[A]): FileBaseToolOutLike

  def cleanUp(): Unit = ()
}
