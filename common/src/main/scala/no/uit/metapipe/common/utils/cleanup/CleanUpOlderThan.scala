package no.uit.metapipe.common.utils.cleanup

import java.io.{File, FileFilter}
import java.nio.file.{Files, Path}

import no.uit.metapipe.common.logging.Logging
import no.uit.sfb.scalautils.common.FileUtils

import scala.concurrent.duration._

/**
  * Cleans-up cache
  * Policy: delete any file whose last modified time is older than duration
  */
class CleanUpOlderThan(timeout: Duration, datasetsPath: Path)
    extends CleanUpPolicy
    with Logging {
  def apply(): Unit = {
    log.info(s"Cleaning-up cache (root=$datasetsPath)")
    log.info(s"Removing any file and directories older than $timeout")
    FileUtils.createDirs(datasetsPath)
    val time = System.currentTimeMillis().millis
    val lastModifDate = time - timeout

    def getNumOfBrokenSymLinks(dir: Path): Int = {
      val filter: FileFilter = new FileFilter {
        override def accept(file: File): Boolean = return !file.exists()
      }
      dir.toFile.listFiles(filter).length
    }

    def noBrokenSymLinksLs(dir: Path) = {
      val dirList = dir.toFile.listFiles().filter(p => Files.exists(p.toPath))
      val directories: Array[File] = dirList.partition(p => p.isDirectory)._1
      val files: Array[File] = dirList.partition(p => p.isDirectory)._2
      (directories.map(d => d.toPath), files.map(f => f.toPath))
    }

    def removeBrokenLinksAndcleanUpDir(dir: Path): Unit = {
      val (directories, files) = noBrokenSymLinksLs(dir) //FileUtils.ls(dir)
      files foreach { file =>
        if (Files.getLastModifiedTime(file).toMillis.millis < lastModifDate)
          FileUtils.deleteFile(file)
      }
      directories foreach { d =>
        removeBrokenLinksAndcleanUpDir(d)
        if (d.toFile.listFiles().length == getNumOfBrokenSymLinks(d))
          FileUtils.deleteDir(d)
        else if (d.toFile.listFiles().length < 1)
          FileUtils.deleteDir(d)
      }
    }

    def cleanUpDir(dir: Path): Unit = {
      val (directories, files) = FileUtils.ls(dir)
      files foreach { file =>
        if (Files.getLastModifiedTime(file).toMillis.millis < lastModifDate)
          FileUtils.deleteFile(file)
      }
      directories foreach { d =>
        cleanUpDir(d)
        if (FileUtils.isEmptyDir(d))
          FileUtils.deleteDir(d)
      }
    }

    //cleanUpDir(datasetsPath)
    log.info("Cleaning is in the process.")
    removeBrokenLinksAndcleanUpDir(datasetsPath)
  }
}
