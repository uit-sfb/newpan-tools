package no.uit.metapipe.common.clients

import no.uit.metapipe.common.execution.attempt.AttemptLike

/**
  * Utility for generating storage URIs
  * 'key' is a dataset key
  * The default value for the key is meant as a pattern to be replaced by a real key
  */
class StorageUriGenerator(
    val objectStoreUrl: String
) {
  protected def template(userId: String, jobId: String, key: String)(
      folder: String): String = {
    s"$objectStoreUrl/$userId" +
      s"/$jobId/$folder" +
      s"/${StorageUriGenerator
        .keyToFileName(key)}"
  }

  private def template(attempt: AttemptLike[_], key: String)(
      folder: String): String = {
    template(attempt.userId, attempt.parentJobId, key)(folder)
  }

  def inputUri(userId: String, jobId: String, key: String): String = {
    template(userId, jobId, key)(s"inputs")
  }

  /**
    * MUST be deterministic so that lazy retry works
    */
  def outputUri(attempt: AttemptLike[_], key: String): String = {
    template(attempt, key)("outputs")
  }

  def intermUri(attempt: AttemptLike[_], key: String): String = {
    template(attempt, key)("intermediate")
  }
}

object StorageUriGenerator {
  def keyToFileName(k: String): String =
    s"${k.reverse.replaceFirst("_", ".").reverse}"
}
