package no.uit.metapipe.common.execution.executor

import no.uit.metapipe.common.execution.context.ModuleCtx
import no.uit.metapipe.common.execution.task.{FailureState, TaskState}
import no.uit.metapipe.common.logging.Logging
import no.uit.sfb.rxfsm.RxDirectedFsm
import no.uit.sfb.scalautils.common.concurrency.RunnerLike

/**
  * CancellableRunnerLike which hold a state
  * run() should be called only once (throw an exception if called twice). In case of failure, discard and create a new executor.
  * cleanUp() should be executed finally (normal termination, cancellation, failure)
  */
trait TaskExecutor[P, T]
    extends RunnerLike[P, T]
    with RxDirectedFsm[TaskState]
    with Logging {
  def taskId: String

  def stateCompanion = TaskState

  protected def oModuleCtx: Option[ModuleCtx]

  def cancel(): Unit

  /**
    * Notify the executor a failure
    */
  final def failedWith(fs: FailureState): Unit = {
    tryTransitionTo(fs)
  }

  onTerminalState(s => {
    oModuleCtx foreach { moduleCtx =>
      moduleCtx.archive(s.name)
    }
    cleanUp()
  })

  /**
    * Clean up method called when the task completes (success or failure)
    */
  protected def cleanUp(): Unit = ()
}
