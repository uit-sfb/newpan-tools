package no.uit.metapipe.common.utils.snapshot

object Snapshot {
  def snapshot[T: SnapshotStrategy](identifier: String)(f: => T): T = {
    val strategy = implicitly[SnapshotStrategy[T]]
    strategy.restore(identifier).getOrElse {
      val res = f
      strategy.save(identifier, res)
      res
    }
  }
}

trait SnapshotStrategy[T] {
  def save(identifier: String, t: T)
  def restore(identifier: String): Option[T]
}
