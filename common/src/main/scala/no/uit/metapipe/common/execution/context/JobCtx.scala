package no.uit.metapipe.common.execution.context

import java.net.URL
import java.nio.file.{Path, Paths}

import no.uit.metapipe.common.execution.pipeline.Identifier

/**
  * Do not create manually -- use an ExecutorCtx
  */
case class JobCtx(protected val basePath: Path, identifier: Identifier)
    extends InOutContextLike {
  override protected lazy val mayBeMultiple = true

  def newAttemptCtx(attemptId: String,
                    numThreads: Int = 1,
                    useDocker: Boolean,
                    artifactoryUrl: URL) =
    AttemptCtx(basePath,
               identifier + attemptId,
               numThreads,
               useDocker,
               artifactoryUrl) //added Docker flag
}
