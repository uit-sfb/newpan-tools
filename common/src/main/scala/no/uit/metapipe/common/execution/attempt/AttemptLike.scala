package no.uit.metapipe.common.execution.attempt

import no.uit.metapipe.common.lib.TaskLike

/**
  * Adds the concept of attempt
  */
trait AttemptLike[+Params] extends TaskLike[Params] {
  this: TaskLike[Params] =>

  def parentJobId: String
  def uri: String
  def userId: String

  //Poll state from JobManager
  def state: () => AttemptState
  def attemptUri: String
}
