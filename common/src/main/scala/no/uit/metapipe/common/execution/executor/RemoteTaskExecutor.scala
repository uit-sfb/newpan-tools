package no.uit.metapipe.common.execution.executor

import no.uit.metapipe.common.execution.executor.impl.TaskExecutorImpl
import no.uit.metapipe.common.execution.task.TaskResult
import no.uit.metapipe.common.lib.{ExecutableLogic, TaskParameter}
import no.uit.sfb.scalautils.common.concurrency.heartbeat.Heartbeat

import scala.concurrent.{ExecutionContext, Future}
import scala.concurrent.duration._
import scala.concurrent.blocking

/**
  * Executes a task and runs a heartbeat thread asynchronously.
  * To be used on a remote machine and communication hand in hand with a ProxyTaskExecutor(removed as not used) running on the "local" machine
  */
abstract class RemoteTaskExecutor(
    prefix: String,
    logic: ExecutableLogic[TaskParameter, TaskResult])(
    implicit ec: ExecutionContext)
    extends TaskExecutorImpl[TaskParameter, TaskResult](prefix) {
  private val hbPeriod = 5.seconds

  private val heartbeat: Heartbeat =
    new Heartbeat(hbPeriod, emmitHeartbeat())

  override def cleanUp(): Unit = {
    heartbeat.cancel()
    super.cleanUp()
  }

  //Typically send the state
  protected def emmitHeartbeat(): Unit

  protected def executeImpl(p: TaskParameter): Future[TaskResult] =
    Future {
      blocking {
        logic.execute(p)
      }
    }

  protected def cancelImpl(): Unit = ()
}
