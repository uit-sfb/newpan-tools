package no.uit.metapipe.common.execution.task

import no.uit.metapipe.common.lib.dataset.impl.DatasetForm

case class TaskResult(
    notifications: Map[String, String] = Map(),
    outputs: Map[String, DatasetForm] = Map()
)
