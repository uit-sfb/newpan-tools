package no.uit.metapipe.common.exceptions

//Triggered by a WorkflowLogic failure
class LogicExecutionException(message: String) extends Exception(message)
