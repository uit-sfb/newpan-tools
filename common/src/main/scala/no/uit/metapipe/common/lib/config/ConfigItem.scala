package no.uit.metapipe.common.lib.config

case class ConfigItem(jobMan: String,
                      objectStore: String,
                      authService: String,
                      tags: Set[String])

object ConfigItem {
  def fromClientConfig(cc: ClientConfig) = ConfigItem(
    cc.jobMan.toString,
    cc.objectStore.toString,
    cc.auth.toString,
    cc.tags
  )
}
