package no.uit.metapipe.common.execution.execmanapi

import no.uit.metapipe.common.lib.dataset.impl.{
  DatasetForm,
  MetaDescriptor,
  MetapipeDataset
}

import scala.concurrent.Future

/**
  * Interface to be used by the WorkflowLogic implementations
  */
trait SpecializedExecutionApi {
  final def fetchDataset(
      descr: MetaDescriptor): Future[Option[MetapipeDataset]] = {
    fetchDataset(descr.key)
  }

  /**
    * Best effort to fetch the dataset pointed by the key
    * The key is used against attempt.inputs, attempt.intermediate and attempt.outputs to get the url of the dataset
    * If not found, the url of the dataset is created and search for on storage (this assumes the storageUriGenerator uses a deterministic scheme)
    */
  def fetchDataset(key: String): Future[Option[MetapipeDataset]]

  /**
    * Creates a dataset and push to storage
    */
  def pushDataset(key: String, ref: DatasetForm): Future[MetapipeDataset]

  /**
    * Keeps master executor (JobManager or other executor) updated with new task's properties
    */
  def updateProperties(properties: Map[String, String]): Unit
}

/**
  * Interface used by the JobExecutors to plug into metapipeClient.
  * To be used by the Executors
  */
trait SpecializedExecutionManagerApi[State] extends SpecializedExecutionApi {

  /**
    * Keeps master executor (JobManager or other executor) updated with the current state of the task
    */
  def updateState(newState: State): Unit

  /**
    * Sends heartbeat to master executor (JobManager or other executor)
    * Returns false if an error occurred and true if apparently succeeded
    */
  def sendHeartbeat(): Boolean
}
