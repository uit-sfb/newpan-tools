package no.uit.metapipe.common.execution.attempt

import no.uit.metapipe.common.clients._
import no.uit.sfb.scalautils.json.Json

case class Attempt[+Params](
    id: String,
    parameters: Params,
    inputs: Map[String, DatasetUrl],
    userId: String,
    uri: String,
    attemptUri: String,
    parentJobId: String,
    state: () => AttemptState,
    outputs: () => Map[String, DatasetUrl]
) extends AttemptLike[Params]

object Attempt {
  def create[Params <: AnyRef: Manifest](
      executorJobDto: ExecutorJobDto,
      jobServiceClient: JobServiceClient): Attempt[Params] = {
    def fetchAttemptDto(): AttemptDto = {
      jobServiceClient.getAttempt(executorJobDto.attemptUri)
    }
    def state(): AttemptState = {
      val attemptState =
        fetchAttemptDto().state
      AttemptState.fromString(attemptState)
    }
    def outputs(): Map[String, DatasetUrl] = {
      fetchAttemptDto().outputs
    }
    Attempt(
      executorJobDto.attempt.attemptId,
      Json.parse[Params](executorJobDto.parameters),
      executorJobDto.inputs,
      executorJobDto.userId.getOrElse("defaultUser"),
      executorJobDto.selfUri,
      executorJobDto.attemptUri,
      executorJobDto.jobId,
      () => state(),
      () => outputs()
    )
  }
}
