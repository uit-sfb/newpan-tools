package no.uit.metapipe.common.lib.dataset.impl

case class MetaDescriptor(key: String,
                          description: Option[String] = None,
                          format: Option[String] = None,
                          tags: Set[String] = Set() //Additional custom tags
) {
  def toMap(): Map[String, String] = {
    Map(
      "key" -> key,
      "description" -> description.getOrElse(""),
      "format" -> format.getOrElse("undefined")
    ) ++
      (tags.zipWithIndex map {
        case (v, idx) =>
          s"tag_$idx" -> v
      })
  }
}

object MetaDescriptor {
  def fromMap(m: Map[String, String]): Option[MetaDescriptor] = {
    m.get("key") map { key =>
      val tags = for {
        (k, v) <- m
        if k.startsWith("tag_")
      } yield v
      MetaDescriptor(key, m.get("description"), m.get("format"), tags.toSet)
    }
  }
}
