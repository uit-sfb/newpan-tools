package no.uit.metapipe.common.execution.context

import java.nio.file.Path

import no.uit.metapipe.common.execution.pipeline.Identifier
import no.uit.sfb.scalautils.common.FileUtils
import no.uit.sfb.scalautils.json.Json

import scala.concurrent.duration._

/**
  * Contexts define a folder where some code leaves some files (ex. input, output, temp, status files...)
  * It is also used to measure some metrics (ex. cpu usage, runtime, ...)
  * Note: contexts must be unique and a RuntimeException error is thrown in case one tries to create a context with the same id of another context
  * The exception being if mayBeMultiple is set to true (for ExecutorCtx only)
  */
trait ContextLike {
  protected def basePath: Path
  def identifier: Identifier

  final lazy val dir = basePath.resolve(identifier.toRelativePath)
  final lazy val tempDir = dir.resolve("temp")

  protected lazy val statusPath = dir.resolve(ContextObj.moduleStatus)

  lazy val startTime = getTime()

  private def getTime(): Duration = System.currentTimeMillis().millis

  protected lazy val mayBeMultiple = false

  protected def init(): Unit = {
    if (!mayBeMultiple && FileUtils.exists(dir))
      throw new RuntimeException(
        s"Context '$dir' already exists -- contexts must be unique")
    else {
      //We create the directory so that we can check whether a context exists or not
      //Todo: use a ContextBuilder object with synchronize to make it thread safe
      FileUtils.createDirs(dir)
      startTime //To get the time when init() is called
    }
  }

  /**
    * After archive is called the status of the context is written to a file
    */
  def archive(finalState: String): Unit = {
    val status = DefaultCtxStatus(
      finalState,
      StartStopTime(startTime, getTime()).wallClockTimeInfo,
      None //Todo: measure CPU time
    )
    writeStatus(status)
    //We delete the temp folder (if any)
    if (FileUtils.exists(tempDir))
      FileUtils.deleteDir(tempDir)
  }

  final protected def createPath(directory: Path): String => Path = { name =>
    {
      val path = directory.resolve(name)
      if (name.isEmpty)
        FileUtils.createDirs(path)
      else
        FileUtils.createParentDirs(path)
      path
    }
  }

  def tempFile(name: String): Path = createPath(tempDir)(name)

  def status(): DefaultCtxStatus =
    Json.fromFileAs[DefaultCtxStatus](statusPath)

  private def writeStatus(status: DefaultCtxStatus): Unit =
    Json.toFileAs[DefaultCtxStatus](statusPath, status)

  init()
}

object ContextObj {
  val moduleStatus = "module_status.json"
  val processStatus = "process_status.json"
}

/**
  * Context with input and output folder
  */
trait InOutContextLike extends ContextLike {
  protected lazy val inputDir = dir.resolve("inputs")
  protected lazy val outputDir = dir.resolve("outputs")

  def inputFile(name: String): Path = createPath(inputDir)(name)

  def outputFile(name: String): Path = createPath(outputDir)(name)
}
