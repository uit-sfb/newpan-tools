package no.uit.metapipe.common.utils.packaging

import java.net.URL
import java.nio.file.{Files, NoSuchFileException, Path, Paths}

import no.uit.metapipe.common.logging.Logging
import no.uit.sfb.scalautils.common.{FileUtils, FutureUtils}

import scala.concurrent.ExecutionContext
import sys.process._
import scala.concurrent.duration._

case class Artifact(packageName: String, pVersion: String, aVersion: String)
    extends Logging {
  implicit val ec = ExecutionContext.global

  val localPath = Paths.get(packageName).resolve(s"${pVersion}_$aVersion")
  val artifactName = s"${packageName}_${pVersion}_$aVersion.tgz"
  def url(artifactsUrl: URL): URL =
    new URL(s"$artifactsUrl/$packageName/$artifactName")

  lazy val toDependency = Package(packageName, pVersion, Map(aVersion -> this))

  /**
    * Download the artifact and extracts it.
    * When the function is entered, a lock is materialized as a file created atomically. When the function exits, the file is deleted (not atomically but it does not matter at that point).
    * In case two jvm instances try to download at the same time, only one will effectively download the artifact, while the second one will only wait for the lock file to disappear.
    * Note: This is just an attempt make the download function process-safe but there is really no guaranty that this implementation is really safe.
    * If overwrite is set to false, the downloading is skipped if the package is found.
    * If force is set to true and the artifact is locked, remove the lock and delete the matching directory (as it is assumed only partially downloaded)
    */
  def download(dir: Path,
               artifactsUrl: URL,
               overwrite: Boolean = false,
               force: Boolean): Unit = {
    val packageDir = dir.resolve(packageName.toString)
    val target = packageDir.resolve(s"${pVersion}_$aVersion")
    val lock = packageDir.resolve(s"${target.getFileName}.lock")
    FileUtils.createParentDirs(lock)

    if (FileUtils.exists(lock)) {
      println(
        s"Package ${packageName}_${pVersion}_$aVersion is locked meaning that another process is currently downloading it.")
      if (force) {
        FileUtils.deleteDirIfExists(target)
        Files.delete(lock)
        log.warn(
          s"Taking over the lock of package ${packageName}_${pVersion}_$aVersion due to the use of -f|--force flag. This might lead to inconsistencies in case you act in a multi-threaded environment!")
      }
      FutureUtils.RetryUntilTrue(!FileUtils.exists(lock),
                                 24.hours.toSeconds.toInt,
                                 1.second)
    } else {
      Files.createFile(lock) //Create file atomically
      try {
        if (!FileUtils.exists(target) || overwrite) {
          println(s"Downloading $this... (this may take a while)")
          FileUtils.deleteDirIfExists(target)
          FileUtils.createDirs(packageDir)
          val ret =
            Process(
              s"""curl --write-out "%{http_code}" -sO ${url(artifactsUrl)}""",
              dir.toFile).!!
          if (ret.replace("\"", "").replace("\n", "").toInt >= 400)
            throw new Exception(
              s"Dependency (${url(artifactsUrl)}) download failed with code: '$ret'")
          println(
            s"Extracting archive $artifactName... (this may take a while)")
          s"tar -xzf $dir/$artifactName -C $packageDir --strip-components=1".!!
          s"rm $dir/$artifactName".!!
        } else {
          println(s"$this already installed")
        }
      } finally {
        Files.delete(lock)
      }
    }
  }

  def removeOlderVersions(dir: Path): Unit = {

    try {
      val toRemove = FileUtils.ls(dir.resolve(packageName))._1 flatMap { p =>
        Artifact.parse(s"${packageName}_${p.getFileName}.tgz")
      } filter { _ < this }
      toRemove foreach { a =>
        println(s"Removing older $a")
        FileUtils.deleteDirIfExists(dir.resolve(a.localPath))
      }
    } catch {
      case e: NoSuchFileException => {
        FileUtils.createDirs(dir.resolve(packageName))
      }
    }

  }
}

object Artifact extends Ordering[Artifact] {
  def parse(artifactName: String): Option[Artifact] = {
    artifactName.split(".tgz").headOption flatMap { fullName =>
      fullName.split("_").toList match {
        case n :: dv :: v :: Nil => Some(Artifact(n, dv, v))
        case _                   => None
      }
    }
  }

  def compare(x: Artifact, y: Artifact): Int = {
    if (x.packageName != y.packageName)
      throw new Exception(
        s"Cannot compare artifacts with different package names (${x.packageName} vs ${y.packageName})")
    val cmp = GenericVersion.compare(x.pVersion, y.pVersion)
    if (cmp != 0)
      cmp
    else {
      GenericVersion.compare(x.aVersion, y.aVersion)
    }
  }
}
