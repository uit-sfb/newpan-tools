package no.uit.metapipe.common.execution.executor

object ExecutorGroups {
  val default = "default" //Standard executor
  val highRam = "highRAM" //High RAM executor
}
