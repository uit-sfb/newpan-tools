package no.uit.metapipe.common.lib.dataset

import no.uit.metapipe.common.lib.dataset.impl.MetaDescriptor

/**
  * A dataset is a medium-less piece of data along with some metadata (of type Metadata).
  * By 'medium.less' we mean that the data can be in memory, on disc or on a network.
  * Transferring the piece of data from one medium to the other leaves the dataset unchanged.
  * A Dataset can easily be converted from one medium to the other and some caching
  * capabilities can be added when managed by a DatasetManagerLike
  */
trait DatasetLike {

  /**
    * The metadata
    */
  def meta: MetaDescriptor

  /**
    * Converts this dataset to a DatasetForm (local if persisted or remote if not)
    */
  def toDatasetForm: DatasetFormLike
}
