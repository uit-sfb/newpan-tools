package no.uit.metapipe.common.lib.config

/**
  * Any RawConfig contains configuration values that need to be processed in order to get the definitive configuration
  * The processing is typically:
  *   - conversion from relative path to absolute path
  *   - substitution of a key (such as metapipe_prod or metapipe_test) to a value
  * The processed config is available with 'process'
  */
trait RawConfig[T] {
  def process: T
}
