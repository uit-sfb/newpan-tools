package no.uit.metapipe.common.utils.packaging

import scala.util.Try

/**
  * Add ordering to versions of type X.Y.Z. ...
  * If X1 and X2 are integers, they are compared as integers
  * If not, they are compared lexicographically as string.
  * To compare two versions, they must have the same number of elements
  */
object GenericVersion extends Ordering[String] {

  def convertToInt(s: String): Int = {
    try {
      s.toInt
    } catch {
      case e: Exception => 0
    }
  }

  def compare(v1: String, v2: String): Int = {
    val splt1 = v1.split('.') //size 3
    val splt2 = v2.split('.') //size 3
    //temporary workaround
//    val s1 = splt1.map(i => convertToInt(i).toString)
//    val s2 = splt2.map(i => convertToInt(i).toString)
//
//    val sp1 =
//      if (s1.length < s2.length) List.fill(s2.length)(s1.head).toArray else s1
//
//    val sp2 = s2
    if (splt1.size != splt2.size)
      throw new Exception(
        s"Cannot compare dependency versions with different formats ($v1 vs $v2)")
    splt1
      .zip(splt2)
      .foldLeft(0)((acc, couple) => {
        if (acc != 0)
          acc
        else if (couple._1 == couple._2)
          0
        else {
          Try {
            (couple._1.toInt, couple._2.toInt)
          }.toOption match {
            case Some((i1, i2)) => Ordering[Int].compare(i1, i2)
            case None =>
              Ordering[String].compare(couple._1, couple._2)
          }
        }
      })
  }
}
