package no.uit.metapipe.common.execution.pipeline

import no.uit.metapipe.common.execution.context.ContextLike
import no.uit.metapipe.common.logging.Logging
import no.uit.sfb.apis.stateful.{DGStateLike, DirectedGraphLike}
import no.uit.sfb.rxfsm.RxStatefulLike
import rx.Obs

trait ModuleLike[S <: DGStateLike]
    extends RxStatefulLike[S]
    with DirectedGraphLike[S, Obs]
    with Logging {
  def context: ContextLike
  final lazy val id = context.identifier
  def moduleName: String
  def execute(): Unit
  def cancel(): Unit = {
    log.info(s"Cancelling module $id...")
  }

  onTransition((oldS, newS) => {
    log.info(s"Module '$id' changed state: ${oldS.name} -> ${newS.name}")
  }, skipInitial = true)

  onInvalidTransition((oldS, newS) =>
    log.warn(
      s"Module '$id' invalid transition detected: ${oldS.name} -> ${newS.name}"))

  onTerminalState(s => {
    log.info(s"Module '$id' reached terminal state '$s'")
    context.archive(s.name)
  })
}
