package no.uit.metapipe.common.lib.dataset.impl

import java.io.BufferedInputStream
import java.nio.file.Path

import no.uit.metapipe.common.lib.dataset._
import no.uit.metapipe.common.logging.Logging
import no.uit.sfb.scalautils.common.data.{FileHolderLike, StreamHolderLike}

import scala.concurrent.duration._
import scala.concurrent.Await
import scala.util.Try

/** MetapipeDataset are managed datasets. They should be created exclusively by a DatasetManager to ensure proper local-remote synchronization
  * The whole logic for managing datasets is located in the provided instance of datasetManager
  */
class MetapipeDataset(val id: String, val meta: MetaDescriptor)(
    implicit val datasetManager: MetapipeDatasetManager)
    extends ManagedDatasetLike
    with StreamHolderLike
    with FileHolderLike
    with Logging {
  override type ManagerType = MetapipeDatasetManager

  //Just a utility method to find a meaningful name when seeding
  lazy val fileName = datasetManager.dataPath(id).getFileName.toString

  /**
    * For convenience purpose this method hardLinks the data to a desired location
    * Yield a caching of the data
    */
  def seed(path: Path): Unit = {
    consume(_ => ()) //We cache the data
    datasetManager.copy(id, path)
  }

  //Blocking
  def toDatasetForm: DatasetForm = {
    val cached = Await.result(datasetManager.isCached(id), 10.seconds)
    if (cached) {
      FileDatasetForm(
        datasetManager.dataPath(id),
        meta
      )
    } else {
      RemoteDatasetForm(
        datasetManager.dataUri(id),
        meta
      )
    }
  }

  protected def newStream(): BufferedInputStream = {
    val is = Try {
      Await.result(datasetManager.newStream(id), 6.hours)
    } recover {
      case e =>
        log.warn(s"Could not stream dataset '$id'", e)
        throw e
    }
    new BufferedInputStream(is.get)
  }
}
