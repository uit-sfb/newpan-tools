package no.uit.metapipe.common.execution.executor

import no.uit.metapipe.common.execution.executor.impl.TaskExecutorImpl
import no.uit.sfb.scalautils.common.concurrency.CancellableRunner
import no.uit.sfb.scalautils.common.FutureUtils

import scala.concurrent.Future
import scala.util.Try

trait ExecutionImplementation[P, T] {
  this: TaskExecutorImpl[P, T] =>
  protected def executeImpl(p: P): Future[T]
  protected def cancelImpl(): Unit
}

/**
  * Execute the logic synchronously
  * Blocking
  */
trait InlineExecution[P, T] extends ExecutionImplementation[P, T] {
  this: TaskExecutorImpl[P, T] =>

  protected def executeImpl(p: P): Future[T] = {
    FutureUtils.fromTry(Try {
      logic.execute(p)
    })
  }
  protected def cancelImpl(): Unit = () //Cannot cancel an inline execution
}

/**
  * Execute the logic asynchronously
  * Non-blocking
  */
trait DetachedExecution[P, T] extends ExecutionImplementation[P, T] {
  self: TaskExecutorImpl[P, T] =>

  val runner: CancellableRunner[P, T] = CancellableRunner(
    param => logic.execute(param))

  protected def executeImpl(p: P): Future[T] = {
    runner.execute(p)
  }

  protected def cancelImpl(): Unit = {
    runner.cancel()
  }
}
