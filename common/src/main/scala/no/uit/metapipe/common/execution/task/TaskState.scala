package no.uit.metapipe.common.execution.task

import no.uit.metapipe.common.utils.fsm.{FsmState, FsmStateCompanion}

abstract class TaskState extends FsmState {
  lazy val isFailure: Boolean = false
  lazy val isRecoverable: Option[Boolean] = None
}

trait TerminalState {
  self: TaskState =>
  lazy val nextStates: Set[FsmState] = Set()
}

abstract class FailureState extends TaskState {
  override lazy val isFailure: Boolean = true
}

trait RecoverableTaskState extends FailureState with TerminalState {
  override lazy val isRecoverable: Option[Boolean] = Some(true)
  override lazy val specializationOf = Some(TaskState.FAILED)
}

trait UnrecoverableTaskState extends FailureState with TerminalState {
  override lazy val isRecoverable: Option[Boolean] = Some(false)
  override lazy val specializationOf = Some(TaskState.CRASHED)
}

/**
  * Initial state for a module.
  * It usually stays there until all its inputs are complete.
  */
object TaskState extends FsmStateCompanion[TaskState] {
  case object NOT_STARTED extends TaskState {
    lazy val nextStates: Set[FsmState] = Set(
      WAITING_FOR_RESOURCE,
      RUNNING,
      SKIPPED,
      FAILED,
      CRASHED
    )
  }

  /**
    * The execution of a module is stalled because one of:
    * - Some modules are in state FINISHED while others are still in state NOT_STARTED (probably a transient state or may indicate a deadlock if remains in this state)
    * - a proxy is waiting for a remote executor to pull the task
    * - A module is waiting for another module to complete due to some resource constraint (f. ex. RAM or cluster)
    */
  case object WAITING_FOR_RESOURCE extends TaskState {
    lazy val nextStates: Set[FsmState] = Set(
      RUNNING,
      FAILED,
      CRASHED
    )
  }

  /**
    * The executor of a module started execution
    */
  case object RUNNING extends TaskState {
    lazy val nextStates: Set[FsmState] = Set(
      WAITING_FOR_RESOURCE,
      FINISHED,
      FAILED,
      CRASHED
    )
  }

  /**
    * The executor finished execution successfully
    */
  case object FINISHED extends TaskState {
    lazy val nextStates: Set[FsmState] = Set(COMPLETED, FAILED, CRASHED)
  }

  /**
    * The module finished pushing outputs successfully
    */
  case object COMPLETED extends TaskState {
    lazy val nextStates: Set[FsmState] = Set()
  }

  /**
    * The module was skipped because already computed
    */
  case object SKIPPED extends TaskState {
    lazy val nextStates: Set[FsmState] = Set()
  }

  /**
    * Cancelled by external manager
    */
  case object CANCELLED extends RecoverableTaskState

  /**
    * Generic state for recoverable failed task
    * ConnectionException
    * SocketTimeoutException
    */
  case object FAILED extends RecoverableTaskState

  /**
    * An anterior module failed
    */
  case object INPUT_FAILED extends RecoverableTaskState

  /**
    * Contact with remote executor lost
    */
  case object NO_CONTACT extends RecoverableTaskState

  /**
    *Generic state for unrecoverable failure (a new attempt would yield the same consistent failure)
    */
  case object CRASHED extends UnrecoverableTaskState

  /**
    * One or more output were not complete while the task was
    */
  case object MISSING_OUTPUT extends UnrecoverableTaskState

  lazy val initialState: TaskState = NOT_STARTED
  lazy val states = Seq(
    NOT_STARTED,
    WAITING_FOR_RESOURCE,
    RUNNING,
    FINISHED,
    COMPLETED,
    CANCELLED,
    FAILED,
    INPUT_FAILED,
    NO_CONTACT,
    CRASHED,
    MISSING_OUTPUT
  )
}
