package no.uit.metapipe.common.execution.context

import java.net.URL
import java.nio.file.Path

import no.uit.metapipe.common.execution.pipeline.Identifier
//temporary solution
import no.uit.metapipe.common.lib.config._

import no.uit.metapipe.common.execution.toolabs.{
  Outs,
  ProcessRunner,
  ProcessStatus
}
import no.uit.sfb.scalautils.common.FileUtils
import no.uit.sfb.scalautils.json.Json

import scala.util.{Failure, Success, Try}

class ToolFailedException(msg: String) extends Exception(msg)

/**
  * Do not create manually -- use an ModuleCtx
  */
case class ToolCtx(protected val basePath: Path,
                   identifier: Identifier,
                   numThreads: Int = 1,
                   useDocker: Boolean,
                   artifactoryUrl: URL) //added Docker flag
    extends InOutContextLike {
  private val stdErrPath = dir.resolve("stderr")
  private val stdOutPath = dir.resolve("stdout")

  private val processStatusPath = dir.resolve(ContextObj.processStatus)

  def createToolSubCtx(name: String) =
    ToolCtx(basePath, identifier + name, numThreads, useDocker, artifactoryUrl)

  def isVirgin(): Boolean = {
    !FileUtils.exists(processStatusPath)
  }

  def failureException(statusCode: Int): ToolFailedException = {
    val details = Try {
      stdErr()
    } match {
      case Success(stderr) => stderr
      case Failure(e)      => s"(Could not read stderr file due to: ${e.getMessage})"
    }

    val msg =
      s"Tool $identifier failed with status code '$statusCode':\n$details"
    new ToolFailedException(msg)
  }

  /**
    * Creates a process runner matching the current context
    * stdOutPathOverride can be used in cases where a tool's output is not a file given as parameter but stdout
    * When used, stdout is not created and it is the overridden path that contains the stdout.
    */
  def process(stdOutPathOverride: Path = stdOutPath): ProcessRunner =
    new ProcessRunner(outputDir,
                      processStatusPath,
                      Outs.redirect(stdOutPathOverride),
                      Outs.redirect(stdErrPath))

  def stdErr(): String = FileUtils.readFile(stdErrPath)
  def stdOut(): String = FileUtils.readFile(stdOutPath)
  def processStatus(): ProcessStatus =
    Json.fromFileAs[ProcessStatus](processStatusPath)

  //We make sure we crate the outputs folder where the tool will run
  outputFile("")
}
