package no.uit.metapipe.common.utils

import java.io.{ByteArrayOutputStream, PrintWriter}

import scala.sys.process.ProcessLogger

object ProcessHelper {
  /** Runs a command and waits for result.
    *
    * Solution taken from: http://stackoverflow.com/a/15438493
    *
    * @return (exit code, stdout, stderr)
    */
  def runCommand(argv: Seq[String]): (Int, String, String) = {
    import scala.sys.process._
    val stdoutStream = new ByteArrayOutputStream
    val stderrStream = new ByteArrayOutputStream
    val stdoutWriter = new PrintWriter(stdoutStream)
    val stderrWriter = new PrintWriter(stderrStream)
    val exitValue = argv.!(ProcessLogger(stdoutWriter.println, stderrWriter.println))
    stdoutWriter.close()
    stderrWriter.close()
    (exitValue, stdoutStream.toString, stderrStream.toString)
  }
}
