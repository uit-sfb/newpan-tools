package no.uit.metapipe.common.execution.context

import java.net.URL
import java.nio.file.Path

import no.uit.metapipe.common.execution.pipeline.Identifier

/**
  * Do not create manually -- use a JobCtx
  */
case class AttemptCtx(protected val basePath: Path,
                      identifier: Identifier,
                      numThreads: Int,
                      useDocker: Boolean,
                      artifactoryUrl: URL)
    extends ContextLike {
  def newModuleCtx(moduleName: String,
                   numThreadsOverride: Int = numThreads,
                   useDocker: Boolean = useDocker,
                   artifactoryUrl: URL = artifactoryUrl) =
    ModuleCtx(basePath,
              identifier + moduleName,
              numThreadsOverride,
              useDocker,
              artifactoryUrl) //added Docker flag
}
