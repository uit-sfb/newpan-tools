package no.uit.metapipe.common.utils.docker

import java.nio.file.{Path, Paths}
import java.util.logging.Logger

object DockerUtils {
  val volumePathInput = Paths.get("/data/inputs")
  val volumePathOutput = Paths.get("/data/outputs")
  val registryValue = Paths.get("registry.gitlab.com/uit-sfb/genomic-tools")
  lazy val tag = "latest"

  def buildRunArgs(name: String,
                   image: String,
                   startFlagSeq: Seq[String] = Seq(),
                   dockerOptions: Seq[String] = Seq(),
                   volumesFrom: Seq[VolumeFromMount] = Seq(),
                   volumes: Seq[VolumeMount] = Seq(),
                   args: Seq[String] = Seq()): Seq[String] = {
    Seq(
//      Seq("run", "--rm"),
      startFlagSeq,
      dockerOptions,
      volumesFrom flatMap { _.toArgs },
      volumes flatMap { _.toArgs },
      Seq(s"--name=$name"),
      Seq(image),
      args
    ).flatten
  }

  /**
    * Convenience method which assume that all the input files are found in a same local directory and are mounted under the same directory `volumePathInput`,
    * and all the output files are written in the same directory `volumePathOutput` and are mounted back to a same local directory.
    * For `inputs` and `outputs`, the key of the Map is the argument option (for example '-i' or '--input'...). In case they are positional arguments, the label can be empty.
    * The input / output file args are automatically generated and inserted between preArgs and postArgs, and `inputsBeforeOutputs` determines which of inputs or outputs should appear first.
    * In case all those conditions cannot be met, use the generic function above.
    */
  def buildRunArgsQuick(name: String,
                        image: String,
                        dockerOptions: Seq[String] = Seq(),
                        volumesFrom: Seq[VolumeFromMount] = Seq(),
                        volumesPackages: Seq[VolumeMount] = Seq(),
                        preArgs: Seq[String] = Seq(),
                        inputs: Seq[(String, Path)] = Seq(),
                        outputs: Seq[(String, Path)] = Seq(),
                        postArgs: Seq[String] = Seq(),
                        noRemove: Boolean = false,
                        inputsBeforeOutputs: Boolean = true,
                        ifIsDataBase: Boolean = false,
                        noOutputsInArgs: Boolean = false,
                        noInputsInArgs: Boolean = false): Seq[String] = {
    val oInputParent = inputs.headOption map { case (_, p) => p.getParent }
    assert(
      inputs
        .forall { case (_, p) => p.getParent == oInputParent.get }, //We do not need to check that oInputParent is nonEmpty here because since we are executing this code it meas it isn't
      s"All input files must be located in the same directory, but test failed."
    )
    val oOutputParent = outputs.headOption map { case (_, p) => p.getParent }
    assert(
      outputs
        .forall { case (_, p) => p.getParent == oOutputParent.get }, //Same as above
      "All output files must be located in the same directory, but condition not met."
    )

    val volumes: Seq[VolumeMount] = Seq(
      oInputParent map { parentPath =>
        VolumeMount(parentPath, volumePathInput, true)
      },
      oOutputParent map { parentPath =>
        VolumeMount(parentPath, volumePathOutput, false)
      }
    ).flatten ++ volumesPackages

    val inputArgs = inputs flatMap {
      case (label, p) if !noInputsInArgs =>
        Seq(label, volumePathInput.resolve(p.getFileName).toString)
      case _ => Seq()
    }

    val outputArgs = outputs flatMap {
      case (label, p) if !noOutputsInArgs =>
        Seq(label, volumePathOutput.resolve(p.getFileName).toString)
      case _ => Seq()
    }

    val inOutArgs: Seq[String] = (if (inputsBeforeOutputs)
                                    inputArgs ++ outputArgs
                                  else
                                    outputArgs ++ inputArgs) filter {
      _.nonEmpty
    }
    val startFlagSeq = if (!noRemove) Seq("run", "--rm") else Seq("run")

    buildRunArgs(name,
                 image,
                 startFlagSeq,
                 dockerOptions,
                 volumesFrom,
                 volumes,
                 preArgs ++ inOutArgs ++ postArgs)
  }
}
