package no.uit.metapipe.common.utils.docker

import java.nio.file.Path

trait VolumeMountLike {
  def isReadOnly: Boolean
  lazy val suffix = if (isReadOnly) "ro" else "rw"
}

case class VolumeMount(from: Path, to: Path, isReadOnly: Boolean)
    extends VolumeMountLike {
  val toArgs: Seq[String] = Seq("-v", s"$from:$to:$suffix")
}

case class VolumeFromMount(imageFrom: String, isReadOnly: Boolean)
    extends VolumeMountLike {
  val toArgs: Seq[String] = Seq("--volumes-from", s"$imageFrom:$suffix")
}
