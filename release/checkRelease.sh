#!/usr/bin/env bash

set -e

TO_RELEASE_VERSION="$1"

echo "Reading version and scalaVersion..."

DIR="$( cd "$( dirname "$0" )" && pwd )"
ROOT_DIR=$(dirname $DIR)

CURRENT_DIR=$(pwd)

cd $ROOT_DIR

SBT_OUT=$(sbt -Dsbt.log.noformat=true root/projectVersion root/projectScalaVersion | tail -2)

RAW_VERSION=$(echo "$SBT_OUT" | head -1)
arrIN=(${RAW_VERSION// / }) #We remove the "[info] "
VERSION=${arrIN[1]}
echo "version=$VERSION"

echo "Reading scalaVersion..."

RAW_SCALA_VERSION=$(echo "$SBT_OUT" | tail -1)
arrIN=(${RAW_SCALA_VERSION// / }) #We remove the "[info] "
FULL_SCALA_VERSION=${arrIN[1]}
arrIN=(${FULL_SCALA_VERSION//./ })
SCALA_VERSION="${arrIN[0]}.${arrIN[1]}"

echo "scalaVersion=$SCALA_VERSION"

ERROR=0


if [[ "$TO_RELEASE_VERSION" != "$VERSION" ]]; then
  echo "The tag does not match the version defined in build.sbt ("$TO_RELEASE_VERSION" vs "$VERSION")"
  echo "Please update the version in build.sbt to match the tag to be used (most likely remove '-SNAPSHOT')"
  ERROR=1
fi

if [[ ! -z $(echo "$VERSION" | grep "SNAPSHOT$") ]]; then
  echo "A release tag should not include -SNAPSHOT but ('$VERSION' found)"
  echo "Please update the version in build.sbt to a sensible value (most likely remove '-SNAPSHOT')"
  ERROR=1
fi

echo "Checking that no -SNAPSHOT dependency is present in build.sbt..."

set +e #grep returns code 1 when no line is matches the pattern
SBT_OUT=$(sbt -Dsbt.log.noformat=true "show compile:dependencyList" | grep "SNAPSHOT$")
set -e

if [[ ! -z "$SBT_OUT" ]]; then
  echo "A release version should not include any -SNAPSHOT dependency but the following were found:"
  echo "$SBT_OUT"
  echo "Please remove any -SNAPSHOT dependency."
  ERROR=1
fi

if [[ "$ERROR" != "0" ]]; then
  echo ""
  echo "Please follow the following procedure:"
  echo "  - delete the tag on both Gitlab and git"
  echo "  - fix the error described above"
  echo "  - commit and push"
  echo "  - wait for the pipeline to complete (successfully)"
  echo "  - create the tag on git then push"
  echo "  - when the pipeline has completed successfully, update the tag in build.sbt (most likely bump the version up and add '-SNAPSHOT')"
  echo "  - commit and push"
  exit 1
fi

cd "$CURRENT_DIR"

echo "Check ok"