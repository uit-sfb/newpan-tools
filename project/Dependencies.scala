import sbt._
import sbt.librarymanagement.ModuleID

object Dependencies {
  lazy val logback = "ch.qos.logback" % "logback-classic" % "1.2.3"
  lazy val scopt = "com.github.scopt" %% "scopt" % "4.0.0-RC2"
  lazy val reflections = "org.reflections" % "reflections" % "0.9.10" //0.9.11 breaks reflection completely
  lazy val jcommander = "com.beust" % "jcommander" % "1.48" //Later releases do not work (com.beust.jcommander.ParameterException: Cannot use final field no.uit.metapipe.metapipectl.RunSubmitGalaxyCommand#inputs as a parameter; compile-time constant inlining may hide new values written to it.)

  lazy val scalaUtilsVersion = "0.2.3"
  lazy val scalaUtilsCommon = "no.uit.sfb" %% "scala-utils-common" % scalaUtilsVersion
  lazy val scalaUtilsJson = "no.uit.sfb" %% "scala-utils-json" % scalaUtilsVersion
  lazy val scalaUtilsHttp = "no.uit.sfb" %% "scala-utils-http" % scalaUtilsVersion
  lazy val scalaUtilsApis = "no.uit.sfb" %% "scala-utils-apis" % scalaUtilsVersion
  lazy val scalaUtilsS3Store = "no.uit.sfb" %% "scala-utils-s3store" % scalaUtilsVersion
  lazy val scalaUtilsRxfsm = "no.uit.sfb" %% "scala-utils-rxfsm" % scalaUtilsVersion
  lazy val scalaUtilsGenomiclib = "no.uit.sfb" %% "scala-utils-genomiclib" % scalaUtilsVersion

  //Because spark lin is "provided", you have to include this Seq to any project depending on SparkAbstractions
  lazy val spark: Seq[ModuleID] = Seq("spark-core")
    .map("org.apache.spark" %% _ % "2.4.1" % Provided)
    .map(_.exclude("*", "log4j"))
    .map(_.exclude("*", "slf4j-log4j12")) ++
    Seq("org.slf4j" % "log4j-over-slf4j" % "1.7.25")

  lazy val sqlite = "org.xerial" % "sqlite-jdbc" % "3.25.2"
  lazy val woodstox = "org.codehaus.woodstox" % "woodstox-core-asl" % "4.4.1"
  lazy val bioJava = "org.biojava" % "biojava-core" % "5.1.1"
  lazy val javaxXmlBind = "javax.xml.bind" % "jaxb-api" % "2.3.1"
  lazy val eclipsePersistence = "org.eclipse.persistence" % "org.eclipse.persistence.moxy" % "2.6.0" //2.7.3 would clash with joda-convert

  lazy val commonsNet = "commons-net" % "commons-net" % "3.6"
  lazy val commonsCompress = "org.apache.commons" % "commons-compress" % "1.18"
  lazy val scalaCsv = "com.github.tototoshi" %% "scala-csv" % "1.3.5"
  lazy val fingaleHtttp = "com.twitter" %% "finagle-http" % "18.11.0"

  //Test
  lazy val scalaTest = "org.scalatest" %% "scalatest" % "3.0.5"
  lazy val scalaMock = "org.scalamock" %% "scalamock-scalatest-support" % "3.6.0"
}
