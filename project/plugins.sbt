addSbtPlugin("com.eed3si9n" % "sbt-assembly" % "0.14.8")
addSbtPlugin("com.typesafe.sbt" % "sbt-native-packager" % "1.6.0")
addSbtPlugin("no.uit.sfb" % "sbt-git-smartversion" % "1.1.0")