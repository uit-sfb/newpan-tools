package no.uit.metapipe

import java.io.{File, FileInputStream}
import java.net.URL
import java.nio.file.Path
import java.security.{DigestInputStream, MessageDigest}

import scala.sys.process._

object RunCreateArtifact {

  def computeHash(path: String): String = {
    val buffer = new Array[Byte](8192)
    val md5 = MessageDigest.getInstance("MD5")

    val dis = new DigestInputStream(new FileInputStream(new File(path)), md5)
    try { while (dis.read(buffer) != -1) {} } finally { dis.close() }

    md5.digest.map("%02x".format(_)).mkString
  }

  def exec(artifactoryUser: String,
           artifactoryPassword: String,
           artifactsUrl: URL,
           artifactPath: String): Unit = {
    if (!artifactoryUser.isEmpty && !artifactoryPassword.isEmpty) {

//      println(s"User:$artifactoryUser")
//      println(s"Password:$artifactoryPassword")
//      println(s"Path: $artifactPath")

      val packageName = "metapipe"

      val artifactName = "cliTools.jar"

      println("Uploading...")

      val checkSum = computeHash(artifactPath.toString)
      val artifURL = new URL(s"$artifactsUrl/$packageName/$artifactName")
      val ret =
        s"""curl -u ${artifactoryUser}:${artifactoryPassword} --silent --output /dev/null --write-out "%{http_code}" -T $artifactPath $artifURL;)""".!!.replace(
          "\"",
          "").trim()
      if (ret != "201") {
        println(s"Upload failed with code $ret")
        sys.exit(1)
      }
    } else
      println(
        "Upload skipped since no Artifactory credentials provided (please use -u|--user and -p|--password options)")
  }

}
