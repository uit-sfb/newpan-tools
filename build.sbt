import com.typesafe.sbt.packager.docker.{Cmd, CmdLike, ExecCmd}
import Dependencies._
import no.uit.metapipe.RunCreateArtifact.exec
import no.uit.sfb.sbt.smartversion.GitPlugin.autoImport.git
import sbt.Keys._

ThisBuild / scalaVersion     := "2.12.7"
ThisBuild / organization     := "no.uit.sfb"
ThisBuild / organizationName := "SfB"

ThisBuild / credentials += Credentials(Path.userHome / ".sbt" / ".credentials")

ThisBuild / resolvers ++= {
  Seq(Some("Artifactory-dev" at "https://artifactory.metapipe.uit.no/artifactory/sbt-release-local/"),
  if ((ThisBuild / version).value.endsWith("-SNAPSHOT"))
    Some("Artifactory" at "https://artifactory.metapipe.uit.no/artifactory/sbt-dev-local/")
  else
    None
  ).flatten
}

ThisBuild / resolvers += "gitlab" at "https://gitlab.com/api/v4/groups/2067095/-/packages/maven"

ThisBuild / publishTo := {
  if ((ThisBuild / version).value.endsWith("-SNAPSHOT"))
    Some("Artifactory Realm" at "https://artifactory.metapipe.uit.no/artifactory/sbt-dev-local;build.timestamp=" + new java.util.Date().getTime)
  else
    Some("Artifactory Realm" at "https://artifactory.metapipe.uit.no/artifactory/sbt-release-local")
}

ThisBuild / updateOptions := updateOptions.value.withLatestSnapshots(false)

val projectVersion = settingKey[String]("Project version.")
val projectScalaVersion = settingKey[String]("Project Scala version.")

lazy val merge = assemblyMergeStrategy in assembly := {
  case x: String if x.contains("org/slf4j/impl") => MergeStrategy.first
  case PathList("META-INF", "io.netty.versions.properties", xs @ _*) => MergeStrategy.first
  case PathList("javax", xs @ _*) => MergeStrategy.first
  case x =>
    val oldStrategy = (assemblyMergeStrategy in assembly).value
    oldStrategy(x)
}

lazy val assemblySettings = Seq(
  test in assembly := {},
  merge
)

logBuffered in Test := false

// TODO: set testOptions in cliTools += Tests.Argument("-oF")

lazy val baseSettings = Seq(
  scalacOptions ++= Seq("-unchecked", "-deprecation", "-feature"),
  libraryDependencies ++= Seq(
    scalaTest % Test,
    scalaMock % Test
  )
)

lazy val common = project
  .settings(baseSettings,
    publish := {},
    publishLocal := {}
  )
  .disablePlugins(sbtassembly.AssemblyPlugin)
  .settings(libraryDependencies ++= Seq(
    logback,
    scalaUtilsCommon,
    scalaUtilsJson,
    scalaUtilsHttp,
    scalaUtilsApis,
    scalaUtilsS3Store,
    scalaUtilsRxfsm
  ) ++ spark)

lazy val publishCliTools =
  sbt.taskKey[Unit]("publish cliTools on Artifactory")
lazy val artifactory =
  sbt.settingKey[URL]("Artifactory URL")
lazy val loadedCredentials = sbt.settingKey[Either[String,DirectCredentials]]("attempt to get sbt credentials")
lazy val directCredentials = sbt.settingKey[DirectCredentials]("direct credentials")
lazy val pathToJar = sbt.taskKey[String]("path to the cliTools.jar")

lazy val cliTools = project
  .enablePlugins(GitVersioning)
  .enablePlugins(DockerPlugin,JavaAppPackaging)
  .dependsOn(common)
  .settings(
    assemblySettings,
    baseSettings,
    libraryDependencies ++= Seq(
      woodstox,
      sqlite,
      scalaUtilsGenomiclib,
      commonsNet,
      commonsCompress,
      scalaCsv,
      fingaleHtttp,
      jcommander,
      eclipsePersistence,
      javaxXmlBind
    ),
    useJGit,
    git.gitlabCiOverride := true,
//git.targetVersionFile := "../targetVersion"
    dockerLabels := Map("gitCommit" -> s"${git.formattedShaVersion.value.getOrElse("unknown")}@${git.gitCurrentBranch.value}"),
    dockerBaseImage := "openjdk:12",
    dockerRepository in Docker := Some("registry.gitlab.com"),
    dockerUsername in Docker := Some("uit-sfb/newpan-tools"),
//    daemonUser in Docker := "sfb", //"in Docker" needed for this parameter
    loadedCredentials := Credentials.loadCredentials(Path.userHome / ".sbt" / ".credentials"),
    directCredentials := loadedCredentials.value.fold(fa => new DirectCredentials("","","",""), fb => fb),
    artifactory := new URL("https://artifactory.metapipe.uit.no/artifactory/generic-local/no.uit.sfb/"),
    pathToJar := s"${(artifactPath in (Compile,packageBin)).value.toPath.getParent}/${(assemblyJarName in assembly).value}",
      publishCliTools := {
      exec(directCredentials.value.userName,directCredentials.value.passwd,artifactory.value,pathToJar.value)
    }
  )

lazy val root = (project in file("."))
  .settings(
    name := "metapipeBackend",
    publish := {},
    publishLocal := {},
    projectVersion := {version.value},
    projectScalaVersion := {scalaVersion.value},
    assemblySettings)
  .aggregate(common, cliTools)
