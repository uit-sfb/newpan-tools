package no.uit.metapipe.testdata

import java.io.InputStream

object TestData {
  def streamFor(name: String): InputStream = getClass.getResourceAsStream(name)

  def testDataTsv = streamFor("testData.tsv")
}
