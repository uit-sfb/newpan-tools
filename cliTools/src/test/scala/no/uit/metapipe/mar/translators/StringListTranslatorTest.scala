package no.uit.metapipe.mar.translators

import no.uit.sfb.schemas.mar.ObjectFactory
import org.scalatest.{Matchers, FunSpec}

import scala.collection.JavaConversions._

class StringListTranslatorTest extends FunSpec with Matchers {
  val of = new ObjectFactory

  val translator = StringListTranslator("test",
                                        ':',
                                        _.setGeoLocName(of.createStringList()),
                                        _.getGeoLocName.getItem,
                                        _.getGeoLocNameMissing,
                                        _.setGeoLocNameMissing)

  describe("StringListTranslator") {
    it("should translate a list of identifiers from TSV to XML") {
      val marRecord = of.createMarRecord()
      translator.rowToXml(Map("test" -> "a:b:c"), marRecord)
      List("a", "b", "c") shouldEqual marRecord.getGeoLocName.getItem.toList
    }

    it("should translate a list of identifiers from XML to TSV") {
      val marRecord = of.createMarRecord()
      marRecord.setGeoLocName(of.createStringList())
      val ids = marRecord.getGeoLocName.getItem
      ids.add("a")
      ids.add("b")
      ids.add("c")
      Map("test" -> "a:b:c") shouldEqual translator.xmlToRow(marRecord)
    }
  }
}
