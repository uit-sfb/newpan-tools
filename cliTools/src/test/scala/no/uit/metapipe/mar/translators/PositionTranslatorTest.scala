package no.uit.metapipe.mar.translators

import no.uit.sfb.schemas.mar.MarRecord
import org.scalatest.{Matchers, FunSpec}

class PositionTranslatorTest extends FunSpec with Matchers {
  describe("PosistionTranslator") {
    val record = new MarRecord
    val translator = PositionTranslator("lat_lon",
                                        _.getLatLon,
                                        _.getLatLonMissing,
                                        _.setLatLon,
                                        _.setLatLonMissing)

    TranslatorTestHelper(translator,
                         Seq(
                           Map("lat_lon" -> "69.692320, 18.973551"),
                           Map("lat_lon" -> "unknown")
                         ))
  }
}
