package no.uit.metapipe.mar.translators

import no.uit.metapipe.mar.Translator
import no.uit.metapipe.mar.exceptions.ParserException
import no.uit.sfb.schemas.mar.{ObjectFactory, InvestigationType, MarRecord}
import org.scalatest.{Assertions, Matchers, FunSpec}

/*
  it("should translate XXX from TSV to XML") {
    pending
  }

  it("should translate XXX from XML to TSV") {
    pending
  }

  it("should translate XXX from TSV to XML to TSV") {
    pending
  }
 */

object TranslatorTestHelper {
  private val objectFactory = new ObjectFactory

  def apply(translator: Translator,
            examples: Seq[Map[String, String]]): Unit = {
    val record = objectFactory.createMarRecord()
    translator.xmlToRow(record)

    for (row <- examples) {
      val marRecord = objectFactory.createMarRecord()
      translator.rowToXml(row, marRecord)
      val result = translator.xmlToRow(marRecord)
      assert(result == row, s"$result did not equal $row")
    }
  }
}

class MixsTranslatorTest extends FunSpec with Matchers {
  describe("Mar Translators") {
    describe("when translating 'MixS' attributes") {
      it("should translate investigation_type from TSV to XML") {
        val record = new MarRecord
        InvestigationTypeTranslator.rowToXml(
          Map("investigation_type" -> "Bacteria_Archaea"),
          record)
        record.getInvestigationType shouldEqual InvestigationType.BACTERIA_ARCHAEA
      }

      it("should throw appropriate error messages for undefined values when translating from TSV to XML") {
        val ex = Assertions.intercept[ParserException] {
          val record = new MarRecord
          InvestigationTypeTranslator.rowToXml(
            Map("investigation_type" -> "NON-EXISTING-VALUE"),
            record)
        }
        ex.errors.exists(_.attribute == "investigation_type") shouldEqual true
      }

      it("should translate investigation_type from XML to TSV") {
        val record = new MarRecord
        record.setInvestigationType(InvestigationType.BACTERIA_ARCHAEA)
        val row = InvestigationTypeTranslator.xmlToRow(record)
        row("investigation_type") shouldEqual "Bacteria_Archaea"
      }
    }

    describe("when translating 'Microbe version 1.0 Package' attributes") {}

    describe("when translating 'SfB' attributes") {}

    describe("when translating 'BacDive' attributes") {}

    describe("when translating 'PATRIC' attributes") {
      it("should translate publication_pmid from TSV to XML to TSV") {
        val translator =
          Translators.PublicationPmid("publication_pmid",
                                      _.getPublicationPmid,
                                      _.getPublicationPmidMissing,
                                      _.setPublicationPmid,
                                      _.setPublicationPmidMissing)
        TranslatorTestHelper(
          translator,
          Seq(
            Map("publication_pmid" -> "10319509,10.1007/BF00210994,https://www.ncbi.nlm.nih.gov/pubmed")
          ))
      }
      it("should translate other_typing from TSV to XML to TSV") {
        val translator = OtherTypingTranslator
        TranslatorTestHelper(translator,
                             Seq(
                               Map("other_typing" -> "genotype:Wild type")
                             ))
      }

    }

    describe(
      "when translating 'Pathogen: clinical or host-associated version 1.0 Package' attributes") {}
  }
}
