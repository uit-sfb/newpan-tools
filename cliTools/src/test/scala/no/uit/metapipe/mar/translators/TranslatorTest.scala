package no.uit.metapipe.mar.translators

import no.uit.metapipe.mar.exceptions.ParserException
import no.uit.metapipe.mar.{AttributeError, Translator}
import no.uit.sfb.schemas.mar.MarRecord
import org.scalatest.{Matchers, FunSpec}
import org.scalatest.Assertions

class TranslatorTest extends FunSpec with Matchers {
  val stubTranslator = new Translator {

    override def attributeNames: Set[String] = Set("test")

    override def rowToXml(tableRow: Map[String, String],
                          marRecord: MarRecord): Unit = ???
    override def xmlToRow(marRecord: MarRecord): Map[String, String] =
      Map("test" -> "hello")
  }

  class ErrorTranslator(errors: Seq[AttributeError]) extends Translator {

    override def attributeNames: Set[String] = Set("test")

    override def xmlToRow(marRecord: MarRecord): Map[String, String] = ???
    override def rowToXml(tableRow: Map[String, String],
                          marRecord: MarRecord): Unit = {
      throw new ParserException(errors)
    }
  }

  describe("Translator") {
    describe("merging multiple translators") {
      it("should fail if XML -> Row returns more than one column with the same name") {
        Assertions.assertThrows[IllegalArgumentException] {
          val t = Translator(Seq(stubTranslator, stubTranslator))
          t.xmlToRow(null)
        }
      }

      it("should merge parser errors for each attribute into one") {
        val translator = Translator(
          Seq(
            new ErrorTranslator(
              Seq(AttributeError("attribute_1", "attribute 1 error"))),
            new ErrorTranslator(
              Seq(
                AttributeError("attribute_2", "attribute 2 error"),
                AttributeError("attribute_2", "attribute 2 error 2")
              ))
          ))

        val ex = Assertions.intercept[ParserException] {
          translator.rowToXml(null, null)
        }
        ex.errors.toSet shouldEqual Set(
          AttributeError("attribute_1", "attribute 1 error"),
          AttributeError("attribute_2", "attribute 2 error"),
          AttributeError("attribute_2", "attribute 2 error 2")
        )
      }
    }
  }
}
