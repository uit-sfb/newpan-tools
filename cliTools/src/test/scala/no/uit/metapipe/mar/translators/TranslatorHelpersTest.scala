package no.uit.metapipe.mar.translators

import javax.xml.datatype.{DatatypeFactory, XMLGregorianCalendar}

import org.scalatest.{Matchers, FunSpec}

class TranslatorHelpersTest extends FunSpec with Matchers {
  def calendar = {
    val dataTypeFactory = DatatypeFactory.newInstance()
    dataTypeFactory.newXMLGregorianCalendar(2016, 12, 2, 11, 28, 55, 19, 1)
  }

  describe("TranslatorHelpers") {
    describe("PrecisionDateTime translators") {
      it("should serialize year correctly") {
        val pd = objectFactory.createPrecisionDateTimeFixed()
        pd.setYear(calendar)
        val serialized = TranslatorHelpers.serializePrecisionDateTimeFixed(pd)
        serialized shouldEqual "2016"
      }

      it("should serialize yearMonth correctly") {
        val pd = objectFactory.createPrecisionDateTimeFixed()
        pd.setYearMonth(calendar)
        val serialized = TranslatorHelpers.serializePrecisionDateTimeFixed(pd)
        serialized shouldEqual "2016-12"
      }

      it("should serialize date correctly") {
        val pd = objectFactory.createPrecisionDateTimeFixed()
        pd.setDate(calendar)
        val serialized = TranslatorHelpers.serializePrecisionDateTimeFixed(pd)
        serialized shouldEqual "2016-12-02"
      }

      it("should serialize dateTime correctly") {
        val pd = objectFactory.createPrecisionDateTimeFixed()
        pd.setDateTime(calendar)
        val serialized = TranslatorHelpers.serializePrecisionDateTimeFixed(pd)
        serialized shouldEqual "2016-12-02T11:28:55.019+00:01"
      }

      it("should deserialize year correctly") {
        val dt = TranslatorHelpers.parsePrecisionDateTimeFixed("2016")
        dt.getYear.getYear shouldBe calendar.getYear
        dt.getYearMonth shouldBe null
        dt.getDate shouldBe null
        dt.getDateTime shouldBe null
      }

      it("should deserialize yearMonth correctly") {
        val dt = TranslatorHelpers.parsePrecisionDateTimeFixed("2016-12")
        dt.getYear shouldBe null
        dt.getYearMonth.getYear shouldBe 2016
        dt.getYearMonth.getMonth shouldBe 12
        dt.getDate shouldBe null
        dt.getDateTime shouldBe null
      }

      it("should deserialize date correctly") {
        val dt = TranslatorHelpers.parsePrecisionDateTimeFixed("2016-12-02")
        dt.getYear shouldBe null
        dt.getYearMonth shouldBe null
        dt.getDate.getYear shouldBe 2016
        dt.getDate.getMonth shouldBe 12
        dt.getDate.getDay shouldBe 2
        dt.getDateTime shouldBe null
      }

      it("should deserialize dateTime correctly") {
        val dt = TranslatorHelpers.parsePrecisionDateTimeFixed(
          "2016-12-02T11:28:55.019+00:01")
        dt.getYear shouldBe null
        dt.getYearMonth shouldBe null
        dt.getDate shouldBe null
        dt.getDateTime.getYear shouldBe 2016
        dt.getDateTime.getMonth shouldBe 12
        dt.getDateTime.getDay shouldBe 2

        dt.getDateTime.getHour shouldBe 11
        dt.getDateTime.getMinute shouldBe 28
        dt.getDateTime.getSecond shouldBe 55

        dt.getDateTime.getMillisecond shouldBe 19
        dt.getDateTime.getTimezone shouldBe 1
      }
    }
  }
}
