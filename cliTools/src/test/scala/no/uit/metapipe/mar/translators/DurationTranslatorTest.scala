package no.uit.metapipe.mar.translators

import org.scalatest.{Matchers, FunSpec}

class DurationTranslatorTest extends FunSpec with Matchers {
  val translator = Translators.Duration("host_age",
                                        _.getHostAge,
                                        _.getHostAgeMissing,
                                        _.setHostAge,
                                        _.setHostAgeMissing)

  describe("DurationTranslator") {
    it("should translate fixed durations") {
      TranslatorTestHelper(translator,
                           Seq(
                             Map("host_age" -> "5.0 years"),
                             Map("host_age" -> "3 decades"),
                             Map("host_age" -> "unknown")
                           ))
    }
    it("should translate duration ranges") {
      TranslatorTestHelper(translator,
                           Seq(
                             Map("host_age" -> "5.0-6 years"),
                             Map("host_age" -> "3-5.0 decades"),
                             Map("host_age" -> "unknown")
                           ))
    }
  }
}
