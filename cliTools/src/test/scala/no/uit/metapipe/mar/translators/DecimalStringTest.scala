package no.uit.metapipe.mar.translators

import no.uit.metapipe.mar.exceptions.ParserException
import org.scalatest.{FunSpec, Matchers}

class DecimalStringTest extends FunSpec with Matchers {
  val translator = Translators.DecimalString("silicate",
                                             _.getSilicate,
                                             _.getSilicateMissing,
                                             _.setSilicate,
                                             _.setSilicateMissing)

  describe("DecimalString") {
    it("should accept integers") {
      TranslatorTestHelper(translator,
                           Seq(
                             Map("silicate" -> "123")
                           ))
    }

    it("should accept decimals") {
      TranslatorTestHelper(translator,
                           Seq(
                             Map("silicate" -> "123.456")
                           ))
    }

    it("should fail on invalid input") {
      assertThrows[ParserException] {
        TranslatorTestHelper(translator,
                             Seq(
                               Map("silicate" -> "123.123 m")
                             ))
      }
    }
  }
}
