package no.uit.metapipe.mar.translators

import no.uit.metapipe.mar.Translator
import org.scalatest.{Matchers, FunSpec}

class TranslatorFactoryTest extends FunSpec with Matchers {
  val record = objectFactory.createMarRecord()

  describe("Translators") {
    it("should translate .PrecisionDecimalString") {
      val depthTranslator =
        Translators.PrecisionDecimalString("depth",
                                           _.getDepth,
                                           _.getDepthMissing,
                                           _.setDepth,
                                           _.setDepthMissing)
      TranslatorTestHelper(depthTranslator,
                           Seq(
                             Map("depth" -> "355.0"),
                             Map("depth" -> "2.00|5.00"),
                             Map("depth" -> "-1.5|-0.2"),
                             Map("depth" -> "missing"),
                             Map("depth" -> "unknown")
                           ))
    }
    describe(".Rrnas") {
      val rrnasTranslator = RrnasTranslator("completeRrnas",
                                            _.getCompleteRrnas,
                                            _.getCompleteRrnasMissing,
                                            _.setCompleteRrnas,
                                            _.setCompleteRrnasMissing)
      TranslatorTestHelper(rrnasTranslator,
                           Seq(
                             Map("completeRrnas" -> "1, 0, 4"),
                             Map("completeRrnas" -> "0, 3, 4"),
                             Map("completeRrnas" -> "missing"),
                             Map("completeRrnas" -> "unknown")
                           ))
    }
    describe(".PrecisionDateTime") {
      val pdtTranslator =
        Translators.PrecisionDateTime("collectionDate",
                                      _.getCollectionDate,
                                      _.getCollectionDateMissing,
                                      _.setCollectionDate,
                                      _.setCollectionDateMissing)
      TranslatorTestHelper(
        pdtTranslator,
        Seq(
          Map("collectionDate" -> "2016"),
          Map("collectionDate" -> "2016-12"),
          Map("collectionDate" -> "2016-12-02"),
          Map("collectionDate" -> "2016-12-02T11:28:55.019+00:01"),
          Map("collectionDate" -> "2016--2020"),
          Map("collectionDate" -> "2016-12--2020-11"),
          Map("collectionDate" -> "2016-12-02--2020-11-11"),
          Map(
            "collectionDate" -> "2016-12-02T11:28:55.019+00:01--2020-12-02T11:28:55.019+00:01"),
          Map("collectionDate" -> "missing"),
          Map("collectionDate" -> "unknown")
        )
      )
    }

    describe("PublicationPmid") {
      it("should have the correct regexes") {
        val DOI = Translators.PublicationPmid.DOI
        val PMID = Translators.PublicationPmid.PMID

        DOI.findFirstIn("10.1007/BF00210994") shouldBe defined
        PMID.findFirstIn("15393697") shouldBe defined
      }
    }

    describe("BiosampleAccession") {
      val bioTranslatorRef =
        Translators.Accession(Translator.accessions.bioSample)(
          "biosample_accession",
          _.getBiosampleAccession,
          _.getBiosampleAccessionMissing,
          _.setBiosampleAccession,
          _.setBiosampleAccessionMissing)
      TranslatorTestHelper(
        bioTranslatorRef,
        Seq(
          Map("biosample_accession" -> "http://www.ebi.ac.uk/biosamples/samples/ERS240714")
        ))
      val bioTranslatorCat =
        Translators.Accession(Translator.accessions.bioSample)(
          "biosample_accession",
          _.getBiosampleAccession,
          _.getBiosampleAccessionMissing,
          _.setBiosampleAccession,
          _.setBiosampleAccessionMissing)
      TranslatorTestHelper(
        bioTranslatorCat,
        Seq(
          Map("biosample_accession" -> "http://www.ebi.ac.uk/biosamples/search?searchTerm=ERS240714")
        ))
    }
  }
}
