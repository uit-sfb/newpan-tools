package no.uit.metapipe.mar.translators

import org.scalatest.{FunSpec, Matchers}

class PrecisionTimeTranslatorTest extends FunSpec with Matchers {
  val translator = Translators.PrecisionTime("collection_time",
                                             _.getCollectionTime,
                                             _.getCollectionTimeMissing,
                                             _.setCollectionTime,
                                             _.setCollectionTimeMissing)

  describe("PrecisionTimeTranslator") {
    it("should translate unknowns") {
      TranslatorTestHelper(translator,
                           Seq(
                             Map("collection_time" -> "unknown")
                           ))
    }

    it("should translate fixed time") {
      TranslatorTestHelper(translator,
                           Seq(
                             Map("collection_time" -> "13:37:00")
                           ))
    }

    it("should translate time ranges") {
      TranslatorTestHelper(translator,
                           Seq(
                             Map("collection_time" -> "06:45:00|12:00:00")
                           ))
    }
  }
}
