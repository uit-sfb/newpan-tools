package no.uit.metapipe.mar.translators

import java.io.{ByteArrayInputStream, ByteArrayOutputStream}
import java.nio.charset.StandardCharsets

import no.uit.metapipe.testdata.TestData
import no.uit.metapipe.mar.{DatabaseType, TsvXmlTranslator, XmlTsvTranslator}
import no.uit.sfb.schemas.mar.ObjectFactory
import org.apache.commons.io.IOUtils
import org.scalatest.{FunSpec, Matchers}

class TranslatorIntegrationTest extends FunSpec with Matchers {
  describe("TSV->XML->TSV translators") {
    val types = Seq(
      DatabaseType.MarCat,
      DatabaseType.MarDB,
      DatabaseType.MarRef
    )

    for (typ <- types) {
      it(s"should translate all the attributes for ${typ.name}") {
        val inputRows: Map[String, String] =
          typ.tsvHeaderNames.map(name => name -> "missing").toMap
        val record = (new ObjectFactory).createMarRecord()
        typ.translator.rowToXml(inputRows, record)
        val outputRows = typ.translator.xmlToRow(record)
        val inputKeys = inputRows.keySet
        val outputKeys = outputRows.keySet

        // This line should be equivalent to the line below, but it produces a more friendly error message.
        val diff = ((inputKeys union outputKeys) -- (inputKeys intersect outputKeys)).toList.sorted shouldEqual List()

        inputKeys shouldEqual outputKeys // same as above (in theory)
      }
    }

    it("should be able to translate TSV to XML and back to the same TSV") {
      pending
      //val tsvIn = IOUtils.toByteArray(TestData.testDataTsv)
      val tsvIn = TestData.testDataTsv
      val xmlOut = new ByteArrayOutputStream()
      val jsonOut = new ByteArrayOutputStream()

      //TsvXmlTranslator(tsvIn, () => xmlOut)
      val tsvXmlTranslator = new TsvXmlTranslator(DatabaseType.MarRef)
      val recordsIn = tsvXmlTranslator.parseTsv(tsvIn)
      tsvXmlTranslator.writeXml(recordsIn, () => xmlOut, () => jsonOut)

      val xmlIn = new ByteArrayInputStream(xmlOut.toByteArray)
      val tsvOut = new ByteArrayOutputStream()

      //XmlTsvTranslator(xmlIn, () => tsvOut)
      val xmlTsvTranslator = new XmlTsvTranslator(DatabaseType.MarRef)
      val recordsOut = xmlTsvTranslator.xmlToTsv(xmlIn)
      xmlTsvTranslator.writeTsv(recordsOut, () => tsvOut)

      val tsvInStr = IOUtils
        .toString(TestData.testDataTsv, StandardCharsets.UTF_8)
        .replace("\r", "")
        .stripLineEnd
      val tsvOutStr =
        new String(tsvOut.toByteArray, "UTF-8").replace("\r", "").stripLineEnd

      recordsIn zip recordsOut foreach {
        case (rowIn, rowOut) =>
          rowIn.keySet shouldEqual xmlTsvTranslator.headers.toSet
          rowOut.keySet shouldEqual xmlTsvTranslator.headers.toSet

          // if this fails it could be because a tsv parsing error is causing a record to be skipped
          rowIn("base_ID") shouldEqual rowOut("base_ID")

          xmlTsvTranslator.headers.toSet.foreach { header: String =>
            Map(header -> rowIn(header)) shouldEqual Map(
              header -> rowOut(header))
          }
      }
      tsvInStr shouldEqual tsvOutStr // are the columns in the same order?
    }
  }
}
