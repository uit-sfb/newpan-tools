package no.uit.metapipe

import no.uit.metapipe.format.{RunFastaCommand, RunFastaUtils}
import no.uit.metapipe.uniref.{
  CreateDatabaseCommand,
  CreateFastaCommand,
  RunCreateUnirefDatabase,
  RunCreateUnirefFasta
}
import no.uit.metapipe.mar._
import no.uit.metapipe.utils.CommandLineApplication

object Main extends CommandLineApplication {
  parseAndStartApplication(
    Map(
      //Uniref
      new CreateDatabaseCommand -> RunCreateUnirefDatabase,
      new CreateFastaCommand -> RunCreateUnirefFasta,
      //mar
      new MarConverterCommand -> RunMarConverter,
      new RunXmlTsvCommand -> RunXmlTsv,
      new RunTsvXmlCommand -> RunTsvXml,
      new RunExportKmlCommand -> RunExportKml,
      new RunFastaCommand -> RunFastaUtils,
      new ApiServerCommand -> RunApiServer
    ))(args)
}
