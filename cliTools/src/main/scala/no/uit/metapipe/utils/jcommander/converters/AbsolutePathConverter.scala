package no.uit.metapipe.common.utils.jcommander.converters

import java.nio.file.Path

import com.beust.jcommander.converters.PathConverter

class AbsolutePathConverter extends PathConverter {
  override def convert(value: String): Path = {
    super.convert(value).toAbsolutePath.normalize
  }
}
