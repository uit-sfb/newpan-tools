package no.uit.metapipe.utils

import java.nio.file.{Files, Path}

import no.uit.metapipe.common.utils.PosixModes
import no.uit.sfb.scalautils.common.FileUtils
import org.apache.commons.compress.archivers.zip.ZipFile

import scala.collection.JavaConverters._

object Zip {
  @unchecked
  def extract(inputArchive: Path,
              outputDir: Path,
              filter: String => Boolean = _ => true): Unit = {

    val zipFile = new ZipFile(inputArchive.toFile)
    try {
      val entries = zipFile.getEntries
      entries.asScala.foreach { entry =>
        val entryName = entry.getName.stripPrefix("/")
        if (filter(entryName)) {
          val entryDestination = outputDir.resolve(entryName)
          if (entry.isDirectory) {
            FileUtils.createDirs(entryDestination)
          } else {
            FileUtils.createParentDirs(entryDestination)
            val in = zipFile.getInputStream(entry)
            FileUtils.streamToFile(entryDestination, in)
          }
          if (entry.getUnixMode != 0) {
            Files.setPosixFilePermissions(entryDestination,
                                          PosixModes.fromInt(entry.getUnixMode))
          }
        }
      }
    } finally {
      zipFile.close()
    }

  }
}
