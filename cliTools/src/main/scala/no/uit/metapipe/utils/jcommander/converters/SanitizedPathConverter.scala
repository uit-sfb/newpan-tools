package no.uit.metapipe.common.utils.jcommander.converters

import java.nio.file.Path

import com.beust.jcommander.IStringConverter

class SanitizedPathConverter extends AbsolutePathConverter {
  override def convert(value: String): Path = {
    require(!value.contains(" "))
    val sanitized =
      if (value.head == '~')
        System.getenv("HOME") ++ value.tail
      else
        value
    super.convert(sanitized)
  }
}

class OptionSanitizedPathConverter extends IStringConverter[Option[Path]] {
  override def convert(value: String): Option[Path] = {
    if (value.isEmpty)
      None
    else
      Some((new SanitizedPathConverter).convert(value))
  }
}
