package no.uit.metapipe.common.utils.jcommander.converters

import java.net.{MalformedURLException, URL}

import com.beust.jcommander.IStringConverter

import scala.util.Try

class SanitizedUrlConverter extends IStringConverter[URL] {
  override def convert(value: String): URL = {
    require(!value.contains(" "))
    Try {
      new URL(value)
    } recover {
      case e: MalformedURLException =>
        val sanitizedPath = new SanitizedPathConverter().convert(value)
        new URL("file:" + sanitizedPath.toString)
    }
  }.get
}

class OptionSanitizedUrlConverter extends IStringConverter[Option[URL]] {
  override def convert(value: String): Option[URL] = {
    if (value.isEmpty)
      None
    else
      Some((new SanitizedUrlConverter).convert(value))
  }
}
