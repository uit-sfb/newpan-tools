package no.uit.metapipe.utils

import java.nio.file.Path
import sys.process._

object Wget {
  def download(uri: String, dest: Path): Unit = {
    s"wget -q -O $dest $uri".!
  }
}
