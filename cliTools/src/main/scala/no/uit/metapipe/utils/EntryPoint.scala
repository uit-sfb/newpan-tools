package no.uit.metapipe.utils

import rx.Ctx

trait EntryPoint[T <: CommandLike] {
  implicit val ctx: Ctx.Owner = Ctx.Owner.safe()
  final def apply(args: CommandLike): Unit = {
    exec(args.asInstanceOf[T])
  }
  def exec(args: T): Unit
}
