package no.uit.metapipe.utils

import com.beust.jcommander.{JCommander, Parameter, ParameterException}
import no.uit.metapipe.common.logging.Logging

import scala.language.existentials

/**
  * Starting point of our applications.
  */
abstract class CommandLineApplication extends App with Logging {
  object MainArgs {
    @Parameter(names = Array("-h", "--help"), help = true)
    var help = false
  }

  def parseAndStartApplication(
      commands: Map[CommandLike, EntryPoint[_ <: CommandLike]])(
      args: Array[String]): Unit = {
    def newJc(): JCommander = {
      val jc = JCommanderFactory.getInstance(MainArgs)
      commands.keySet foreach { k =>
        jc.addCommand(k.cmd, k)
      }
      jc
    }

    val jc1 = newJc()
    try { jc1.parseWithoutValidation(args: _*) } catch {
      case e: ParameterException => //Do nothing here, but leave the exception catching
    }
    if (MainArgs.help) {
      jc1.usage()
    } else {
      val (arguments, function) = {
        commands find {
          case (k, _) =>
            k.cmd == jc1.getParsedCommand
        } match {
          case Some(cmd) => cmd
          case None =>
            throw new RuntimeException(
              s"Command ${jc1.getParsedCommand} not found (available commands are: ${commands
                .map {
                  _._1.cmd
                }
                .mkString(", ")})")
        }
      }

      function(arguments)
    }
  }
}
