package no.uit.metapipe.common.utils.jcommander.validators

import com.beust.jcommander.{IValueValidator, ParameterException}

class OneOfValidator(options: Set[String]) extends IValueValidator[String] {
  def validate(parameterName: String, parameterValue: String): Unit = {
    if (!options.contains(parameterValue))
      throw new ParameterException(
        s"Parameter $parameterName must be one of $options but '$parameterValue' found")
  }
}

class MapseqDbValidator
    extends OneOfValidator(Set("none", "default", "silvamar"))

class KaijuDbValidator extends OneOfValidator(Set("none", "default", "mardb"))
