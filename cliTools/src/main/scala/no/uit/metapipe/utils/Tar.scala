package no.uit.metapipe.utils

import java.nio.file.{Files, Path}
import java.util.zip.GZIPOutputStream

import no.uit.metapipe.common.utils.PosixModes
import no.uit.sfb.scalautils.common.FileUtils
import org.apache.commons.compress.archivers.ArchiveStreamFactory
import org.apache.commons.compress.archivers.tar.{
  TarArchiveEntry,
  TarArchiveOutputStream
}
import org.apache.commons.io.IOUtils
import sys.process._

object Tar {

  /**
    * Extracts only a subset of files
    */
  def extract(archive: Path, dest: Path, filesToExtract: String*): Unit = {
    if (!FileUtils.exists(dest))
      Files.createDirectories(dest)
    val z =
      if (archive.getFileName.endsWith(".gz") || archive.getFileName.endsWith(
            ".tgz"))
        "z"
      else ""
    s"tar xf$z $archive -C $dest ${filesToExtract.mkString(" ")}".!
  }

  def extractGz(archive: Path, dest: Path): Unit = {
    s"gzip -dk $archive".!
    FileUtils.moveDir(archive.getParent.resolve(
                        archive.getFileName.toString
                          .split('.')
                          .init
                          .mkString(".")), //We remove the trailing .gz
                      dest)
  }

  /** Puts existing files into a new tar archive
    *
    * @param baseDir All paths within the archive will be relative to baseDir. For example,
    *                if baseDir is /a/b/c/toolName and an input file is /a/b/c/toolName/toolFile then
    *                the resulting filename within the archive will be toolName/toolFile.
    * @param inputs Sequence of input files that should go into the archive.
    * @param outputFile Path to the new archive. Supports the following file endings: .tar, .tar.gz, .tgz.
    */
  def write(baseDir: Path, inputs: Seq[Path], outputFile: Path): Unit = {
    inputs.foreach(
      input =>
        require(input.startsWith(baseDir),
                s"$input (input) is not contained within $baseDir (baseDir)"))

    val os = {
      val fos = Files.newOutputStream(outputFile)
      val fn = outputFile.getFileName.toString
      if (fn.endsWith(".gz") || fn.endsWith(".tgz")) {
        new GZIPOutputStream(fos)
      } else {
        fos
      }
    }

    val tarOut = new ArchiveStreamFactory()
      .createArchiveOutputStream("tar", os)
      .asInstanceOf[TarArchiveOutputStream]

    inputs.foreach { input =>
      val relativePath = baseDir.relativize(input)
      val entry = tarOut
        .createArchiveEntry(input.toFile, relativePath.toString)
        .asInstanceOf[TarArchiveEntry]
      val mode = PosixModes.toInt(Files.getPosixFilePermissions(input))
      entry.setMode(mode)

      tarOut.putArchiveEntry(entry)
      val fis = Files.newInputStream(input)
      IOUtils.copy(fis, tarOut)
      tarOut.closeArchiveEntry()
    }

    tarOut.finish()
    tarOut.close()
  }
}
