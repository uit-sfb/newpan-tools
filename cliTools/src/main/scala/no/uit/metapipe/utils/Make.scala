package no.uit.metapipe.utils

import java.nio.file.Path

object Make {
  def apply(path: Path): Unit = {
    val pb = new ProcessBuilder("make")
    pb.directory(path.toFile)
    pb.inheritIO()
    val res = pb.start().waitFor()
    if (res != 0) {
      throw new IllegalStateException("Make failed!")
    }
  }
}
