package no.uit.metapipe.utils

import no.uit.sfb.scalautils.json.Json

case class GitHubRelease(name: String,
                         tag_name: String,
                         draft: Boolean,
                         prerelease: Boolean,
                         tarball_url: String,
                         assets: Seq[GitHubAsset])
case class GitHubAsset(name: String, browser_download_url: String)

object GitHub {
  def releases(owner: String, repo: String): List[GitHubRelease] = {
    require(!owner.contains("/"), "owner and repo cannot contain /")
    require(!repo.contains("/"), "owner and repo cannot contain /")

    val timeoutMs = 10000
    val uri = s"https://api.github.com/repos/$owner/$repo/releases"
    val response = scalaj.http
      .Http(uri)
      .method("GET")
      .timeout(timeoutMs, timeoutMs)
      .asString

    if (response.code != 200) {
      throw new IllegalStateException(
        s"invalid response code from GitHub: ${response.code} GET $uri")
    }

    Json.parse[List[GitHubRelease]](response.body)
  }
}
