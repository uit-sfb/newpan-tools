package no.uit.metapipe.utils

import java.nio.file.{Path, Paths}
import collection.JavaConverters._
import scala.language.implicitConversions

trait CommandLike {
  def cmd: String
  implicit final def toAbsolutePath(str: String): Path =
    Paths.get(str).toAbsolutePath.normalize
  implicit final def toJavaList[T](list: Seq[T]): java.util.List[T] =
    scala.collection.mutable.Buffer[T](list: _*).asJava
  implicit final def fromJavaList[T](list: java.util.List[T]): Seq[T] =
    list.asScala
}
