package no.uit.metapipe.utils

import java.net.URI
import java.nio.file.Path

import no.uit.sfb.scalautils.common.FileUtils
import org.apache.commons.net.ftp.{FTP, FTPClient}

object Ftp {
  def withClient[T](host: String, port: Int = 21)(f: FTPClient => T): T = {
    val client = new FTPClient
    try {
      client.connect(host, port)
      client.enterLocalPassiveMode()
      client.login("anonymous", "anonymous")
      client.setFileType(FTP.BINARY_FILE_TYPE)
      f(client)
    } finally {
      if (client.isConnected) {
        client.logout()
        client.disconnect()
      }
    }
  }

  def download(uri: URI, dest: Path): Unit = {
    require(uri.getScheme == "ftp")
    val port = if (uri.getPort < 0) 21 else uri.getPort
    withClient(uri.getHost, port) { client =>
      val is = client.retrieveFileStream(uri.getPath)
      FileUtils.streamToFile(dest, is)
    }
  }
}
