package no.uit.metapipe.format

import java.io.BufferedInputStream
import java.nio.file.{Files, Paths}

import com.beust.jcommander.{Parameter, Parameters}
import no.uit.metapipe.utils.{CommandLike, EntryPoint}
import no.uit.sfb.genomic.parser.FastaRecord
import no.uit.sfb.scalautils.common.FileUtils
import no.uit.sfb.scalautils.common.data.StreamHolder

object RunFastaUtils extends EntryPoint[RunFastaCommand] {
  def exec(args: RunFastaCommand): Unit = {
    import no.uit.sfb.genomic.parser.Records._
    val fasta: FastaRecord =
      FastaRecord.fromStream(
        StreamHolder(
          FileUtils.unGzipStream(new BufferedInputStream(
            Files.newInputStream(Paths.get(args.inputFile))))))

    val (filteredFasta: FastaRecord, removedDuplicates: Int) =
      if (args.removeDuplicates) {
        val filtered = (fasta.groupBy(_.id) map {
          case (_, entries) => entries.head
        }).toSeq
        (filtered, fasta.length - filtered.length)
      } else
        (fasta, 0)

    filteredFasta.toFile(Paths.get(args.outputFile), false)

    if (args.printStatistics) {
      System.err.println(s"Number of removed duplicates: $removedDuplicates")
    }
  }
}
@Parameters(commandDescription = "FASTA file utilities")
class RunFastaCommand extends CommandLike {
  val cmd = "fasta"

  @Parameter(names = Array("-i", "--input-file"),
             description = "Input FASTA file. Use - (dash) for `stdin`",
             required = true)
  var inputFile: String = _

  @Parameter(names = Array("-o", "--output-file"),
             description = "Output FASTA file. Use - (dash) for `stdout`.")
  var outputFile: String = "-"

  @Parameter(
    names = Array("-r", "--remove-duplicates"),
    description = "Remove any fasta sequence that has the same ID as one that's already been read. " +
      "This method does not compare the actual contents of the sequences, only the IDs."
  )
  var removeDuplicates: Boolean = false

  @Parameter(names = Array("-s", "--print-statistics"),
             description = "Print statistics to stderr")
  var printStatistics: Boolean = false
}
