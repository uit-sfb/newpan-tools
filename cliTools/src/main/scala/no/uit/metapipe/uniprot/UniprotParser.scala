package no.uit.metapipe.uniprot

import java.io.InputStream

import javax.xml.bind.JAXBContext
import javax.xml.stream.XMLStreamConstants._
import com.ctc.wstx.stax.WstxInputFactory
import no.uit.metapipe.common.logging.Logging
import org.uniprot.uniprot.DbReferenceType
import org.uniprot.uniprot.Entry

import scala.collection.JavaConverters._

object UniprotParser {
  def apply(inputStream: InputStream): Iterator[Record] =
    new UniprotParserImpl(inputStream)
}

case class Record(
    id: String,
    annotation: String,
    sequence: String,
    taxonomyId: Option[String],
    pfamId: Option[String],
    emblId: Option[String],
    refSeqId: Option[String],
    `type`: String,
    organismName: Option[String]
)

class UniprotParserImpl(inputStream: InputStream)
    extends Iterator[Record]
    with Logging {
  //val xmlInputFactory = XMLInputFactory.newInstance()
  val xmlInputFactory = new WstxInputFactory

  val jaxbContext =
    JAXBContext.newInstance(classOf[org.uniprot.uniprot.ObjectFactory])
  log.debug("Using JAXB context: " + jaxbContext.getClass.getName)

  val unmarshaller = jaxbContext.createUnmarshaller()
  val xmlR = xmlInputFactory.createXMLStreamReader(inputStream)
  xmlR.nextTag()
  xmlR.require(START_ELEMENT, null, "uniprot")

  override def hasNext: Boolean =
    (xmlR.getEventType != END_DOCUMENT) && xmlR.getEventType != END_ELEMENT

  override def next(): Record = {
    var record: Option[Record] = None
    while (record == None) {
      while (xmlR.getEventType != START_ELEMENT || xmlR.getLocalName != "entry") { // todo: might fail
        xmlR.next()
      }
      xmlR.require(START_ELEMENT, null, "entry")
      val entry = unmarshaller.unmarshal(xmlR).asInstanceOf[Entry]
      try {
        record = parseRecord(entry)
        if (record.isEmpty) {
          log.warn(s"Unable to parse record.")
        }
      } catch {
        case e: Exception => log.warn(s"Unable to parse record.")
      }
      xmlR.next()
    }
    record.get
  }

  def parseRecord(entry: Entry): Option[Record] = {
    var pfamId: Option[String] = None
    var emblId: Option[String] = None
    var refSeqId: Option[String] = None
    for (ref: DbReferenceType <- entry.getDbReference.asScala) {
      ref.getType match {
        case "EMBL" =>
          val id = ref.getId
          ref.getProperty.asScala
            .filter(_.getType == "protein sequence ID")
            .foreach { seqId =>
              emblId = Some(s"$id ${seqId.getValue}")
            }
        case "RefSeq" =>
          val id = ref.getId
          val seqId = ref.getProperty.asScala
            .filter(_.getType == "nucleotide sequence ID")
            .foreach { seqId =>
              refSeqId = Some(s"$id ${seqId.getValue}")
            }
        case "Pfam" =>
          pfamId = Some(ref.getId)
        case _ =>
      }
    }

    val taxonomyId = entry.getOrganism.getDbReference.asScala
      .find(_.getType == "NCBI Taxonomy")
      .map(_.getId)
    val organismName =
      entry.getOrganism.getName.asScala
        .find(_.getType == "common")
        .map(_.getValue)

    val annotation = entry.getProtein.getRecommendedName.getFullName.getValue
    val sequence = entry.getSequence.getValue

    for (name <- entry.getName.asScala.headOption) yield {
      Record(
        id = name,
        taxonomyId = taxonomyId,
        pfamId = pfamId,
        emblId = emblId,
        refSeqId = refSeqId,
        `type` = "uniprot",
        organismName = organismName,
        annotation = annotation,
        sequence = sequence
      )
    }
  }
}
