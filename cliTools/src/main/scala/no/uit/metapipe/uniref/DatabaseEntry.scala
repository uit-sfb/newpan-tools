package no.uit.metapipe.uniref

import java.io.{ByteArrayInputStream, ByteArrayOutputStream}
import java.util.zip.{GZIPInputStream, GZIPOutputStream}

import javax.xml.bind.JAXBContext
import com.ctc.wstx.stax.{WstxInputFactory, WstxOutputFactory}
import no.uit.metapipe.common.logging.Logging
import org.uniprot.uniref.{EntryType, ObjectFactory, UniRef}

import scala.collection.JavaConverters._

case class DatabaseEntry(unirefId: String,
                         ncbiTaxonomyId: String,
                         gzippedXml: Array[Byte])

/* WARN: Not thread safe */
class DatabaseEntryConversions extends Logging {
  private object vars {
    val buffer = new ByteArrayOutputStream()

    val jaxbContext = JAXBContext.newInstance(classOf[ObjectFactory])
    log.debug("Using JAXB context: " + jaxbContext.getClass.getName)
    val marshaller = jaxbContext.createMarshaller()
    val unmarshaller = jaxbContext.createUnmarshaller()
    val xmlInputFactory = new WstxInputFactory

    val xmlOutputFactory = new WstxOutputFactory
  }

  def propertyMap(entry: EntryType): Map[String, String] =
    entry.getProperty.asScala.map { property =>
      property.getType -> property.getValue
    }.toMap

  def entryToDatabaseEntry(entry: EntryType): Option[DatabaseEntry] = {
    val properties = propertyMap(entry)
    for {
      ncbiTaxonomyId <- properties.get("common taxon ID")
    } yield {
      DatabaseEntry(entry.getId, ncbiTaxonomyId, entryToGzippedXml(entry))
    }
  }

  def databaseEntryToEntry(dbe: DatabaseEntry): EntryType = {
    val entry = gzippedXmlToEntry(dbe.gzippedXml)
    val properties = propertyMap(entry)
    require(entry.getId == dbe.unirefId)
    require(properties.get("common taxon ID") == Some(dbe.ncbiTaxonomyId))
    entry
  }

  import vars._

  def entryToGzippedXml(entry: EntryType): Array[Byte] = {
    val gzippedBuffer = new GZIPOutputStream(buffer)
    val xmlO = xmlOutputFactory.createXMLStreamWriter(gzippedBuffer)
    val root = new UniRef
    root.getEntry.add(entry)
    marshaller.marshal(root, xmlO)
    gzippedBuffer.finish()
    val result = buffer.toByteArray
    buffer.reset()
    result
  }

  def gzippedXmlToEntry(gzippedXml: Array[Byte]): EntryType = {
    val gzipStream = new ByteArrayInputStream(gzippedXml)
    val xmlStream = new GZIPInputStream(gzipStream)
    val xmlR = xmlInputFactory.createXMLStreamReader(xmlStream)
    val uniref = unmarshaller.unmarshal(xmlR, classOf[UniRef])
    uniref.getValue.getEntry.get(0)
  }
}
