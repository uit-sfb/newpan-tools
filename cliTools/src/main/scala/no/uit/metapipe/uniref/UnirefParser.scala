package no.uit.metapipe.uniref

import java.io.InputStream

import javax.xml.bind.JAXBContext
import javax.xml.stream.XMLStreamConstants._
import com.ctc.wstx.stax.WstxInputFactory
import no.uit.metapipe.common.logging.Logging
import org.uniprot.uniref.{EntryType, ObjectFactory, PropertyType, UniRef50}

class UnirefParser(inputStream: InputStream)
    extends Logging
    with Iterator[EntryType] {
  var initialized = false

  val xmlInputFactory = new WstxInputFactory

  val jaxbContext = JAXBContext.newInstance(classOf[ObjectFactory])
  log.debug("Using JAXB context: " + jaxbContext.getClass.getName)

  val unmarshaller = jaxbContext.createUnmarshaller()
  val xmlR = xmlInputFactory.createXMLStreamReader(inputStream)

  def begin(): Unit = {
    if (!initialized) {
      initialized = true
      xmlR.nextTag()
      xmlR.require(START_ELEMENT, null, null)
      xmlR.nextTag()
      xmlR.require(START_ELEMENT, null, null)
    }
  }

  def hasNext: Boolean = {
    begin()
    xmlR.getEventType == START_ELEMENT && xmlR.getLocalName == "entry"
  }

  def next(): EntryType = {
    if (!hasNext) throw new NoSuchElementException("next on empty iterator")
    if (!initialized) begin()
    xmlR.require(START_ELEMENT, null, "entry")
    val entry = unmarshaller.unmarshal(xmlR, classOf[EntryType]).getValue
    if (xmlR.hasNext) xmlR.next()
    entry
  }
}
