package no.uit.metapipe.uniref

import java.io._
import java.nio.file.{Files, Paths}
import java.sql.DriverManager

import com.beust.jcommander.{Parameter, Parameters}
import no.uit.metapipe.utils.{CommandLike, EntryPoint}
import org.apache.commons.io.IOUtils

object RunCreateUnirefFasta extends EntryPoint[CreateFastaCommand] {
  def exec(args: CreateFastaCommand): Unit = {
    Class.forName("org.sqlite.JDBC")
    val connection =
      DriverManager.getConnection("jdbc:sqlite:" + args.sqliteDatabaseFile)
    val unirefDatabase = new UnirefDatabase(connection)
    val unirefService = new UnirefService(unirefDatabase)

    val outputFileOutputStream =
      if (args.outputFile == "-") System.out
      else new PrintStream(Files.newOutputStream(Paths.get(args.outputFile)))

    val entries = Option(args.taxonomyFile) match {
      case Some(taxonomyFile) =>
        val taxonomyFileInputStream =
          if (taxonomyFile == "-") System.in
          else Files.newInputStream(Paths.get(taxonomyFile))
        val file = IOUtils.toString(taxonomyFileInputStream, "UTF-8")
        val taxonomies: Array[String] =
          file.split('\n').map(_.stripLineEnd).filter(_.size > 0)
        unirefService.findEntriesByTaxonomies(taxonomies)
      case None =>
        unirefService.findAllEntries()
    }

    entries.foreach { entry =>
      outputFileOutputStream.println(">" + entry.unirefId)
      entry.sequence.grouped(100).foreach(outputFileOutputStream.println)
    }
  }
}

@Parameters
class CreateFastaCommand extends CommandLike {
  val cmd = "uniref-create-fasta"

  @Parameter(names = Array("-d", "--database"),
             description = "SQLite3 database file")
  var sqliteDatabaseFile: String = "annotations.sqlite3"

  @Parameter(
    names = Array("--taxonomy-file"),
    description =
      "New-line separated list of EMBL taxonomy IDs. Use - (dash) for stdin.",
    required = false)
  var taxonomyFile: String = _

  @Parameter(
    names = Array("--output-file", "-o"),
    description =
      "New-line separated list of EMBL taxonomy IDs. Use - (dash) for stdin.",
    required = true)
  var outputFile: String = "-"
}
