package no.uit.metapipe.uniref

import scala.collection.JavaConverters._

trait UnirefEntry {
  def unirefId: String
  def ncbiTaxonomyId: String
  def name: String
  def memberCount: Int
  def commonTaxon: String
  def commonTaxonId: String
  def sequence: String
  def references: Map[String, List[String]]
}

// todo: this is a mess. Refactor: replace new UnirefEntry with workflow.model.UnirefAnnotation
class UnirefEntryFactory {
  val conversions = new DatabaseEntryConversions

  def fromDatabaseEntry(dbe: DatabaseEntry): UnirefEntry = {
    lazy val parsedEntry = conversions.databaseEntryToEntry(dbe)
    lazy val properties = conversions.propertyMap(parsedEntry)

    new UnirefEntry {
      override def unirefId: String = dbe.unirefId
      override def ncbiTaxonomyId: String = dbe.ncbiTaxonomyId
      override def sequence: String = {
        val raw = parsedEntry.getRepresentativeMember.getSequence.getValue
        raw.replaceAll("[\r\n ]", "")
      }
      override def name: String = parsedEntry.getName
      override def memberCount: Int = properties("member count").toInt
      override def commonTaxon: String = properties("common taxon")
      override def commonTaxonId: String = {
        val taxonId = properties("common taxon ID")
        require(taxonId == ncbiTaxonomyId)
        taxonId
      }
      override def references: Map[String, List[String]] = {
        parsedEntry.getRepresentativeMember.getDbReference.getProperty.asScala
          .groupBy(_.getType)
          .filterKeys(k => k.endsWith("ID") || k.endsWith("accession"))
          .toMap
          .mapValues(_.map(_.getValue).toList)
      }
    }
  }
}
