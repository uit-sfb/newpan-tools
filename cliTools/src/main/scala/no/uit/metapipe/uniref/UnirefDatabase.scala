package no.uit.metapipe.uniref

import java.io.File
import java.nio.file.Paths
import java.sql.{Connection, PreparedStatement, ResultSet, Statement}

import no.uit.metapipe.common.logging.Logging
import org.apache.commons.io.IOUtils

class UnirefDatabase(connection: Connection) extends Logging {
  def resource(name: String) = {
    val fullName = Paths.get(s"/no/uit/metapipe/uniref/uniref/$name").toString
    IOUtils.toString(getClass.getResourceAsStream(fullName), "UTF-8")
  }

  var insertEntryStmt: PreparedStatement = _
  var selectEntryStmt: PreparedStatement = _

  val findEntriesByTaxonomiesSql: String = resource(
    "findEntriesByTaxonomies.sql")
  val findAllEntriesSql: String = resource("findAllEntries.sql")

  var numInsertedEntries = 0

  def createTable(): Unit = {
    val createDatabaseSql = resource("createDatabase.sql")
    val stmt = connection.createStatement()
    stmt.execute(createDatabaseSql)
    stmt.execute("CREATE INDEX unirefId_idx ON uniref (unirefId);")
    stmt.execute("CREATE INDEX ncbiTaxonomyId_idx ON uniref (ncbiTaxonomyId);")
    stmt.close()
  }

  def prepareInsert(): Unit = {
    createTable()
    prepareStatements()
    connection.setAutoCommit(false)
  }

  def prepareStatements(): Unit = {
    def prepareSql(resourceName: String) =
      connection.prepareStatement(resource(resourceName))
    insertEntryStmt = prepareSql("insertEntry.sql")
    selectEntryStmt = prepareSql("selectEntryByUnirefId.sql")
  }

  def insertDatabaseEntry(entry: DatabaseEntry, writeBufferSize: Int): Unit = {
    numInsertedEntries += 1
    val s = insertEntryStmt
    s.setString(1, entry.unirefId)
    s.setString(2, entry.ncbiTaxonomyId)
    s.setBytes(3, entry.gzippedXml)
    s.addBatch()
    if (numInsertedEntries >= writeBufferSize) {
      executeInsert()
      numInsertedEntries = 0
    }
  }

  private def parseEntry(result: ResultSet): DatabaseEntry = {
    DatabaseEntry(
      unirefId = Option(result.getString(1)).get,
      ncbiTaxonomyId = Option(result.getString(2)).get,
      gzippedXml = Option(result.getBytes(3)).get
    )
  }

  def findEntryByUnirefId(unirefId: String): Option[DatabaseEntry] = {
    selectEntryStmt.setString(1, unirefId)
    val result = selectEntryStmt.executeQuery()
    if (result.next()) {
      Some(parseEntry(result))
    } else None
  }

  def findAllEntries(): Iterator[DatabaseEntry] = {
    val sql = findAllEntriesSql
    val stmt = connection.createStatement()
    val result = stmt.executeQuery(sql)
    Iterator
      .continually[Option[DatabaseEntry]] {
        if (result.next()) {
          Some(parseEntry(result))
        } else {
          stmt.close()
          None
        }
      }
      .takeWhile(_ != None)
      .map(_.get)
  }

  def findEntriesByTaxonomies(
      taxonomies: Array[String]): Iterator[DatabaseEntry] = {
    val sql = findEntriesByTaxonomiesSql.format(taxonomies.mkString(","))
    val stmt = connection.createStatement()
    val result = stmt.executeQuery(sql)
    Iterator
      .continually[Option[DatabaseEntry]] {
        if (result.next()) {
          Some(parseEntry(result))
        } else {
          stmt.close()
          None
        }
      }
      .takeWhile(_ != None)
      .map(_.get)
  }

  def executeInsert() = {
    log.debug("Inserting batch")
    insertEntryStmt.executeBatch()
    log.debug("Batch inserted")
  }

  def commit() = {
    log.debug("Flushing write buffer.")
    executeInsert()
    log.debug("Committing to database")
    connection.commit()
  }

  def close() = {
    log.debug("Closing database")
    insertEntryStmt.close()
    selectEntryStmt.close()
    connection.close()
  }
}
