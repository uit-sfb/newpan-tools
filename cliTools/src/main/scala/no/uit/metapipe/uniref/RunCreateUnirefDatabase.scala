package no.uit.metapipe.uniref

import java.nio.file.{Files, Paths}
import java.sql.DriverManager
import java.util.zip.GZIPInputStream

import com.beust.jcommander._
import no.uit.metapipe.uniref._
import no.uit.metapipe.utils.{CommandLike, EntryPoint}

object RunCreateUnirefDatabase extends EntryPoint[CreateDatabaseCommand] {
  def exec(args: CreateDatabaseCommand): Unit = {
    Class.forName("org.sqlite.JDBC")
    val connection =
      DriverManager.getConnection("jdbc:sqlite:" + args.sqliteDatabaseFile) //path to the sqlite file
    val unirefDatabase = new UnirefDatabase(connection)
    unirefDatabase.prepareInsert()

    val inputStream = { //finds uniref50.xml.gz
      val filename = args.inputFile //empty by default, supposed to be name of the xml file, required
      val fileInputStream = Files.newInputStream(Paths.get(filename))
      if (filename.endsWith(".gz")) new GZIPInputStream(fileInputStream)
      else fileInputStream
    }

    val unirefParser = new UnirefParser(inputStream)
    val converter = new DatabaseEntryConversions

    val entries: Iterator[DatabaseEntry] =
      for {
        entry <- unirefParser
        databaseEntry <- converter.entryToDatabaseEntry(entry)
      } yield databaseEntry

    try {
      entries.foreach(entry => {
        unirefDatabase.insertDatabaseEntry(entry, args.batchSize)
      })
    } finally {
      unirefDatabase.commit()
      unirefDatabase.close()
    }

  }
}

@Parameters
class CreateDatabaseCommand extends CommandLike {
  val cmd = "uniref-create-database"

  @Parameter(names = Array("-i", "--input-file"),
             description = "Uniprot XML file",
             required = true)
  var inputFile: String = _

  @Parameter(names = Array("-d", "--database"),
             description = "SQLite3 database file")
  var sqliteDatabaseFile: String = "annotations.sqlite3"

  @Parameter(names = Array("--batch-size"),
             description = "Number of items in write buffer")
  var batchSize = 10000

  @Parameter(
    names = Array("--invalid-output-file"),
    description =
      "Records that fail to parse are written to this file. (not implemented)",
    hidden = true
  )
  var invalidRecordsFile: String = _
}
