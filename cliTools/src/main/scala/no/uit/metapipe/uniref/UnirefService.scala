package no.uit.metapipe.uniref

import java.sql.DriverManager

import org.sqlite.SQLiteConfig

class UnirefService(db: UnirefDatabase) {
  val converter = new UnirefEntryFactory

  def findAllEntries(): Iterator[UnirefEntry] = {
    db.findAllEntries().map(converter.fromDatabaseEntry)
  }

  def findEntryByUnirefId(unirefId: String): Option[UnirefEntry] = {
    db.findEntryByUnirefId(unirefId).map(converter.fromDatabaseEntry)
  }

  def findEntriesByTaxonomies(
      taxonomies: Array[String]): Iterator[UnirefEntry] = {
    db.findEntriesByTaxonomies(taxonomies).map(converter.fromDatabaseEntry)
  }
}

object UnirefService {
  def fromSqliteFilename(filename: String): UnirefService = {
    Class.forName("org.sqlite.JDBC") //that doesn't seemed to be used anywhere...
    val config = new SQLiteConfig()
    config.setReadOnly(true)
    val connection = DriverManager.getConnection("jdbc:sqlite:" + filename)
//    println(s"connection is valid ${connection.isValid(3)}")
    val unirefDatabase = new UnirefDatabase(connection)
    unirefDatabase.prepareStatements()
    new UnirefService(unirefDatabase)
  }
}
