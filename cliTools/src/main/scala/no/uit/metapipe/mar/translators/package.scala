package no.uit.metapipe.mar

import no.uit.sfb.schemas.mar.ObjectFactory

package object translators {
  private[translators] val objectFactory = new ObjectFactory
}
