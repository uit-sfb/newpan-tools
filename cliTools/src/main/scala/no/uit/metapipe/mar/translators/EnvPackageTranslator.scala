package no.uit.metapipe.mar.translators

import no.uit.metapipe.mar.AttributeError
import no.uit.metapipe.mar.exceptions.ParserException
import no.uit.sfb.schemas.mar.EnvPackage

object EnvPackageTranslator extends TranslatorFactory[EnvPackage] {
  override protected def typeToString(ctx: ValueTranslatorImpl,
                                      t: EnvPackage): String = {
    t.value()
  }

  override protected def stringToType(ctx: ValueTranslatorImpl,
                                      s: String): EnvPackage = {
    try {
      EnvPackage.fromValue(s)
    } catch {
      case _: IllegalArgumentException =>
        val values = EnvPackage.values().map(_.value()).mkString(",")
        throw new ParserException(
          Seq(
            AttributeError(ctx.columnName,
                           s"'$s' did not match any of: $values")))
    }
  }
}
