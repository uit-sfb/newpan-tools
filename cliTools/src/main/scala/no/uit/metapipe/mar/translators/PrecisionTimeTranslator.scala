package no.uit.metapipe.mar.translators

import java.text.DecimalFormat
import java.util.{Calendar, Date}
import javax.xml.datatype.{
  DatatypeConstants,
  DatatypeFactory,
  XMLGregorianCalendar
}

import no.uit.metapipe.mar.AttributeError
import no.uit.metapipe.mar.exceptions.ParserException
import no.uit.sfb.schemas.mar.{
  ObjectFactory,
  PrecisionTime,
  TimeFixed,
  TimeRange
}

object PrecisionTimeTranslator extends TranslatorFactory[PrecisionTime] {
  val sep = '|'

  override protected def typeToString(ctx: ValueTranslatorImpl,
                                      t: PrecisionTime): String = {
    def formatTime(cal: Calendar): String = {
      val df = new DecimalFormat("00")
      val hour = df.format(cal.get(Calendar.HOUR_OF_DAY))
      val min = df.format(cal.get(Calendar.MINUTE))
      val sec = df.format(cal.get(Calendar.SECOND))
      s"$hour:$min:$sec"
    }

    t match {
      case range: TimeRange =>
        val from = formatTime(range.getFrom.toGregorianCalendar)
        val to = formatTime(range.getTo.toGregorianCalendar)
        s"$from$sep$to"
      case fixed: TimeFixed =>
        formatTime(fixed.getValue.toGregorianCalendar)
    }
  }

  override protected def stringToType(ctx: ValueTranslatorImpl,
                                      s: String): PrecisionTime = {
    val of = new ObjectFactory

    val TimeM = """^(\d\d):(\d\d):(\d\d)$""".r

    def parseTime(serialized: String): XMLGregorianCalendar = {
      val dataTypeFactory = DatatypeFactory.newInstance()
      val TimeM(hh, mm, ss) = serialized
      dataTypeFactory.newXMLGregorianCalendarTime(
        hh.toInt,
        mm.toInt,
        ss.toInt,
        DatatypeConstants.FIELD_UNDEFINED)
    }

    val Time = """^(\d\d:\d\d:\d\d)$""".r
    s.split(sep).toList match {
      case List(Time(t1), Time(t2)) =>
        val tr = of.createTimeRange()
        tr.setFrom(parseTime(t1))
        tr.setTo(parseTime(t2))
        tr
      case List(Time(fixed)) =>
        val tf = of.createTimeFixed()
        tf.setValue(parseTime(fixed))
        tf
      case _ =>
        throw new ParserException(
          Seq(AttributeError(
            ctx.columnName,
            s"value '$s' did not match any of these patterns: 11:00:00, or 12:00:00|13:00:00")))
    }
  }
}
