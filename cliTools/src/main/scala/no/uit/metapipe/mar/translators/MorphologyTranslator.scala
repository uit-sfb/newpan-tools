package no.uit.metapipe.mar.translators

import no.uit.metapipe.mar.{AttributeError, Translator}
import no.uit.metapipe.mar.exceptions.ParserException
import no.uit.sfb.schemas.mar.{Morphology, MarRecord}

object MorphologyTranslator extends Translator {
  val columnName = "morphology"

  override def attributeNames: Set[String] = Set(columnName)

  override def xmlToRow(marRecord: MarRecord): Map[String, String] = {
    TranslatorHelpers.translateToColumn(
      columnName,
      marRecord.getMorphology,
      marRecord.getMorphologyMissing)(_.value())
  }
  override def rowToXml(tableRow: Map[String, String],
                        marRecord: MarRecord): Unit = {
    TranslatorHelpers.translateToXml(tableRow(columnName),
      marRecord.setMorphology,
      marRecord.setMorphologyMissing) { row =>
      try {
        Morphology.fromValue(row)
      } catch {
        case e: IllegalArgumentException =>
          val values = Morphology.values().map(_.value()).mkString(", ")
          throw new ParserException(Seq(AttributeError(
            columnName,
            s"The value '$row' was not amongst the expected values: $values")))
      }
    }
  }
}
