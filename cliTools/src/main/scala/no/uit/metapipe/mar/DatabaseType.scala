package no.uit.metapipe.mar

import no.uit.metapipe.mar.translators.AccessionTranslatorFactory.UrlResolver

sealed trait DatabaseType {
  def name: String
  def translator(mmpUrl: UrlResolver): Translator =
    Translator(name, tsvHeaderNames.toSet, mmpUrl)
  def translator: Translator = translator(_ => None)
  def tsvHeaderNames: Seq[String]
}

object DatabaseType {

  object MarRefDbFunCore extends DatabaseType {
    override def name: String = "mar-ref-db-fun-core"
    override def tsvHeaderNames: Seq[String] = List(
      "base_ID",
      "alt_elev",
      "collection_date",
      "depth",
      "env_biome",
      "env_feature",
      "env_material",
      "env_package",
      "env_salinity",
      "env_temp",
      "geo_loc_name",
      "geo_loc_name_GAZ",
      "geo_loc_name_GAZ_ENVO",
      "analysis_project_type",
      "investigation_type",
      "lat_lon",
      "pathogenicity",
      "project_name",
      "assembly",
      "assembly_version",
      "isol_growth_condt",
      "num_replicons",
      "ref_biomaterial",
      "microbe_package",
      "sample_type",
      "strain",
      "isolation_source",
      "collected_by",
      "culture_collection",
      "kingdom",
      "phylum",
      "class",
      "order",
      "family",
      "genus",
      "species",
      "taxon_lineage_ids",
      "taxon_lineage_names",
      //"bacdive_id",
      "curation_date",
      "implementation_date",
      "mmp_biome",
      "silva_accession_SSU",
      "silva_accession_LSU",
      "uniprot_proteome_id",
      "assembly_accession_genbank",
      //"assembly_accession_refseq",
      "bioproject_accession",
      "biosample_accession",
      "genbank_accession",
      "NCBI_refseq_accession",
      "NCBI_taxon_identifier",
      "annotation_provider",
      "annotation_date",
      "annotation_pipeline",
      "annotation_method",
      "annotation_software_revision",
      "features_annotated",
      "genes",
      "cds",
      "pseudo_genes",
      "rrnas",
      "complete_rrnas",
      "partial_rrnas",
      "total_trnas",
      "unique_trnas",
      "ncrna",
      "frameshifted_genes",
      "host_common_name",
      "host_scientific_name",
      "organism",
      "full_scientific_name",
      "disease",
      "publication_pmid",
      "isolation_country",
      "isolation_comments",
      "comments",
      "sequencing_centers",
      "body_sample_site",
      "body_sample_subsite",
      "other_clinical",
      //"gram_stain",
      //"cell_shape",
      "motility",
      "sporulation",
      "temperature_range",
      "optimal_temperature",
      "halotolerance",
      "oxygen_requirement",
      "plasmids",
      "binning",
      "binning_version",
      "estimated_completeness",
      "estimated_contamination",
      "strain_heterogeneity",
      "qs",
      "mapping",
      "mapping_version",
      "quality_assessment",
      "quality_assessment_version",
      "genome_length",
      "gc_content",
      "refseq_cds",
      //"biovar",
      "other_typing",
      "type_strain",
      "seq_meth",
      "sequencing_depth",
      "contigs",
      "n50",
      "genome_status",
      "host_sex",
      "host_health_stage",
      "host_age",
      //"serovar",
      //"pathovar",
      "mmp_ID",
      "updated_date",
      "antismash_types",
      "antismash_clusters",
      "chebi_id",
      "chebi_name",
      "chembl_id",
      "compound_name",
      "uniprot_id",
      "uniprot_description"
    )
  }

  object MarRef extends DatabaseType {
    override def name: String = "marref"
    override def tsvHeaderNames: Seq[String] =
      MarRefDbFunCore.tsvHeaderNames ++ List(
        "bacdive_id",
        "assembly_accession_refseq",
        "gram_stain",
        "cell_shape",
        "biovar",
        "serovar",
        "pathovar",
        "gtdb_classification",
        "fastani_reference",
        "fastani_ani"
      )
  }

  object MarDB extends DatabaseType {
    override def name: String = "mardb"
    override def tsvHeaderNames: Seq[String] = MarRef.tsvHeaderNames
  }

  object SalDB extends DatabaseType {
    override def name: String = "saldb"
    override def tsvHeaderNames: Seq[String] = MarRef.tsvHeaderNames
  }

  object MarCat extends DatabaseType {
    override def name: String = "marcat"
    override def tsvHeaderNames: Seq[String] = {
      Seq(
        "base_ID",
        "mmp_ID",
        "collection_date",
        "env_package",
        "env_biome",
        "env_feature",
        "env_material",
        "geo_loc_name",
        "geo_loc_name_GAZ",
        "geo_loc_name_GAZ_ENVO",
        "lat_lon",
        "investigation_type",
        "project_name",
        "seq_meth",
        "samp_collect_device",
        "diss_oxygen",
        "nitrate",
        "phosphate",
        "depth",
        "env_temp",
        "env_salinity",
        "chlorophyll",
        "silicate",
        "host_common_name",
        "sampling_platform",
        "sampling_campaign",
        "sampling_site",
        "event_date_time",
        "sample_protocol_label",
        "event_device",
        "sample_lower_threshold",
        "sample_upper_threshold",
        "sample_name",
        "ena_version",
        "microbe_package",
        "microbe_package_id",
        "classification_id",
        "classification_lineage",
        "sra",
        "comments",
        "bioproject_accession",
        "biosample_accession",
        "contact_organisation",
        "contact_name",
        "contact_email",
        "publication_pmid",
        "collection_time",
        "implementation_date",
        "curation_date",
        "updated_date",
        "run_id",
        "mmp_biome"
      )
    }
  }

  object MarFun extends DatabaseType {
    override def name: String = "marfun"
    override def tsvHeaderNames: Seq[String] =
      MarRefDbFunCore.tsvHeaderNames ++ List(
        "itsonedb_id",
        "ploidy",
        "propagation"
      )
  }

  object SarsCoV2 extends DatabaseType {
    override def name: String = "sarscov2db"
    override def tsvHeaderNames: Seq[String] = {
      Seq(
        // Manually curated
        "cdb_id",
        "genbank_accession",
        "gisaid_id",
        "sra_run",
        "host_common_name",
        "host_scientific_name",
        "host_sex",
        "host_health_stage",
        "host_age",
        "body_sample_site",
        "body_sample_subsite",
        "other_clinical",
        "samp_collect_device",
        "isolation_source",
        "collection_date",
        "lat_lon",
        "isolation_country",
        "collected_by",
        "bioproject_accession",
        "biosample_accession",
        "isolate_name",
        "isolation_comments",
        "project_name",
        "geo_loc_name_GAZ",
        "geo_loc_name_GAZ_ENVO",
        "other_typing",
        "publication_pmid",
        "env_package",
        "pathogenicity",
        // Automatically curated
        "submitted_by",
        "seq_meth",
        "sequencing_depth",
        "assembly_tool",
        "submission_date",
        "assembly_version",
        "genome_length",
        "host_scientific_name",
        "curation_date",
        "implementation_date",
        "env_package",
        "genome_status",
        "quality",
        // Static values
        "dna_shape",
        "baltimore_class",
        "disease",
        "investigation_type",
        "isol_growth_condt",
        "ref_biomaterial",
        "organism",
        "kingdom",
        "family",
        "genus",
        "species",
        "morphology"
      )
    }
  }

  def databases = Seq(MarRef, MarDB, MarCat, MarFun, SalDB, SarsCoV2)
  def apply(name: String): Option[DatabaseType] = databases.find(_.name == name)

}
