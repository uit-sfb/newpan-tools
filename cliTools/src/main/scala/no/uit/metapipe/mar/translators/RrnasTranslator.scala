package no.uit.metapipe.mar.translators

import java.math.BigInteger

import no.uit.metapipe.mar.{AttributeError, Translator}
import no.uit.metapipe.mar.exceptions.ParserException
import no.uit.sfb.schemas.mar.{Missing, InvestigationType, MarRecord, Rrnas}

object RrnasTranslator extends TranslatorFactory[Rrnas] {
  val sep = ", "
  override protected def typeToString(ctx: RrnasTranslator.ValueTranslatorImpl,
                                      t: Rrnas): String = {
    s"${t.getRrna5S}$sep${t.getRrna16S}$sep${t.getRrna23S}"
  }

  override protected def stringToType(ctx: RrnasTranslator.ValueTranslatorImpl,
                                      s: String): Rrnas = {
    s.split(sep).toList match {
      case List(r5, r16, r23) =>
        val rrnas = objectFactory.createRrnas()
        try {
          rrnas.setRrna5S(BigInt(r5).bigInteger)
          rrnas.setRrna16S(BigInt(r16).bigInteger)
          rrnas.setRrna23S(BigInt(r23).bigInteger)
        } catch {
          case _: NumberFormatException =>
            throw new ParserException(Seq(AttributeError(
              ctx.columnName,
              s"Expected a list of RNA the following form '1${sep}2${sep}3' (r5${sep}r16${sep}r32), got the following instead: '$s'")))
        }
        rrnas
      case _ =>
        throw new ParserException(
          Seq(
            AttributeError(
              ctx.columnName,
              s"'$s' did not match the expected form '1${sep}2${sep}3'")))
    }
  }
}
