package no.uit.metapipe.mar.translators

import no.uit.metapipe.mar.{AttributeError, Translator}
import no.uit.metapipe.mar.exceptions.ParserException
import no.uit.sfb.schemas.mar.MarRecord

object OtherTypingTranslator extends Translator {
  val columnName = "other_typing"

  override def attributeNames: Set[String] = Set(columnName)

  val separator = ":"
  override def xmlToRow(marRecord: MarRecord): Map[String, String] = {
    TranslatorHelpers.translateToColumn("other_typing",
                                        marRecord.getOtherTyping,
                                        marRecord.getOtherTypingMissing) { ot =>
      val str = ot.getTyping() + ":" + ot.getTerm()
      str
    }
  }
  override def rowToXml(tableRow: Map[String, String],
                        marRecord: MarRecord): Unit = {
    TranslatorHelpers.translateToXml(tableRow("other_typing"),
                                     marRecord.setOtherTyping,
                                     marRecord.setOtherTypingMissing) { row =>
      val ot = objectFactory.createOtherTyping()
      row.split(separator).toList match {
        case typing :: term :: Nil =>
          ot.setTerm(term)
          ot.setTyping(typing)
          ot
        case _ =>
          throw ParserException(
            Seq(
              AttributeError(
                "other_typing",
                s"The value '$row' was not a tuple separated by '$separator'")))
      }
    }
  }
}
