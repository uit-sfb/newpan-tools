package no.uit.metapipe.mar

import java.io.InputStream
import javax.xml.bind.JAXBElement

import com.twitter.finagle.Service
import com.twitter.util.{FuturePool, Future}
import no.uit.metapipe.mar.integration.MarXmlParser
import no.uit.sfb.schemas.mar.{MarRecords, MarRecord}
import scala.collection.JavaConverters._

case class MarRequest(ids: Seq[String], database: String)
case class MarResponse(data: MarRecords)

class MarDatabaseService(marDatabase: () => InputStream, pool: FuturePool)
    extends Service[MarRequest, MarResponse] {
  val marXmlParser = new MarXmlParser

  override def apply(request: MarRequest): Future[MarResponse] = {
    pool {
      // Unmarshall
      val marRecordsJAXBElement: JAXBElement[MarRecords] =
        marXmlParser.read(marDatabase())

      val data = marRecordsJAXBElement.getValue

      val ids = request.ids.toSet

      val newRecords = data.getRecord.asScala.filter(
        r =>
          r.getMmpID != null && ids
            .contains(r.getMmpID.getValue)) //util.Collection[_ <: MarRecord]
      data.getRecord.clear()
      data.getRecord.addAll(newRecords.asJava)

      MarResponse(data)
    }
  }
}
