package no.uit.metapipe.mar.translators

import no.uit.metapipe.mar.{AttributeError, Translator}
import no.uit.metapipe.mar.exceptions.ParserException
import no.uit.sfb.schemas.mar.{HostSex, MarRecord}

object HostSexTranslator extends Translator {
  val columnName = "host_sex"

  override def attributeNames: Set[String] = Set(columnName)

  override def xmlToRow(marRecord: MarRecord): Map[String, String] = {
    TranslatorHelpers.translateToColumn(columnName,
                                        marRecord.getHostSex,
                                        marRecord.getHostSexMissing)(_.value())
  }
  override def rowToXml(tableRow: Map[String, String],
                        marRecord: MarRecord): Unit = {
    TranslatorHelpers.translateToXml(tableRow(columnName),
                                     marRecord.setHostSex,
                                     marRecord.setHostSexMissing) { row =>
      try {
        HostSex.fromValue(row)
      } catch {
        case e: IllegalArgumentException =>
          val values = HostSex.values().map(_.value()).mkString(", ")
          throw new ParserException(Seq(AttributeError(
            columnName,
            s"The value '$row' was not amongst the expected values: $values")))
      }
    }
  }
}
