package no.uit.metapipe.mar.integration

import java.io.ByteArrayOutputStream

import com.twitter.finagle.{Filter, Service}
import com.twitter.finagle.http.{Request, Response}
import com.twitter.io.Buf
import com.twitter.util.Future
import no.uit.metapipe.mar.{
  DatabaseType,
  MarRequest,
  MarResponse,
  XmlTsvTranslator
}

import scala.collection.JavaConverters._

class MarHttpFilter extends Filter[Request, Response, MarRequest, MarResponse] {
  override def apply(
      request: Request,
      service: Service[MarRequest, MarResponse]): Future[Response] = {
    val ids: Seq[String] =
      request.params.get("ids").map(_.split('.').toSeq).getOrElse(Seq())
    val database = request.params("db")

    service(MarRequest(ids = ids, database = database)).map { res =>
      val hr = Response()

      val dbType = DatabaseType(res.data.getDatabaseType).get

      val translator = dbType.translator

      // todo: refactor into Writer
      val tsvRecords = res.data.getRecord.asScala.map { marRecord =>
        translator.xmlToRow(marRecord)
      }.toSeq

      val xmlTsvTranslator = new XmlTsvTranslator(dbType)

      val os = new ByteArrayOutputStream()
      xmlTsvTranslator.writeTsv(tsvRecords, () => os)

      hr.content = Buf.Utf8(new String(os.toByteArray, "UTF-8"))
      hr.contentType = "text/tab-separated-values"
      hr.headerMap.add(
        "Content-Disposition",
        "attachment; filename=\"selected-" + database + "-records.tsv\"")
      hr
    }
  }
}
