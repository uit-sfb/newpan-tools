package no.uit.metapipe.mar.translators

import no.uit.metapipe.mar.{AttributeError, Translator}
import no.uit.metapipe.mar.exceptions.ParserException
import no.uit.sfb.schemas.mar.{Halotolerance, MarRecord}

object HalotoleranceTranslator extends Translator {
  val columnName = "halotolerance"

  override def attributeNames: Set[String] = Set(columnName)

  override def xmlToRow(marRecord: MarRecord): Map[String, String] = {
    TranslatorHelpers.translateToColumn(
      columnName,
      marRecord.getHalotolerance,
      marRecord.getHalotoleranceMissing)(_.value())
  }
  override def rowToXml(tableRow: Map[String, String],
                        marRecord: MarRecord): Unit = {
    TranslatorHelpers.translateToXml(tableRow(columnName),
                                     marRecord.setHalotolerance,
                                     marRecord.setHalotoleranceMissing) { row =>
      try {
        Halotolerance.fromValue(row)
      } catch {
        case e: IllegalArgumentException =>
          val values = Halotolerance.values().map(_.value()).mkString(", ")
          throw new ParserException(Seq(AttributeError(
            columnName,
            s"The value '$row' was not amongst the expected values: $values")))
      }
    }
  }
}
