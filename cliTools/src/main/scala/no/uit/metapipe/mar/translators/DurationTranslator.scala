package no.uit.metapipe.mar.translators

import no.uit.metapipe.mar.AttributeError
import no.uit.metapipe.mar.exceptions.ParserException
import no.uit.sfb.schemas.mar.{
  PrecisionDecimalStringFixed,
  PrecisionDecimalStringRange,
  DurationUnit,
  Duration
}

object DurationTranslator extends TranslatorFactory[Duration] {
  override protected def typeToString(
      ctx: DurationTranslator.ValueTranslatorImpl,
      t: Duration): String = {
    val amount = t.getAmount match {
      case range: PrecisionDecimalStringRange =>
        s"${range.getMin}-${range.getMax}"
      case fixed: PrecisionDecimalStringFixed => fixed.getValue
    }
    s"$amount ${t.getUnit.value()}"
  }

  override protected def stringToType(
      ctx: DurationTranslator.ValueTranslatorImpl,
      s: String): Duration = {
    val (value, unit) = s.split(" ").toList match {
      case Nil =>
        throw ParserException(
          Seq(AttributeError(ctx.columnName, s"Missing value")))
      case value :: Nil =>
        value -> "years"
      case value :: unit =>
        value -> unit.mkString(" ")
    }
    val values = value.split("-").toList
    val duration = objectFactory.createDuration()
    duration.setUnit(DurationUnit.fromValue(unit))

    values match {
      case List(min, max) =>
        val range = objectFactory.createPrecisionDecimalStringRange()
        range.setMin(min)
        range.setMax(max)
        duration.setAmount(range)
      case List(value) =>
        val fixed = objectFactory.createPrecisionDecimalStringFixed()
        fixed.setValue(value)
        duration.setAmount(fixed)
    }
    duration
  }
}
