package no.uit.metapipe.mar.integration

import com.twitter.finagle.Service
import com.twitter.util.Future
import no.uit.metapipe.mar.{MarRequest, MarResponse}

class MarSelectorService(dbs: Map[String, Service[MarRequest, MarResponse]])
    extends Service[MarRequest, MarResponse] {
  override def apply(request: MarRequest): Future[MarResponse] = {
    dbs(request.database)(request) // todo: error handling
  }
}
