package no.uit.metapipe.mar.translators

import no.uit.metapipe.mar.Translator
import no.uit.sfb.schemas.mar.{Missing, MarRecord}
import scala.collection.JavaConverters._

class StringListTranslator(
    columnName: String,
    separator: Char,
    listConstructor: MarRecord => Unit,
    list: MarRecord => java.util.List[String],
    getMissing: MarRecord => Missing,
    setMissing: MarRecord => (Missing => Unit)
) extends Translator {

  override def attributeNames: Set[String] = Set(columnName)

  override def xmlToRow(marRecord: MarRecord): Map[String, String] = {
    val values = try {
      list(marRecord)
    } catch {
      case _: NullPointerException =>
        null
    }
    TranslatorHelpers.translateToColumn(
      columnName,
      values,
      getMissing(marRecord))(_.asScala.mkString(separator.toString))
  }

  override def rowToXml(tableRow: Map[String, String],
                        marRecord: MarRecord): Unit = {

    val setter: Array[String] => Unit = { values =>
      val l = try {
        list(marRecord)
      } catch {
        case _: NullPointerException => // May God forgive us...
          listConstructor(marRecord)
          list(marRecord)
      }
      values.foreach(l.add)
    }

    TranslatorHelpers.translateToXml(tableRow(columnName),
                                     setter,
                                     setMissing(marRecord)) { str =>
      str.split(separator)
    }
  }
}

object StringListTranslator {
  def apply(
      columnName: String,
      separator: Char,
      listConstructor: MarRecord => Unit,
      list: MarRecord => java.util.List[String],
      getMissing: MarRecord => Missing,
      setMissing: MarRecord => (Missing => Unit)
  ): StringListTranslator = {
    new StringListTranslator(columnName,
                             separator,
                             listConstructor,
                             list,
                             getMissing,
                             setMissing)
  }
}
