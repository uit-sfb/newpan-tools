package no.uit.metapipe.mar.integration

import java.io.InputStream
import javax.xml.bind.{Unmarshaller, JAXBContext, JAXBElement}

import no.uit.sfb.schemas.mar.{ObjectFactory, MarRecords}

class MarXmlParser {
  def read(in: InputStream): JAXBElement[MarRecords] = {
    // Set up
    val jaxbContext: JAXBContext =
      JAXBContext.newInstance(classOf[ObjectFactory])
    val unmarshaller: Unmarshaller = jaxbContext.createUnmarshaller

    // Unmarshall
    val marRecordsJAXBElement: JAXBElement[MarRecords] =
      unmarshaller.unmarshal(in).asInstanceOf[JAXBElement[MarRecords]]
    marRecordsJAXBElement
  }
}
