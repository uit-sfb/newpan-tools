package no.uit.metapipe.mar.exceptions

import no.uit.metapipe.mar.AttributeError

case class ParserException(errors: Seq[AttributeError]) extends Exception {
  override def getMessage: String = {
    errors.map(err => s"${err.attribute}: ${err.error}").mkString(", ")
  }
}
