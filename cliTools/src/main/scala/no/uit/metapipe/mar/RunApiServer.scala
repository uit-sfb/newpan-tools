package no.uit.metapipe.mar

import java.io.BufferedInputStream
import java.nio.file.{Files, Paths}

import com.beust.jcommander.{Parameter, Parameters}
import com.twitter.finagle.Http
import com.twitter.util.{Await, FuturePool}
import no.uit.metapipe.mar.integration.{MarHttpFilter, MarSelectorService}
import no.uit.metapipe.utils.{CommandLike, EntryPoint}
import no.uit.sfb.scalautils.common.FileUtils

object RunApiServer extends EntryPoint[ApiServerCommand] {
  def exec(args: ApiServerCommand): Unit = {

    def marService(filename: String): MarDatabaseService =
      new MarDatabaseService(
        () =>
          FileUtils.unGzipStream(
            new BufferedInputStream(Files.newInputStream(Paths.get(filename)))),
        FuturePool.unboundedPool)

    val marRefService = marService(args.marRefXmlFile)
    val marCatService = marService(args.marCatXmlFile)
    val marDbService = marService(args.marDbXmlFile)
    val marFunService = marService(args.marFunXmlFile)
    val salDbService = marService(args.salDbXmlFile)
    val sarsCoV2Service = marService(args.sarsCoV2XmlFile)

    val marServices = Map(
      "marref" -> marRefService,
      "marcat" -> marCatService,
      "mardb" -> marDbService,
      "marfun" -> marFunService,
      "saldb" -> salDbService,
      "sarscov2db" -> sarsCoV2Service
    )

    val marSelectorService = new MarSelectorService(marServices)

    val marHttpFilter = new MarHttpFilter

    val httpService = marHttpFilter andThen marSelectorService

    val server = Http.serve(args.bind, httpService)
    Await.ready(server)
  }
}

@Parameters
class ApiServerCommand extends CommandLike {
  val cmd = "api-server"

  @Parameter(names = Array("--bind"), description = "Interface to bind to")
  var bind: String = ":8080"

  @Parameter(names = Array("--marref-xml-file"),
             description = "Database XML file",
             required = true)
  var marRefXmlFile: String = _

  @Parameter(names = Array("--marcat-xml-file"),
             description = "Database XML file",
             required = true)
  var marCatXmlFile: String = _

  @Parameter(names = Array("--mardb-xml-file"),
             description = "Database XML file",
             required = true)
  var marDbXmlFile: String = _

  @Parameter(names = Array("--marfun-xml-file"),
    description = "Database XML file",
    required = true)
  var marFunXmlFile: String = _

  @Parameter(names = Array("--saldb-xml-file"),
    description = "Database XML file",
    required = true)
  var salDbXmlFile: String = _

  @Parameter(names = Array("--sarscov2db-xml-file"),
    description = "Database XML file",
    required = true)
  var sarsCoV2XmlFile: String = _
}
