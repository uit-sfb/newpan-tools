package no.uit.metapipe.mar.translators

import no.uit.metapipe.mar.AttributeError
import no.uit.metapipe.mar.exceptions.ParserException
import no.uit.sfb.schemas.mar.Position

object PositionTranslator extends TranslatorFactory[Position] {
  override protected def typeToString(ctx: ValueTranslatorImpl,
                                      t: Position): String = {
    s"${t.getLatitude}, ${t.getLongitude}"
  }

  override protected def stringToType(ctx: ValueTranslatorImpl,
                                      s: String): Position = {
    s.split(", ").toList match {
      case List(lat, lon) =>
        val pos = objectFactory.createPosition()
        try {
          pos.setLatitude(BigDecimal(lat).bigDecimal)
          pos.setLongitude(BigDecimal(lon).bigDecimal)
        } catch {
          case _: NumberFormatException =>
            throw new ParserException(
              Seq(AttributeError(
                ctx.columnName,
                s"Value '$s' did not match the expected pattern: '123.00, 321.938.'")))
        }
        pos
      case _ =>
        throw new ParserException(Seq(AttributeError(
          ctx.columnName,
          s"Value '$s' did not match the expected pattern: '123.00, 321.938.'")))
    }

  }
}
