package no.uit.metapipe.mar.translators

import no.uit.metapipe.mar.translators.AccessionTranslatorFactory.UrlResolver
import no.uit.sfb.schemas.mar.Accession

class AccessionTranslatorFactory(accessionToUrl: UrlResolver)
    extends TranslatorFactory[no.uit.sfb.schemas.mar.Accession] {
  override protected def typeToString(ctx: ValueTranslatorImpl,
                                      t: Accession): String = {
    t.getValue
  }

  override protected def stringToType(ctx: ValueTranslatorImpl,
                                      s: String): Accession = {
    val accession = objectFactory.createAccession()
    accession.setValue(s)
    accessionToUrl(s).foreach { url =>
      accession.setUrl(url)
    }
    accession
  }
}

object AccessionTranslatorFactory {
  type UrlResolver = String => Option[String]
}
