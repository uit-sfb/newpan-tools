package no.uit.metapipe.mar.translators

import no.uit.metapipe.mar.{AttributeError, Translator}
import no.uit.metapipe.mar.exceptions.ParserException
import no.uit.sfb.schemas.mar.{TemperatureRange, MarRecord}

object TemperatureRangeTranslator extends Translator {
  val columnName = "temperature_range"

  override def attributeNames: Set[String] = Set(columnName)

  override def xmlToRow(marRecord: MarRecord): Map[String, String] = {
    TranslatorHelpers.translateToColumn(
      columnName,
      marRecord.getTemperatureRange,
      marRecord.getTemperatureRangeMissing)(_.value())
  }
  override def rowToXml(tableRow: Map[String, String],
                        marRecord: MarRecord): Unit = {
    TranslatorHelpers.translateToXml(tableRow(columnName),
                                     marRecord.setTemperatureRange,
                                     marRecord.setTemperatureRangeMissing) {
      row =>
        try {
          TemperatureRange.fromValue(row)
        } catch {
          case e: IllegalArgumentException =>
            val values = TemperatureRange.values().map(_.value()).mkString(", ")
            throw new ParserException(Seq(AttributeError(
              columnName,
              s"The value '$row' was not amongst the expected values: $values")))
        }
    }
  }
}
