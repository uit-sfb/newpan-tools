package no.uit.metapipe.mar.translators

import javax.xml.datatype.{DatatypeFactory, XMLGregorianCalendar}
import no.uit.metapipe.common.logging.Logging
import no.uit.sfb.schemas.mar._

object TranslatorHelpers {
  def stringToMissingOpt(value: String): Option[Missing] = {
    val nullValueOpt: Option[MissingReason] = value match {
      case ""        => Some(MissingReason.MISSING) //in new version "" means missing
      case "missing" => Some(MissingReason.MISSING)
      case "unknown" => Some(MissingReason.UNKNOWN)
      case "NA"      => Some(MissingReason.NOT_APPLICABLE)
      case _         => None
    }

    nullValueOpt.map { nv: MissingReason =>
      val missing = objectFactory.createMissing()
      missing.setReason(nv)
      missing
    }
  }

  def missingToString(missing: Missing): String = {
    missing.getReason match {
      case MissingReason.MISSING        => "missing"
      case MissingReason.NOT_APPLICABLE => "NA"
      case MissingReason.UNKNOWN        => "unknown"
    }
  }

  val missingToStringHandler: PartialFunction[(Any, Missing), String] = {
    case (null, missing: Missing) => TranslatorHelpers.missingToString(missing)
    case (null, null)             => "missing"
  }

  def translateToColumn[T](columnName: String, element: T, missing: Missing)(
      translator: T => String): Map[String, String] = {
    val value = (element, missing) match {
      case (null, missing: Missing) =>
        TranslatorHelpers.missingToString(missing)
      case (null, null) => "missing"
      case (t, null)    => translator(t)
      case (_, _)       => "wrong input"
    }
    Map(columnName -> value)
  }

  def translateToXml[T](
      columnValue: String,
      setter: T => Unit,
      setMissing: Missing => Unit)(translator: String => T) = {
    val trimmedValue = (columnValue.split('<').headOption map {
      _.split('§').toList match {
        case "" :: ciri :: _ => ciri
        case value :: _      => value
        case _               => ""
      }
    }).getOrElse("")
    TranslatorHelpers.stringToMissingOpt(trimmedValue) match {
      case Some(missing) => setMissing(missing)
      case None          => setter(translator(trimmedValue))
    }
  }

  def parsePrecisionDateTimeFixed(
      serialized: String): PrecisionDateTimeFixed = {
    val pd = new PrecisionDateTimeFixed
    val dataTypeFactory = DatatypeFactory.newInstance()
    val cal = dataTypeFactory.newXMLGregorianCalendar(serialized)

    serialized.length match {
      case 4  => pd.setYear(cal)
      case 7  => pd.setYearMonth(cal)
      case 10 => pd.setDate(cal)
      case _  => pd.setDateTime(cal)
    }
    pd
  }

  def serializePrecisionDateTimeFixed(
      dateTime: PrecisionDateTimeFixed): String = {
    import dateTime._
    type Calendar = XMLGregorianCalendar

    (getYear, getYearMonth, getDate, getDateTime) match {
      case (cal: Calendar, null, null, null) =>
        cal.toXMLFormat.take(4)
      case (null, cal: Calendar, null, null) =>
        cal.toXMLFormat.take(7)
      case (null, null, cal: Calendar, null) =>
        cal.toXMLFormat.take(10)
      case (null, null, null, cal: Calendar) =>
        cal.toXMLFormat
      case _ => "Invalid date time format"
    }
  }
}
