package no.uit.metapipe.mar

import java.io.{InputStream, OutputStream}

import javax.xml.bind.{JAXBContext, JAXBElement, Marshaller, Unmarshaller}

import scala.reflect.ClassTag

object XmlHelpers {
  def marshaller[ObjectFactory](
      implicit ct: ClassTag[ObjectFactory]): Marshaller = {
    val jaxbContext: JAXBContext = JAXBContext.newInstance(ct.runtimeClass)
    val marshaller: Marshaller = jaxbContext.createMarshaller
    marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true)
    marshaller
  }

  def marshal[ObjectFactory: ClassTag, DocType](doc: JAXBElement[DocType],
                                                out: OutputStream): Unit = {
    marshaller[ObjectFactory].marshal(doc, out)
  }

  def unmarshaller[ObjectFactory](
      implicit ct: ClassTag[ObjectFactory]): Unmarshaller = {
    val jaxbContext: JAXBContext = JAXBContext.newInstance(ct.runtimeClass)
    val unmarshaller: Unmarshaller = jaxbContext.createUnmarshaller
    unmarshaller
  }

  def unmarshal[ObjectFactory: ClassTag, DocType](in: InputStream): DocType = {
    val docJAXBElement: JAXBElement[DocType] = unmarshaller[ObjectFactory]
      .unmarshal(in)
      .asInstanceOf[JAXBElement[DocType]]
    docJAXBElement.getValue
  }
}
