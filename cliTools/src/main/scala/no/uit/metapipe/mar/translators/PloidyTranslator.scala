package no.uit.metapipe.mar.translators

import no.uit.metapipe.mar.{AttributeError, Translator}
import no.uit.metapipe.mar.exceptions.ParserException
import no.uit.sfb.schemas.mar.{Ploidy, MarRecord}

object PloidyTranslator extends Translator {
  val columnName = "ploidy"

  override def attributeNames: Set[String] = Set(columnName)

  override def xmlToRow(marRecord: MarRecord): Map[String, String] = {
    TranslatorHelpers.translateToColumn(
      columnName,
      marRecord.getPloidy,
      marRecord.getPloidyMissing)(_.value())
  }
  override def rowToXml(tableRow: Map[String, String],
                        marRecord: MarRecord): Unit = {
    TranslatorHelpers.translateToXml(tableRow(columnName),
                                     marRecord.setPloidy,
                                     marRecord.setPloidyMissing) {
      row =>
        try {
          Ploidy.fromValue(row)
        } catch {
          case e: IllegalArgumentException =>
            val values =
              Ploidy.values().map(_.value()).mkString(", ")
            throw new ParserException(Seq(AttributeError(
              columnName,
              s"The value '$row' was not amongst the expected values: $values")))
        }
    }
  }
}
