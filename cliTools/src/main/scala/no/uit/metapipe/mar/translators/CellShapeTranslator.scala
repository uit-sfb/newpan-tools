package no.uit.metapipe.mar.translators

import no.uit.metapipe.mar.{AttributeError, Translator}
import no.uit.metapipe.mar.exceptions.ParserException
import no.uit.sfb.schemas.mar.{CellShape, MarRecord}

object CellShapeTranslator extends Translator {
  val columnName = "cell_shape"

  override def attributeNames: Set[String] = Set(columnName)

  override def xmlToRow(marRecord: MarRecord): Map[String, String] = {
    TranslatorHelpers.translateToColumn(
      columnName,
      marRecord.getCellShape,
      marRecord.getCellShapeMissing)(_.value())
  }
  override def rowToXml(tableRow: Map[String, String],
                        marRecord: MarRecord): Unit = {
    TranslatorHelpers.translateToXml(tableRow(columnName),
                                     marRecord.setCellShape,
                                     marRecord.setCellShapeMissing) { row =>
      try {
        CellShape.fromValue(row)
      } catch {
        case e: IllegalArgumentException =>
          val values = CellShape.values().map(_.value()).mkString(", ")
          throw new ParserException(Seq(AttributeError(
            columnName,
            s"The value '$row' was not amongst the expected values: $values")))
      }
    }
  }
}
