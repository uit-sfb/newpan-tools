package no.uit.metapipe.mar

case class AttributeError(attribute: String, error: String)
