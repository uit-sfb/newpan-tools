package no.uit.metapipe.mar.translators

import no.uit.metapipe.mar.{AttributeError, Translator}
import no.uit.metapipe.mar.exceptions.ParserException
import no.uit.sfb.schemas.mar.{BaltimoreClass, MarRecord}

object BaltimoreClassTranslator extends Translator {
  val columnName = "baltimore_class"

  override def attributeNames: Set[String] = Set(columnName)

  override def xmlToRow(marRecord: MarRecord): Map[String, String] = {
    TranslatorHelpers.translateToColumn(
      columnName,
      marRecord.getBaltimoreClass,
      marRecord.getBaltimoreClassMissing)(_.value())
  }
  override def rowToXml(tableRow: Map[String, String],
                        marRecord: MarRecord): Unit = {
    TranslatorHelpers.translateToXml(tableRow(columnName),
                                    marRecord.setBaltimoreClass,
                                    marRecord.setBaltimoreClassMissing) { row =>
      try {
        BaltimoreClass.fromValue(row)
      } catch {
        case e: IllegalArgumentException =>
          val values = BaltimoreClass.values().map(_.value()).mkString(", ")
          throw new ParserException(Seq(AttributeError(
            columnName,
            s"The value '$row' was not amongst the expected values: $values")))
      }
    }
  }
}
