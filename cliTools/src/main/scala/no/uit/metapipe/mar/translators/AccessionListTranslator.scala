package no.uit.metapipe.mar.translators

import no.uit.metapipe.mar.Translator
import no.uit.metapipe.mar.translators.AccessionTranslatorFactory.UrlResolver
import no.uit.sfb.schemas.mar.{Accession, AccessionList, MarRecord, Missing}

import scala.collection.JavaConverters._

class AccessionListTranslator(
    columnName: String,
    separator: Char,
    listConstructor: MarRecord => Unit,
    list: MarRecord => java.util.List[Accession],
    getMissing: MarRecord => Missing,
    setMissing: MarRecord => (Missing => Unit),
    accessionToUrl: UrlResolver = _ => None
) extends Translator {

  override def attributeNames: Set[String] = Set(columnName)

  override def xmlToRow(marRecord: MarRecord): Map[String, String] = {
    val values = try {
      list(marRecord).asScala.map(_.getValue)
    } catch {
      case _: NullPointerException =>
        null
    }
    TranslatorHelpers.translateToColumn(
      columnName,
      values,
      getMissing(marRecord))(_.mkString(separator.toString))
  }

  override def rowToXml(tableRow: Map[String, String],
                        marRecord: MarRecord): Unit = {

    val setter: Array[String] => Unit = { values =>
      val l = try {
        list(marRecord)
      } catch {
        case _: NullPointerException =>
          listConstructor(marRecord)
          list(marRecord)
      }

      values.foreach { str =>
        val accession = objectFactory.createAccession()
        accession.setValue(str)
        accessionToUrl(str).foreach { url =>
          accession.setUrl(url)
        }
        l.add(accession)
      }
    }

    TranslatorHelpers.translateToXml(tableRow(columnName),
                                     setter,
                                     setMissing(marRecord)) { str =>
      str.split(separator)
    }
  }
}

object AccessionListTranslator {
  def apply(
      columnName: String,
      separator: Char,
      listConstructor: MarRecord => Unit,
      list: MarRecord => java.util.List[Accession],
      getMissing: MarRecord => Missing,
      setMissing: MarRecord => (Missing => Unit)
  ): AccessionListTranslator = {
    new AccessionListTranslator(columnName,
                                separator,
                                listConstructor,
                                list,
                                getMissing,
                                setMissing)
  }

  def apply(accessionToUrl: UrlResolver)(
      columnName: String,
      separator: Char,
      listConstructor: MarRecord => Unit,
      list: MarRecord => java.util.List[Accession],
      getMissing: MarRecord => Missing,
      setMissing: MarRecord => (Missing => Unit)
  ): AccessionListTranslator = {
    new AccessionListTranslator(columnName,
                                separator,
                                listConstructor,
                                list,
                                getMissing,
                                setMissing,
                                accessionToUrl = accessionToUrl)
  }
}
