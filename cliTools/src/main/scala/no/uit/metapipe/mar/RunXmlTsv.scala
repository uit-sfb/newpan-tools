package no.uit.metapipe.mar

import java.io._
import java.nio.file.{Files, Paths}

import javax.xml.bind.JAXBElement
import com.beust.jcommander.{Parameter, Parameters}
import com.github.tototoshi.csv.{CSVWriter, QUOTE_MINIMAL, TSVFormat}
import no.uit.metapipe.mar.integration.MarXmlParser
import no.uit.metapipe.utils.{CommandLike, EntryPoint}
import no.uit.sfb.schemas.mar.MarRecords
import no.uit.sfb.scalautils.common.FileUtils

import scala.collection.JavaConverters._

class XmlTsvTranslator(dbType: DatabaseType) {
  def xmlToTsv(in: InputStream): Seq[Map[String, String]] = {
    val marXmlParser = new MarXmlParser

    // Unmarshall
    val marRecordsJAXBElement: JAXBElement[MarRecords] = marXmlParser.read(in)
    val marRecords = marRecordsJAXBElement.getValue

    // translate to rows
    val translator = dbType.translator

    marRecords.getRecord.asScala.map { marRecord =>
      translator.xmlToRow(marRecord)
    }.toSeq
  }

  val headers = dbType.tsvHeaderNames

  def writeTsv(tsv: Seq[Map[String, String]],
               outFn: () => OutputStream): Unit = {
    val out: OutputStream = outFn()

    implicit object format extends TSVFormat {
      override val quoting = QUOTE_MINIMAL
    }

    val writer = CSVWriter.open(out)
    writer.writeRow(headers)

    val rows: Seq[Seq[String]] = tsv.map { row =>
      headers.map { name =>
        row(name)
      }
    }

    writer.writeAll(rows)

    writer.close()
  }

  def apply(in: InputStream, outFn: () => OutputStream): Unit = {
    writeTsv(xmlToTsv(in), outFn)
    /*val headers = table.map(_.keys).reduce { (a, b) =>
      if(a != b) {
        throw new IllegalStateException("a and b are not the same")
      }
      a
    }.toList.sorted*/
  }
}

object RunXmlTsv extends EntryPoint[RunXmlTsvCommand] {
  def exec(args: RunXmlTsvCommand): Unit = {
    val in = FileUtils.unGzipStream(
      new BufferedInputStream(Files.newInputStream(Paths.get(args.inputFile))))

    val dbType: DatabaseType = DatabaseType(args.databaseType).getOrElse {
      throw new IllegalArgumentException(
        s"no such database type: ${args.databaseType}")
    }

    val translator = new XmlTsvTranslator(dbType)

    translator(in, () => Files.newOutputStream(Paths.get(args.outputFile)))
  }
}

@Parameters(
  commandDescription = "Converts an XML integration file into a MarRef/MarDB csv-file that can be imported" +
    " to Libre Office Base/Calc")
class RunXmlTsvCommand extends CommandLike {
  val cmd = "convert-xml-tsv"

  @Parameter(names = Array("-i", "--input-file"),
             description =
               "Input file containing XML data. Use - (dash) for `stdin`",
             required = true)
  var inputFile: String = _

  @Parameter(names = Array("-o", "--output-file"),
             description =
               "Output file to write TSV. Use - (dash) for `stdout`.")
  var outputFile: String = "-"

  @Parameter(names = Array("-t", "--database-type"),
             description = "Database type. Can be any of marref, mardb, marcat",
             required = true)
  var databaseType: String = _
}
