package no.uit.metapipe.mar

import java.io._
import java.nio.file.{Files, Paths}

import com.beust.jcommander.{Parameter, Parameters}
import com.github.tototoshi.csv.{CSVReader, TSVFormat}
import no.uit.metapipe.utils.{CommandLike, EntryPoint}
import no.uit.sfb.scalautils.common.FileUtils
import no.uit.sfb.scalautils.json.Json

object RunMarConverter extends EntryPoint[MarConverterCommand] {
  def exec(args: MarConverterCommand): Unit = {
    case class JsonResult(columns: List[String], rows: List[List[String]])

    implicit object format extends TSVFormat

    val reader =
      CSVReader.open(
        new InputStreamReader(
          FileUtils.unGzipStream(new BufferedInputStream(
            Files.newInputStream(Paths.get(args.inputFile))))))
    val (headers, data) = reader.allWithOrderedHeaders()

    val rows = data.map { rec =>
      headers map rec
    }

    val result = JsonResult(headers, rows)

    FileUtils.writeToFile(Paths.get(args.outputFile), Json.serialize(result))
  }
}

@Parameters(
  commandDescription = "Converts a MarRef/MarDB csv-file that has been exported from Libre Office Base/Calc" +
    " into a json file that can be read by our web application.")
class MarConverterCommand extends CommandLike {
  val cmd = "mar-converter"

  val keyMapping = Set(
    Seq("--input-file", "-i"),
    Seq("--output-file", "-o")
  )

  @Parameter(names = Array("-i", "--input-file"),
             description =
               "Input file containing CSV data. Use - (dash) for `stdin`",
             required = true)
  var inputFile: String = _

  @Parameter(names = Array("-o", "--output-file"),
             description =
               "Output file to write JSON. Use - (dash) for `stdout`.")
  var outputFile: String = "-"
}
