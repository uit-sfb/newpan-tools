package no.uit.metapipe.mar

import java.io.BufferedInputStream
import java.nio.file.{Files, Paths}

import com.beust.jcommander.{Parameter, Parameters}
import net.opengis.kml._2.{KmlType, StyleType}
import no.uit.metapipe.utils.{CommandLike, EntryPoint}
import no.uit.sfb.schemas.mar.MarRecords
import no.uit.sfb.scalautils.common.FileUtils

import scala.collection.JavaConverters._

object RunExportKml extends EntryPoint[RunExportKmlCommand] {
  val objectFactory = new net.opengis.kml._2.ObjectFactory

  def databaseStyle(databaseName: String): StyleType = {
    val style: StyleType = objectFactory.createStyleType()
    style.setId(databaseName)

    val iconStyle = objectFactory.createIconStyleType()

    val MarRef = DatabaseType.MarRef.name
    val MarDB = DatabaseType.MarDB.name
    val MarCat = DatabaseType.MarCat.name
    val MarFun = DatabaseType.MarFun.name

    val href: String = databaseName match {
      case MarRef =>
        "https://mt.google.com/vt/icon/name=icons/onion/SHARED-mymaps-pin-container_4x.png,icons/onion/1899-blank-shape_pin_4x.png&highlight=0288D1,ff000000&scale=2.0"
      case MarDB =>
        "https://mt.google.com/vt/icon/name=icons/onion/SHARED-mymaps-pin-container-bg_4x.png,icons/onion/SHARED-mymaps-pin-container_4x.png,icons/onion/1899-blank-shape_pin_4x.png&highlight=ff000000,E65100,ff000000&scale=2.0"
      case MarCat =>
        "https://mt.google.com/vt/icon/name=icons/onion/SHARED-mymaps-pin-container_4x.png,icons/onion/1899-blank-shape_pin_4x.png&highlight=C2185B,ff000000&scale=2.0"
      case MarFun =>
        "https://mt.google.com/vt/icon/name=icons/onion/SHARED-mymaps-pin-container_4x.png,icons/onion/1899-blank-shape_pin_4x.png&highlight=7070FF,ff000000&scale=2.0"
      case _ =>
        "https://mt.google.com/vt/icon/name=icons/onion/SHARED-mymaps-pin-container_4x.png,icons/onion/1899-blank-shape_pin_4x.png&highlight=FBC02D,ff000000&scale=2.0"
    }

    val icon = objectFactory.createBasicLinkType()
    icon.setHref(href)
    iconStyle.setIcon(icon)

    style.setIconStyle(iconStyle)
    style
  }

  def exec(args: RunExportKmlCommand): Unit = {
    val xmlIn = FileUtils.unGzipStream(
      new BufferedInputStream(Files.newInputStream(Paths.get(args.inputFile))))
    val marRecords = XmlHelpers
      .unmarshal[no.uit.sfb.schemas.mar.ObjectFactory, MarRecords](xmlIn)

    val kml: KmlType = objectFactory.createKmlType()

    val document = objectFactory.createDocumentType()

    val styleName =
      Option(marRecords.getDatabaseType).getOrElse("defaultDatabase")
    document.getAbstractStyleSelectorGroup.add(
      objectFactory.createStyle(databaseStyle(styleName)))

    for (record <- marRecords.getRecord.asScala) {
      Option(record.getLatLon).foreach { pos =>
        val placemark = objectFactory.createPlacemarkType()

        val title = Option(record.getFullScientificName)
          .orElse {
            Option(record.getProjectName)
          }
          .getOrElse(record.getMmpID.getValue)

        placemark.setName(title)
        val id = record.getMmpID

        val marRefHref =
          if (id.getUrl != null) "href=\"" + id.getUrl + "\"" else ""

        placemark.setStyleUrl(s"#$styleName")

        placemark.setDescription(s"""<a $marRefHref>${id.getValue}</a>
             |${record.getComments}
           """.stripMargin)

        val point = objectFactory.createPointType()
        point.getCoordinates.add(s"${pos.getLongitude},${pos.getLatitude}")

        placemark.setAbstractGeometryGroup(objectFactory.createPoint(point))
        document.getAbstractFeatureGroup.add(
          objectFactory.createPlacemark(placemark))
      }
    }

    kml.setAbstractFeatureGroup(objectFactory.createDocument(document))

    val kmlOut = Files.newOutputStream(Paths.get(args.outputFile))
    XmlHelpers
      .marshal[net.opengis.kml._2.ObjectFactory, net.opengis.kml._2.KmlType](
        objectFactory.createKml(kml),
        kmlOut)

    xmlIn.close()
    kmlOut.close()
  }
}

@Parameters(
  commandDescription =
    """Extracts sampling positions from a Mar XML database file 
and exports them to a KML file that can be imported into Google Earth or Google Maps
for visualization.""")
class RunExportKmlCommand extends CommandLike {
  val cmd = "export-kml"

  @Parameter(names = Array("-i", "--input-file"),
             description =
               "Input file containing XML data. Use - (dash) for `stdin`",
             required = true)
  var inputFile: String = _

  @Parameter(names = Array("-o", "--output-file"),
             description = "Output KML file. Use - (dash) for `stdout`.")
  var outputFile: String = "-"
}
