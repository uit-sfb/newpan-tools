package no.uit.metapipe.mar.translators

import no.uit.metapipe.mar.{AttributeError, Translator}
import no.uit.metapipe.mar.exceptions.ParserException
import no.uit.sfb.schemas.mar.{OxygenRequirement, MarRecord}

object OxygenRequirementTranslator extends Translator {
  val columnName = "oxygen_requirement"

  override def attributeNames: Set[String] = Set(columnName)

  override def xmlToRow(marRecord: MarRecord): Map[String, String] = {
    TranslatorHelpers.translateToColumn(
      columnName,
      marRecord.getOxygenRequirement,
      marRecord.getOxygenRequirementMissing)(_.value())
  }
  override def rowToXml(tableRow: Map[String, String],
                        marRecord: MarRecord): Unit = {
    TranslatorHelpers.translateToXml(tableRow(columnName),
                                     marRecord.setOxygenRequirement,
                                     marRecord.setOxygenRequirementMissing) {
      row =>
        try {
          OxygenRequirement.fromValue(row)
        } catch {
          case e: IllegalArgumentException =>
            val values =
              OxygenRequirement.values().map(_.value()).mkString(", ")
            throw new ParserException(Seq(AttributeError(
              columnName,
              s"The value '$row' was not amongst the expected values: $values")))
        }
    }
  }
}
