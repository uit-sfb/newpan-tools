package no.uit.metapipe.mar

import java.io.{
  BufferedInputStream,
  InputStream,
  InputStreamReader,
  OutputStream
}
import java.nio.file.{Files, Paths}

import javax.xml.XMLConstants
import javax.xml.bind.Marshaller.Listener
import javax.xml.bind._
import org.eclipse.persistence.jaxb.{JAXBContextFactory, MarshallerProperties}
import javax.xml.transform.stream.StreamSource
import javax.xml.validation.SchemaFactory
import com.beust.jcommander.{Parameter, Parameters}
import com.github.tototoshi.csv.{CSVReader, TSVFormat}
import com.typesafe.scalalogging.Logger
import no.uit.metapipe.mar.exceptions.ParserException
import no.uit.metapipe.mar.translators.AccessionTranslatorFactory.UrlResolver
import no.uit.sfb.schemas.mar.{MarRecord, MarRecords, ObjectFactory}
import org.xml.sax.{Attributes, SAXParseException}
import org.xml.sax.helpers.DefaultHandler
import no.uit.metapipe.common.logging.Logging
import no.uit.metapipe.utils.{CommandLike, EntryPoint}
import no.uit.sfb.scalautils.common.FileUtils

import scala.util.Try

class TsvXmlTranslator(dbType: DatabaseType,
                       mmpUrl: UrlResolver = _ => None,
                       validationDisabled: Boolean = false)
    extends Logging {
  if (validationDisabled)
    logger.warn(s"Validation disabled")

  def apply(in: InputStream,
            outFnXml: () => OutputStream,
            outFnJson: () => OutputStream): Unit = {
    try {
      writeXml(parseTsv(in), outFnXml, outFnJson)
    } finally {
      in.close()
    }
  }

  def parseTsv(in: InputStream): Seq[Map[String, String]] = {
    implicit object format extends TSVFormat

    val reader = CSVReader.open(new InputStreamReader(in, "UTF-8"))
    reader.allWithHeaders()
  }

  def writeXml(rows: Seq[Map[String, String]],
               outFnXml: () => OutputStream,
               outFnJson: () => OutputStream): Unit = {
    val translator = dbType.translator(mmpUrl)

    val of = new ObjectFactory
    val marRecords = of.createMarRecords()
    marRecords.setDatabaseType(dbType.name)

    var lineNumber = 0
    val eout = System.err

    rows.foreach { row =>
      lineNumber += 1
      try {
        val marRecord = of.createMarRecord()
        translator.rowToXml(row, marRecord)
        marRecords.getRecord.add(marRecord)
      } catch {
        case ParserException(errors) =>
          //Do NOT disable those prints, otherwise you will not see when the parsing fails
          val baseId =
            row.get("base_ID").map(id => s" (base_ID: $id)").getOrElse("")
          eout.println(
            Console.BOLD + s"Line $lineNumber:$baseId" + Console.RESET)
          errors.foreach(
            e =>
              println(
                s"\t${Console.BOLD}${e.attribute}:${Console.RESET} ${e.error}"))
          throw new Exception("Parsing failed")
      }
    }

    // todo: this REALLY needs refactoring.... keywords: error reporting

    System.setProperty("javax.xml.bind.context.factory",
                       "org.eclipse.persistence.jaxb.JAXBContextFactory")
    val jaxbContext: JAXBContext = JAXBContextFactory.createContext(
      Array(classOf[ObjectFactory]): Array[Class[_]],
      null)
    val marshaller: Marshaller = jaxbContext.createMarshaller
    marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true)
    val schemaSource = new StreamSource(
      getClass.getResourceAsStream("/no/uit/sfb/xsd/mar.xsd"))
    val schema =
      if (validationDisabled)
        null
      else
        SchemaFactory
          .newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI)
          .newSchema(schemaSource)

    marshaller.setSchema(schema)

    // Marshall
    val objectFactory: ObjectFactory = new ObjectFactory
    val marRecordsJAXBElement: JAXBElement[MarRecords] =
      objectFactory.createRecords(marRecords)

    // error reporting
    val errorReporterHandler = new ErrorReporterHandler
    marshaller.setEventHandler(errorReporterHandler.eventHandler)
    marshaller.setListener(errorReporterHandler.listener)
    marshaller.marshal(marRecordsJAXBElement, errorReporterHandler)

    if (errorReporterHandler.hasErrors) {
      eout.println(
        Console.RED + Console.BOLD + "ERRORS! XML schema errors. Not generating XML file." + Console.RESET)
      return
    }

    // Generate XML
    val outXml = outFnXml()
    marshaller.marshal(marRecordsJAXBElement, outXml)

    // Generate JSON
    val marshallerJson: Marshaller = jaxbContext.createMarshaller
    marshallerJson.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true)
    marshallerJson.setProperty(MarshallerProperties.MEDIA_TYPE,
                               "application/json")
    marshallerJson.setProperty(MarshallerProperties.JSON_INCLUDE_ROOT, true)
    val outJson = outFnJson()
    marshallerJson.marshal(marRecordsJAXBElement, outJson)

  }
}

class ErrorReporterHandler(implicit log: Logger) extends DefaultHandler {
  var hasErrors = false

  var currentRecord: Option[MarRecord] = None

  object eventHandler extends ValidationEventHandler {
    override def handleEvent(event: ValidationEvent): Boolean = {
      val msg =
        Option(event.getLinkedException).map(_.getMessage).getOrElse("<>")
      hasErrors = true
      val recordInfo: String = currentRecord
        .map { r =>
          val baseId = Try(s"${r.getBaseID.getValue}").getOrElse("?")
          val mmpId = Try(s"${r.getMmpID.getValue}").getOrElse("?")
          s"baseId: $baseId, mmpID: $mmpId"
        }
        .getOrElse("?")
      log.error(
        s"(record: $recordInfo) " + elements.reverse.mkString("/") + s" : $msg")
      true
    }
  }

  object listener extends Listener {
    override def afterMarshal(source: scala.Any): Unit = {
      source match {
        case r: MarRecord =>
          currentRecord = None
        case _ =>
      }
    }

    override def beforeMarshal(source: scala.Any): Unit = {
      source match {
        case r: MarRecord =>
          currentRecord = Some(r)
        case _ =>
      }
    }
  }

  var elements: List[String] = Nil

  override def startElement(uri: String,
                            localName: String,
                            qName: String,
                            attributes: Attributes): Unit = {
    val name = Option(localName).getOrElse("???")
    elements = name :: elements
  }

  override def endElement(uri: String,
                          localName: String,
                          qName: String): Unit = {
    elements = elements.tail
  }

  def printError(typ: String, e: SAXParseException): Unit = {
    log.error(typ + ": " + elements.reverse.mkString("/") + ": " + e.getMessage)
  }

  override def warning(e: SAXParseException): Unit = printError("warning", e)

  override def error(e: SAXParseException): Unit = printError("error", e)

  override def fatalError(e: SAXParseException): Unit =
    printError("fatalError", e)
}

object RunTsvXml extends EntryPoint[RunTsvXmlCommand] {
  def exec(args: RunTsvXmlCommand): Unit = {
    val in: InputStream = FileUtils.unGzipStream(
      new BufferedInputStream(Files.newInputStream(Paths.get(args.inputFile))))

    val dbType: DatabaseType = DatabaseType(args.databaseType).getOrElse {
      throw new IllegalArgumentException(
        s"no such database type: ${args.databaseType}")
    }

    val urlResolver: UrlResolver = accession =>
      Option(
        Option(args.mmpUrlPrefix)
          .map(prefix => s"$prefix$accession")
          .getOrElse(
            s"https://mmp.sfb.uit.no/databases/${args.databaseType}/#/records/$accession"))

    val translator =
      new TsvXmlTranslator(dbType, urlResolver, args.disableValidation)
    translator(in,
               () => Files.newOutputStream(Paths.get(args.outputFile)),
               () =>
                 Files.newOutputStream(
                   Paths.get(args.outputFile.replace(".xml", "") + ".json")))
  }
}

@Parameters(
  commandDescription = "Converts a MarRef/MarDB/MarCat/MarFun/SalDB/SarsCoV2 tsv-file that has been exported from Libre Office Base/Calc" +
    " into an XML integration file.")
class RunTsvXmlCommand extends CommandLike {
  val cmd = "convert-tsv-xml"

  @Parameter(names = Array("-i", "--input-file"),
             description =
               "Input file containing TSV data. Use - (dash) for `stdin`",
             required = true)
  var inputFile: String = _

  @Parameter(
    names = Array("-o", "--output-file"),
    description =
      "Output file to write XML (JSON will generated in addition). Use - (dash) for `stdout`.")
  var outputFile: String = "-"

  @Parameter(
    names = Array("-t", "--database-type"),
    description =
      "Database type. Can be any of: marref, mardb, marcat, marfun, saldb, sarscov2db",
    required = true)
  var databaseType: String = _

  @Parameter(
    names = Array("--mmp-url-prefix"),
    description =
      "Prefix for mmp url. Example: https://sfb.uit.no/marcat/ will result in https://sfb.uit.no/marcat/MMP12334."
  )
  var mmpUrlPrefix: String = _

  @Parameter(
    names = Array("--no-validation"),
    description = "Disable validation"
  )
  var disableValidation: Boolean = false
}
