package no.uit.metapipe.mar.translators

import no.uit.metapipe.mar.{AttributeError, Translator}
import no.uit.metapipe.mar.exceptions.ParserException
import no.uit.sfb.schemas.mar._

object InvestigationTypeTranslator extends Translator {
  val columnName = "investigation_type"

  override def attributeNames: Set[String] = Set(columnName)

  override def xmlToRow(marRecord: MarRecord): Map[String, String] = {
    TranslatorHelpers.translateToColumn(
      columnName,
      marRecord.getInvestigationType,
      marRecord.getInvestigationTypeMissing)(_.value())
  }
  override def rowToXml(tableRow: Map[String, String],
                        marRecord: MarRecord): Unit = {
    TranslatorHelpers.translateToXml(tableRow(columnName),
                                     marRecord.setInvestigationType,
                                     marRecord.setInvestigationTypeMissing) {
      row =>
        try {
          InvestigationType.fromValue(row)
        } catch {
          case e: IllegalArgumentException =>
            val values =
              InvestigationType.values().map(_.value()).mkString(", ")
            throw new ParserException(Seq(AttributeError(
              columnName,
              s"The value '$row' was not amongst the expected values: $values")))
        }
    }
  }
}
