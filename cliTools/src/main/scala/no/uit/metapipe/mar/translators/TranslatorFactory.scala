package no.uit.metapipe.mar.translators

import javax.xml.datatype.{DatatypeFactory, XMLGregorianCalendar}

import scala.collection.JavaConverters._
import no.uit.metapipe.mar.{AttributeError, Translator}
import no.uit.metapipe.mar.exceptions.ParserException
import no.uit.sfb.schemas.mar._

abstract class TranslatorFactory[T] {
  protected class ValueTranslatorImpl(
      val columnName: String,
      val getValue: MarRecord => T,
      val getMissing: MarRecord => Missing,
      val setValue: MarRecord => (T => Unit),
      val setMissing: MarRecord => (Missing => Unit)
  ) extends Translator {

    override def attributeNames: Set[String] = Set(columnName)

    override def xmlToRow(marRecord: MarRecord): Map[String, String] = {
      TranslatorHelpers.translateToColumn(
        columnName,
        getValue(marRecord),
        getMissing(marRecord))(typeToString(this, _))
    }

    override def rowToXml(tableRow: Map[String, String],
                          marRecord: MarRecord): Unit = {
      TranslatorHelpers.translateToXml(
        tableRow(columnName),
        setValue(marRecord),
        setMissing(marRecord))(stringToType(this, _))
    }
  }

  protected def typeToString(ctx: ValueTranslatorImpl, t: T): String
  protected def stringToType(ctx: ValueTranslatorImpl, s: String): T

  def apply(
      columnName: String,
      getValue: MarRecord => T,
      getMissing: MarRecord => Missing,
      setValue: MarRecord => (T => Unit),
      setMissing: MarRecord => (Missing => Unit)
  ): Translator = {
    new ValueTranslatorImpl(columnName,
                            getValue,
                            getMissing,
                            setValue,
                            setMissing)
  }
}

object Translators {

  object Integer extends TranslatorFactory[java.math.BigInteger] {
    override protected def typeToString(ctx: ValueTranslatorImpl,
                                        t: java.math.BigInteger): String =
      t.toString
    override protected def stringToType(ctx: ValueTranslatorImpl,
                                        s: String): java.math.BigInteger = {
      try {
        BigInt(s).bigInteger
      } catch {
        case _: NumberFormatException =>
          throw new ParserException(
            Seq(AttributeError(ctx.columnName, s"Expected integer, got: '$s'")))
      }
    }
  }

  object Boolean extends TranslatorFactory[java.lang.Boolean] {
    override protected def typeToString(ctx: ValueTranslatorImpl,
                                        t: java.lang.Boolean): String =
      if (t) "Yes" else "No"

    override protected def stringToType(ctx: ValueTranslatorImpl,
                                        s: String): java.lang.Boolean = {
      s match {
        case "Yes" => true
        case "No"  => false
        case _ =>
          throw new ParserException(
            Seq(
              AttributeError(
                ctx.columnName,
                s"'$s' did not match any of the expected values: 'Yes', 'No'")))
      }
    }
  }

  object String extends TranslatorFactory[java.lang.String] {
    override protected def typeToString(ctx: ValueTranslatorImpl,
                                        t: String): String = t

    override protected def stringToType(ctx: ValueTranslatorImpl,
                                        s: String): String = s
  }

  object PublicationPmid
      extends TranslatorFactory[no.uit.sfb.schemas.mar.PublicationPmid] {
    val separator = '|'

    val PMID = """^(\d+)$""".r
    val DOI = """^(\d+\.\d+\/\w+\d+)$""".r

    override protected def typeToString(
        ctx: no.uit.metapipe.mar.translators.Translators.PublicationPmid.ValueTranslatorImpl,
        t: PublicationPmid): String = {
      t.getAccessionOrUrl.asScala
        .map {
          case accession: Accession => accession.getValue
          case url: UrlIdentifier   => url.getValue
        }
        .mkString(separator.toString)
    }

    override protected def stringToType(
        ctx: no.uit.metapipe.mar.translators.Translators.PublicationPmid.ValueTranslatorImpl,
        instr: String): PublicationPmid = {
      val ret = objectFactory.createPublicationPmid()

      instr.split(separator).foreach {
        case s if s.startsWith("http://") || s.startsWith("https://") =>
          val url = objectFactory.createUrlIdentifier()
          url.setValue(s)
          ret.getAccessionOrUrl.add(url)
        case v =>
          val acc = objectFactory.createAccession()
          acc.setValue(v)
          v match {
            case PMID(pmid) =>
              acc.setUrl(s"http://identifiers.org/pubmed/$pmid")
            case DOI(doi) => acc.setUrl(s"http://identifiers.org/doi/$doi")
            case _        =>
          }
          // todo add url
          ret.getAccessionOrUrl.add(acc)
      }
      ret
    }
  }

  object Envo extends TranslatorFactory[no.uit.sfb.schemas.mar.Accession] {
    override protected def typeToString(ctx: ValueTranslatorImpl,
                                        t: Accession): String = {
      t.getValue
    }

    override protected def stringToType(ctx: ValueTranslatorImpl,
                                        s: String): Accession = {
      val accession = objectFactory.createAccession()
      accession.setValue(s)
      if (s.lastIndexOf("(") != -1 && s.lastIndexOf(")") != -1) {
        val id = s.substring(s.lastIndexOf("(") + 1, s.lastIndexOf(")"))
        accession.setUrl(s"http://identifiers.org/envo/$id")
      }
      accession
    }
  }

  object DecimalString extends TranslatorFactory[String] {
    override protected def typeToString(ctx: DecimalString.ValueTranslatorImpl,
                                        t: String): String = {
      t
    }

    override protected def stringToType(ctx: DecimalString.ValueTranslatorImpl,
                                        s: String): String = {
      val Number = """^(-?\d+(\.\d+)?)$""".r
      s match {
        case n if Number.findAllIn(s).nonEmpty => n
        case _ =>
          throw new ParserException(
            Seq(AttributeError(
              ctx.columnName,
              s"the value '$s' was not a decimal. Valid examples: 123 or 123.2 or -124")))
      }
    }
  }

  object PrecisionDecimalString
      extends TranslatorFactory[no.uit.sfb.schemas.mar.PrecisionDecimalString] {
    val sep = '|'

    override protected def typeToString(
        ctx: PrecisionDecimalString.ValueTranslatorImpl,
        length: PrecisionDecimalString): String = {
      length match {
        case fixedDepth: PrecisionDecimalStringFixed => fixedDepth.getValue
        case range: PrecisionDecimalStringRange =>
          s"${range.getMin}$sep${range.getMax}"
      }
    }

    override protected def stringToType(
        ctx: PrecisionDecimalString.ValueTranslatorImpl,
        s: String): PrecisionDecimalString = {
      val Number = """^(-?\d+(\.\d+)?)$""".r

      def isNumber(str: String): Boolean = {
        Number.findFirstIn(str).isDefined
      }

      val values = s.split(sep).toList
      values match {
        case List(min, max) if isNumber(min) && isNumber(max) =>
          val range = objectFactory.createPrecisionDecimalStringRange()
          range.setMin(min)
          range.setMax(max)
          range
        case List(number) if isNumber(number) =>
          val depth = objectFactory.createPrecisionDecimalStringFixed()
          depth.setValue(number)
          depth
        case v =>
          throw new ParserException(Seq(AttributeError(
            ctx.columnName,
            s"the value '$s' did not match either of the patterns '123.123' or '123.2${sep}321 or -123|12' (value or range expected).")))
      }
    }
  }

  object Accession extends AccessionTranslatorFactory(_ => None) {
    def apply(
        accessionToUrl: String => Option[String]): AccessionTranslatorFactory =
      new AccessionTranslatorFactory(accessionToUrl)
  }

  object PrecisionDateTime
      extends TranslatorFactory[no.uit.sfb.schemas.mar.PrecisionDateTime] {
    val separator = "--"

    override protected def typeToString(
        ctx: PrecisionDateTime.ValueTranslatorImpl,
        t: PrecisionDateTime): String = {
      t match {
        case fixed: PrecisionDateTimeFixed =>
          s"${TranslatorHelpers.serializePrecisionDateTimeFixed(fixed)}"
        case range: PrecisionDateTimeRange =>
          s"${TranslatorHelpers.serializePrecisionDateTimeFixed(range.getFrom)}${separator}${TranslatorHelpers
            .serializePrecisionDateTimeFixed(range.getTo)}"
      }
    }

    override protected def stringToType(
        ctx: PrecisionDateTime.ValueTranslatorImpl,
        s: String): PrecisionDateTime = {
      val values = s.split(separator).toList
      values match {
        case List(from, to) =>
          var pd = new PrecisionDateTimeRange
          try {
            pd.setFrom(TranslatorHelpers.parsePrecisionDateTimeFixed(from))
          } catch {
            case _: IllegalArgumentException =>
              throw new ParserException(Seq(AttributeError(
                ctx.columnName,
                s"Expected a valid ISO date, got the following instead: '$from'")))
          }

          try {
            pd.setTo(TranslatorHelpers.parsePrecisionDateTimeFixed(to))
          } catch {
            case _: IllegalArgumentException =>
              throw new ParserException(Seq(AttributeError(
                ctx.columnName,
                s"Expected a valid ISO date, got the following instead: '$to'")))
          }

          pd
        case List(fixed) =>
          try {
            TranslatorHelpers.parsePrecisionDateTimeFixed(fixed)
          } catch {
            case _: IllegalArgumentException =>
              throw new ParserException(Seq(AttributeError(
                ctx.columnName,
                s"Expected either a valid ISO date, or a date range on the following form (D1${separator}D2) got the following instead: '$fixed'")))
          }
      }
    }
  }

  object Date extends TranslatorFactory[XMLGregorianCalendar] {
    override protected def typeToString(ctx: Date.ValueTranslatorImpl,
                                        t: XMLGregorianCalendar): String = {
      t.toXMLFormat.take(10)
    }

    override protected def stringToType(ctx: Date.ValueTranslatorImpl,
                                        s: String): XMLGregorianCalendar = {
      val dataTypeFactory = DatatypeFactory.newInstance()
      try {
        dataTypeFactory.newXMLGregorianCalendar(s)
      } catch {
        case _: IllegalArgumentException =>
          throw new ParserException(
            Seq(
              AttributeError(ctx.columnName,
                             s"'$s' is not a valid ISO date (YYYY-MM-DD)")))
      }
    }
  }

  val Duration = no.uit.metapipe.mar.translators.DurationTranslator
  val AccessionList = AccessionListTranslator
  val StringList = StringListTranslator
  val Position = PositionTranslator
  val Rrnas = RrnasTranslator
  val PrecisionTime = PrecisionTimeTranslator
}
