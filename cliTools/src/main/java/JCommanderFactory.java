import com.beust.jcommander.JCommander;

public class JCommanderFactory {
    public static JCommander getInstance(Object obj) {
        return new JCommander(obj);
    }
}
